/**门店选择**/
(function(exports,$,req,$page,u,ui,tpl){
	var globalreq=null;
	function div(im){
		return im("div");
	}
	function datafn(im){
		return im("data");
	}
	function selectedProvice(im,data){
		var item = u.find(u.pipe(u.prop("id"),u.eq(datafn(im).provice)),data);
		return item;
	}
	function selectedCity(im,provice){
		var item = u.find(u.pipe(u.prop("id"),u.eq(datafn(im).city)),provice.childerns);
		return item;
	}
	function selectedArea(im,city){
		var item =  u.find(u.pipe(u.prop("id"),u.eq(datafn(im).area)),city.childerns);
		return item;
	}
	function selectedStore(im,area){
		var item =  u.find(u.pipe(u.prop("id"),u.eq(datafn(im).store)),area.childerns);
		return item;
	}

	function minNum(real,current){
		if(real<current)return real;
		return current;
	}
	function selectedItem(im,data){
		function childernfn(current){
			if(current==0)return data;
			if(current==1)return provice.childerns;
			if(current==2)return city.childerns;
			if(current==3)return area.childerns;
		}
		var titles=[];
		var provice=selectedProvice(im,data);
		var current=datafn(im).current;
		if(provice==null){
			titles.push({name:"请选择"});
			return {
				current:minNum(0,current),
				titles:titles,
				childerns:childernfn(minNum(0,current))
			}
		}
		titles.push({name:provice.name,id:provice.id});
		var city=selectedCity(im,provice);
		if(city==null){
			titles.push({name:"请选择"});
			return {
				current:minNum(1,current),
				titles:titles,
				childerns:childernfn(minNum(1,current))
			}
		}
		titles.push({name:city.name,id:city.id});
		var area=selectedArea(im,city);
		if(area==null){
			titles.push({name:"请选择"});
			return {
				current:minNum(2,current),
				titles:titles,
				childerns:childernfn(minNum(2,current))
			}
		}
		titles.push({name:area.name,id:area.id});
		var store=selectedStore(im,area);
		if(store==null){
			titles.push({name:"请选择"});
			return {
				current:minNum(3,current),
				titles:titles,
				childerns:childernfn(minNum(3,current))
			}
		}
		titles.push({name:store.name,id:store.id});
		return {
			current:minNum(3,current),
			titles:titles,
			childerns:childernfn(minNum(3,current))
		}
		
	}
	function init(im){
		div(im).html(tpl.comp_gstore({noreq:true}));
		globalreq().then(function(data){
			render(im,data)
		});
	}
	function render(im,data){
		var viewData=selectedItem(im,data.data);
		div(im).html(tpl.comp_gstore({
				data:viewData,
				noreq:false
		}));
		var map={
			"0":"provice",
			"1":"city",
			"2":"area",
			"3":"store"
		}
		function set(field,id){
			datafn(im)[map[field]]=id;
			datafn(im).current=field-0;
		}
		function set1(field,id,sidx){
			datafn(im)[map[field]]=id;
			datafn(im).current=field-0+1;
			if(field=="3")datafn(im).selectfn(viewData.childerns[sidx]);
		}
		function _render(){
			render(im,data);
		}
		ui.gpipes(div(im),{
			set:set,
			set1:set1,
			render:_render
		});
	}
	function make(div,config){
		if(globalreq==null)globalreq=req.cachePromise(req.reqp,"/home/api/payment/getStoreList");
		function r(flag){
			return {div:div,data:config}[flag];
		}
		init(r);
		return r;
	}
	exports.gstore={
		make:u.curry(make)
	};
}(this,$,request,$page,util,util_ui,GTPL));