;(function(exports) {
    $page.cookieDomain=cookieDomain;
    //是否独立站点
    function isOnlySite(){
        if($page.site=="home") return false;
        return true;
    }
    //不同站点 显示不同的券
    function supportPre(){
        if($page.site=="home")return "红券/蓝券/店铺券";
        if($page.site=="haiwaigou")return "店铺券";
        return "红券";
    }
    //是否显示大家电配送时部分偏远地区需额外收取远程费 提示
    function listOfitemTipVisible(){
        if($page.site=="gomeVirtualCard")return false;
        return true;
    }
    //是否显示返回购物车修改连接
    function backCardLink(){

        if($page.site=="allowance")return false;
        if($page.site=="gomeEntityCard")return false;
        if($page.site=="gomeVirtualCard")return false;
        return true;
    }
    //是否禁用设置默认地址
    function isDisabledDefaultAddress(a){
        if(a)return true;
        if($page.site=="allowance") return true;
        return false;
    }
    //是否禁用门店
    function isDisabledMD(){
        if($page.site=="home") return false;
        if($page.site=="allowance") return true;
        return true;
    }
    //禁用激活
    function isDisabledJH(){
        if($page.site=="haiwaigou") return true;
        if($page.site=="allowance") return true;
        if($page.site=="presell") return true;
        return false;
    }
    //收获人是否只读
    function isReadOnlyConsignee(){
        return $page.site=="allowance";
    }
    //格式化一段文本
    function renderText(data, text){
        text = (text || '').toString();
        return text.replace(/\{([^\}]+)\}/g, function(all, match){
            result = data;
            matchArr = match.split('.');
            for(var i=0,key; key=matchArr[i++]; ){
                var tmp=key.split("|");
                if(tmp.length==2){
                    result = $config[tmp[1]](result[tmp[0]]);
                }else{
                    result = result[key];
                }
                if(result===null){ 
                    return all;
                }
            }
            return result;
        });
    }
    function fromatDate(date1,yyyyMMdd){
        if(typeof date1 !=="object") date1=new Date(+date1);
        var month=date1.getMonth()+1
        ,date=date1.getDate()
        ,hours=date1.getHours()
        ,min=date1.getMinutes()
        ,sec=date1.getSeconds();

        return yyyyMMdd.replace(/yyyy/g,date1.getFullYear())
        .replace(/yy/g,String(date1.getFullYear()).substr(2,2))
        .replace(/MM/g,month>=10?month:"0"+month)
        .replace(/M\*/g,month)
        .replace(/dd/g,date>=10?date:"0"+date)
        .replace(/d\*/g,date)
        .replace(/hh/g,hours>=10?hours:"0"+hours)
        .replace(/h\*/g,hours)
        .replace(/m\*/g,min)
        .replace(/mm/g,min>=10?min:"0"+min)
        .replace(/ss/g,sec>=10?sec:"0"+sec)
        .replace(/s\*/g,sec)
    }
    //日期格式化
    function formathhmm(time){
        return fromatDate(new Date(time),"hh:mm");
    }
    //日期格式化
    function formatLong(time){
        return fromatDate(new Date(time),"yyyy-MM-dd hh:mm:ss");
    }
    function renderError(errorData){
        return renderText(
            errorData,
            $config.errorText[errorData.errCode]||errorData.errMsg
        );
    }
    function renderNotice(key,data){
        return $config.renderText(data,$config.notice[key]);
    }
    //购物车促销提示语
    function renderCartPromtionNotice(data){
        var x="";
        if(data.type=="GOME_JIA_JIA_HUAN_GOU"){
            var jjhgNum=util.filter(util.prop("selected"),data.jjhgCiVOs).length;
            data.jjhgNum=jjhgNum;
            if(jjhgNum) data.type=data.type+"_HG";
        }
        if(data.satisfied){//促销生效
            x= $config.renderText(data,$config.notice["C$"+data.type+"_SATISFIED"]);
        }else{
            x= $config.renderText(data,$config.notice["C$"+data.type]);
        }
        if(x)return x;
        return "";
    }
    function renderCartNotice(key,data){
        var x=$config.renderText(data,$config.notice["C$"+key]);
        if(x)return x;
        return "";
    }

    //格式化金额
    function formatAmount(amount){
        if(amount===null)return "";
        if(amount==="")return "";
        return Number(amount).toFixed(2);
    }
    //格式化券
    function formatCoupon(coupon){
        var r=[];
        for(var i=0;i<coupon.length;i++){
            var item=coupon[i];
            r.push(formatAmount(item.faceValue)+"元"+$config.labels[item.couponType]+"x"+item.quantity);
        }
        return "可返"+r.join(",");
    }
    //加价换购提示语
    function jjhgtip(temp){
        if(temp)return "您已换购了商品";
        return "您未换购商品";
    }
    //店铺提示语
    function shopTip(shop){
        var tiparr=[];
        if(shop.reduce&&shop.reduce!=0)tiparr.push("已节省<span class='font-cursive'>¥</span>"+formatAmount(shop.reduce));
        if(shop.couponNum&&shop.couponNum!=0)tiparr.push("返优惠券x"+shop.couponNum);
        if(shop.zpCount&&shop.zpCount!=0)tiparr.push("赠品x"+shop.zpCount);
        return tiparr.join(" ， ");
    }
    //是否禁用电子券按钮(现已去掉这个功能如需打开模版注释即可)
    function isDisableDZQ(){
        if($page.site=="home")return false;
        return true;
    }
    //限制字符个数 超过后显示...
    function limitStr(str,n){
        if(str&&str.length<n)return str;
        return str.substr(0,n)+'...';
    }
    //使用国美在线积分是否禁用
    function isDisableSYGMZXJF(a){
    	if(a.op.vase=="FREEZED")return true;
    	if(a.op.vase=="EXCEPTION")return true;
    	if(a.op.currentPoint==0)return true;
    	return false;
    }
    //禁用提交订单
    function isDisabledTJDD(){
        if($page.site=="presell"){
            if($config.shoppingAtom.presell_tyzfdj){
                return payment.isTailPhoneStatusDone($config.shoppingInstenceAtom.payment)==false;
            }
            return true;
        }
        return false;
    }
    //密文手机号
    function pwdPhone(a){
        if(a==null)return "";
        var b=util.take(4,util.reverse(a));
        return util.take(3,a)+"****"+util.reverse(b);
    }
    //密文电话号码
    function pwdTelphone(a){
       if(a==null)return "";
       if(a.length < 5) return "****"
       var b=util.take(3,util.reverse(a));
       //var c = util.take(7,util.reverse(a));
       //var d = c.substr(3,4);
       //var e = util.reverse(d);
       var f = a.length -7;
       var g = a.substr(0,f);
       return g+"****"+util.reverse(b);
    }
    //渲染购物车标签
    function renderCartLabel(type,defaultlabel){
        var x=$config.labels["C$"+type];
        if(x)return x;
        return defaultlabel;
    }
    //是否虚拟卡站点
    function isGomeVirtualCardSite(){
        return $page.site=="gomeVirtualCard";
    }
    //是否实体卡站点
    function isGomeEntityCardSite(){
        return $page.site=="gomeEntityCard";
    }

    //是否海外购
    function isHwgSite(){
        return $page.site=="haiwaigou";
    }
    //控制页面中是否显示逻辑
    var VBLE={
        youhuiquan:function(){ //使用优惠券
            if(isGomeVirtualCardSite())return false;
            if(isGomeEntityCardSite())return false;
            return true;
        },
        shiyongjifen:function(){//使用积分
            if(isGomeEntityCardSite())return false;
            if(isGomeVirtualCardSite())return false;
            if(isHwgSite())return false;
            return true;
        },
        shiyongzhanghuyue:function(){//使用账户余额
            if(isHwgSite())return false;
            return true;
        },
        shiyongtuijianhao:function(){//使用推荐号
            if(isGomeEntityCardSite())return false;
            if(isGomeVirtualCardSite())return false;
            return true;
        },
        songhuorenxinxi:function(){ //价格展示下面的 送货人信息
            if(isGomeVirtualCardSite())return false;
            return true;  
        },
        shiyongguomeiE:function(){//美通卡
            if($page.site=="home") return true;
            return false; 
        },
        beizhu:function(){//送货清单-备注
            if(isGomeEntityCardSite())return false;
            return true;
        } , //价格展示栏  使用积分是否显示
        jifen:function(){
            return VBLE.shiyongjifen();
        },//价格展示栏 优惠券
        youhuiquan2:function(){
            return VBLE.youhuiquan();
        }
    }
    //URL配置
    var URL={
        cart: "/",
        shopping: "/shopping",
        orderSuccess: "/order-success",
        authorization:"/haiwaigou/read/authorization",
        moblieActive:"http://myhome"+$page.cookieDomain+"/member/accountSecurity?atc=atc",//手机认证
        discountUseRule:"http://help.gome.com.cn/article/279-0-0.html",//优惠券使用规则
        electroniccouponsRule:"http://help.gome.com.cn/question/23.html",//了解电子券
        integralRule:"http://help.gome.com.cn/article/281-0-0.html", //了解积分
        accountbalanceRule:"http://help.gome.com.cn/article/239-0-0.html",//了解账户余额 
        homeinstallation:"http://help.gome.com.cn/question/25.html",//安装说明
        invoiceRule:"http://help.gome.com.cn/article/238-0-0.html",//了解发票制度
        increaseticket:"http://myhome"+$page.cookieDomain+"/member/myInvoice",//申请增票认证
        paymentpassword:"http://myhome"+$page.cookieDomain+"/member/accountSecurity?atc=atc",//开启支付密码  
        dispatchQuery:"http://help.gome.com.cn/question/9.html" //配送查询
        ,imgcode:"http://imgcode"+$page.cookieDomain+"/getimage.no?type=gome_cart"//验证码
        ,imgCodeMTK:"http://imgcode"+$page.cookieDomain+"/getimage.no?type=gome_profile_center"
        ,forgetPassword:"http://myhome"+$page.cookieDomain+"/member/passwordReset"//忘记密码
    }
    var $config = exports.$config = {
        zIndex: {
            general: 10, //普通级别
            mask: 100, //遮罩级别
            dialog: 1000 //对话框级别
        },
        URL:URL,
        notice:{
            "addressConfirm":"您的收货人信息已经超过20条，我们将替换您最早的一条收货信息！"
            //促销类型
        	,"GOME_DA_PEI_GOU":'搭配购优惠组合'
            ,"GOME_MAN_ZHE":'已购满{preCount}件，<em class="fontRed">已减{reduce|formatAmount}元</em>'
            ,"GOME_MAN_JIAN":'已购满{preCondition|formatAmount}元，<em class="fontRed">已减{reduce|formatAmount}元</em>'
            ,"GOME_MAN_FAN":'已购满{preCondition|formatAmount}元，<em class="fontRed">下单{returnedCouponVOs|formatCoupon}</em>'
            ,"GOME_MAN_ZHENG":'已购满{preCondition|formatAmount}元，您已经领取了赠品'
            ,"GOME_JIA_JIA_HUAN_GOU":'已购满{preCondition|formatAmount}元&nbsp;&nbsp;<em class="fontRed">{hasSubs|jjhgtip}</em>'
            //***
            ,"YHQ":'（'+supportPre()+'/国美电器门店积分）<em class="squaer pr">{availableCouponNum}<i class="pabs c-i triangle_icon2">'
            ,"fp_tip_2":"增值税发票在订单完成确认收货15个工作日后开具并邮寄。"
            ,"fp_tip_0":"接到税务局通知，近期将取消提供纸质冠名发票，全部使用电子发票。"
            ,"fp_tip_1":"电子发票是增值税电子普通发票的简称，是税局认可的有效凭证。<br/>其法律效力、基本用途、基本使用规定等与纸质普通发票相同，可作为用户报销、维权、保修的有效凭据。<br/>如您本次购买的商品暂未实现电子发票开具，我们将自动更换为纸质发票。"
            /********购物车提示语**********/
            ,"C$GOME_MAN_JIAN":"活动商品满{preCondition|formatAmount}元，即可享受满减"
            ,"C$GOME_MAN_FAN":"活动商品满{preCondition|formatAmount}元，即可享受满返"
            ,"C$GOME_MAN_ZHE":"活动商品满{preCondition}件，即可享受满折"
            ,"C$GOME_MAN_ZENG":"" //满赠已废弃
            ,"C$GOME_DA_PEI_GOU":"搭配购优惠组合"
            ,"C$GOME_JIA_JIA_HUAN_GOU":"活动商品满{preCondition|formatAmount}元，即可享受换购"
            ,"C$GOME_MAN_ZENG_SATISFIED":""
            ,"C$GOME_MAN_JIAN_SATISFIED":'活动商品已满{preCondition|formatAmount}元（<span style="color:#e6060e;">已减{postCondition|formatAmount}</span>）'
            ,"C$GOME_MAN_FAN_SATISFIED":'活动商品已满{preCondition|formatAmount}元（<span style="color:#e6060e;">{returnedCouponVOs|formatCoupon}</span>）'
            ,"C$GOME_MAN_ZHE_SATISFIED":'活动商品已满{preCondition}件（<span style="color:#e6060e;">已减{postCondition|formatAmount}元</span>）'
            ,"C$GOME_JIA_JIA_HUAN_GOU_SATISFIED":'活动商品已满{preCondition|formatAmount}元（<span style="color:#e6060e;">可加价换购商品{maxNum}件</span>）'
            ,"C$GOME_JIA_JIA_HUAN_GOU_HG_SATISFIED":'活动商品已满{preCondition|formatAmount}元（<span style="color:#e6060e;">已加价换购商品{jjhgNum}件</span>）'
            ,"C$GOME_DA_PEI_GOU_SATISFIED":"搭配购优惠组合"
        }
        ,labels:{
            /***********订单信息页****************/
            "Gome Express":"国美快递",
            "Gome Picking Up":"门店自提",
            "EMS":"EMS快递",
            "Express":"普通快递",
            "cash":"现金",
            "pos":"pos刷卡",
            "counter":"柜台",
            "enterpriseBank":"企业银行",
            //促销类型
            "GOME_MAN_FAN":"满返",
            "GOME_MAN_ZHE":"满折",
            "GOME_MAN_JIAN":"满减",
            "GOME_DA_PEI_GOU":"搭配购",
            "GOME_MAN_ZHENG":"满赠",
            "GOME_JIA_JIA_HUAN_GOU":"加价购"
            //************
            ,"SOME_UP":"与上述商品付款方式一致"
            ,"ZKP":"【折扣品】"
            ,"ZP":"【赠品】"
            ,"HGP":"【换购品】"
            ,"WEEKDAY":"工作日送"
            ,"ALL":"工作日、双休日与假日送"
            ,"WEEKEND":"双休日、假日送"
            //券类型
            ,"REDCOUPON":"红券"
            ,"BLUECOUPON":"蓝券"
            ,"SHOPCOUPON":"店铺券"
            ,"POINT":"积分"
            ,"PLATFORMCOUPON":"店铺平台券"
            ,"SHI_WU":"实物"
            //库存状态
            ,"ON_THE_ROAD":"" //在途状态
            ,"NO_GOODS":'<span class="fontRed">无货</span>'
            ,"INVENTORY_TENSION":'<span class="fontRed">库存紧张</span>'
            ,"OFF":'<span class="fontRed">已下架</span>'
            ,"IN_STOCK":"有货"
            ,"DELIVERY_NOT_SUPPORTED":'<span class="fontRed">该区域暂不支持配送</span>'
            /********购物车页的标签*********/
            ,"C$GOME_MAN_JIAN":"满减"
            ,"C$NPOP_SHOP_MAN_JIAN":"满减"
            ,"C$GOME_MAN_FAN":"满返"
            ,"C$NPOP_SHOP_MAN_FAN":"满返"
            ,"C$GOME_MAN_ZENG":"满赠"
            ,"C$NPOP_SHOP_MAN_ZENG":"满赠"
            ,"C$GOME_JIA_JIA_HUAN_GOU":"换购"
            ,"C$NPOP_SHOP_JIA_JIA_HUAN_GOU":"加价购"
            ,"C$GOME_MAN_ZHE":"满折"
            ,"C$NPOP_SHOP_MAN_ZHE":"多买优惠"
            ,"C$GOME_DA_PEI_GOU":"搭配购"
            ,"C$NPOP_SHOP_GOUWUQUAN":"购物券"
            ,"C$NPOP_KDP_MJ":"跨店铺满减"
            ,"C$NPOP_KDP_MM":"跨店铺满免"

        }
        ,renderNotice:renderNotice
        ,renderCartNotice:renderCartNotice
        ,renderCartPromtionNotice:renderCartPromtionNotice
        ,renderText: renderText
        ,renderError: renderError
        ,formatLong:formatLong
        ,fromatDate:fromatDate
        ,formathhmm:formathhmm
        ,formatAmount:formatAmount
        ,formatCoupon:formatCoupon
        ,defaultErrorText: '出错了'
        ,isDisableDZQ:isDisableDZQ
        ,isDisableSYGMZXJF:isDisableSYGMZXJF
        ,isOnlySite:isOnlySite
        ,backCardLink:backCardLink
        ,listOfitemTipVisible:listOfitemTipVisible
        ,jjhgtip:jjhgtip
        ,isDisabledDefaultAddress:isDisabledDefaultAddress
        ,isReadOnlyConsignee:isReadOnlyConsignee
        ,shopTip:shopTip
        ,isDisabledMD:isDisabledMD
        ,isDisabledJH:isDisabledJH
        ,limitStr:limitStr
        ,pwdPhone:pwdPhone
        ,pwdTelphone:pwdTelphone
        ,isGomeVirtualCardSite:isGomeVirtualCardSite
        ,isDisabledTJDD:isDisabledTJDD
        ,renderCartLabel:renderCartLabel
        ,VBLE:VBLE
        ,errorText:{
            "003018000":'为保障您的账户资金安全，请先 [<a target="_blank" href="'+URL.moblieActive+'">验证手机号</a>]'
            ,"f1":'<p style="color:#f00;">为了保障您的账户资金安全，此优惠暂不可用，请先&nbsp;&nbsp;&nbsp;<a target="_blank" href="'+URL.paymentpassword+'">开启支付密码<span class="jt" style="padding-left:4px;">&gt;</span></a></p>'
        }
        ,shoppingAtom:{//核对订单页->用户资产->全局状态
            yhj:"N",//优惠券展开与否
            yhj_jh:"N",//激活优惠券展开
            lq_more:"Y",//蓝券更多
            dpq_more:"Y",//店铺券更多
            dzq_more:"Y",//电子券更多操作
            hq_more:"Y",//红券更多
            syjf:"N",//使用积分展开
            sydzq:"N",//使用电子券展开
            sygmek:"N",//使用国美E卡
            syzhye:"N",//使用账户余额展开
            sytjh:"N"//推荐号展开
            ,yhj_bm:""//优惠券编码
            ,yhj_jhm:""//优惠券激活码
            ,yhj_tip:""//激活优惠券错误提示
            ,yhj_yzm:""//优惠券验证码
            ,yhj_img:new Date-1//优惠券验证码图片
            ,mdhyjf:null//门店会员积分
            ,dzqma:""//电子券密码
            ,rygh:""//人员工号
            ,rygh_tip:""
            ,yzm:""//支付密码 验证码
            ,yzm_pw:""//支付密码
            ,yzm_img:new Date-0//支付密码 验证码
            ,yzm_tip:""//支付密码验证码错误提示
            ,mdhyjf_tip:""//门店会员积分 错误提示
            ,referrerInfo:null//门店推荐人员工号
            // ,referrerInfo:{
            //     employeeId:1000,
            //     employeeName:"employeeName",
            //     structureName:"structureName"
            // }
            ,presell_tyzfdj:false//预售站点 提交订单旁边同意支付定金单选框
            ,deliveryPreSell:null//预售站点尾款手机号信息
            ,ecard_1:""//绑定国美E卡1
            ,ecard_2:""//绑定国美E卡2
            ,ecard_3:""//绑定国美E卡3
            ,ecard_4:""//绑定国美E卡4
            ,ecard_yzm:""//绑定国美E卡 验证码
            ,ecard_yzm_tip:""
        }
        ,shoppingAddressAtom:{//核对订单页->收获地址->全局状态
            more:"Y"//收获地址更多按钮 展开与否
            ,scrollY:0//收获地址滚动条
        }
        ,shoppingInstenceAtom:{//核对订单页->实例
            address:null //收获地址
            ,payment:null //支付方式
            ,invoice:null //发票
            ,listOfItem:null //配送清单
            ,commitOrder:null//价格
            ,preferential:null //用户资产
        }
        ,cartAtom:{//购物车状态
            limitTip:null
        }

    }; 
}(this));
