;(function($){

	//ie678 hack
	if($.browser.msie){ 
		if($.browser.version-0<9){
			$("body").addClass("ie678");
		}
	}
	var doc=$(document);
	//白色按钮
	doc.on("mousedown",function(e){
		var btn=$(e.target);
		if(btn.is(".btn.btn-default")){
			btn.removeClass("mousedown").addClass("mousedown");
		}
	})
	.on("mouseup",function(e){
		var btn=$(e.target);
		if(btn.is(".btn.btn-default")){
			btn.removeClass("mousedown");
		}
	});
	function domOrParentHasAttr(dom,attr){
		var attrv=dom.getAttribute(attr);
		
		if(attrv!==null)return true;
		if(dom.parentElement==null){
			return false;
		}
		return domOrParentHasAttr(dom.parentElement,attr);
	}
	//点击其它隐藏
	doc.on("click",function(e){
		if(domOrParentHasAttr(e.target,"click-document-pre"))return;
		if(domOrParentHasAttr(e.target,"click-document-hide"))return;
		$("[click-document-hide]").hide();
	});
	//关闭按钮
	$(".c-i.closebtn-new").live("mousemove",function(){
		var a=$(this);
		a.removeClass("closebtn-new").addClass("closebtn-hover");
	});
	$(".c-i.closebtn-hover").live("mouseout",function(){
		var a=$(this);
		a.removeClass("closebtn-hover").addClass("closebtn-new");
	});
	//问号 tips_icon
	$(".c-i.tips_icon").live("mousemove",function(){
		var a=$(this);
		a.removeClass("tips_icon").addClass("tips_icon-hover");
	});
	$(".c-i.tips_icon-hover").live("mouseout",function(){
		var a=$(this);
		a.removeClass("tips_icon-hover").addClass("tips_icon");
	});
	//左右滚动交互
	$(".c-i.arrow-left").live("mousemove",function(){
		var a=$(this);
		a.removeClass("arrow-left").addClass("arrow-left-over");
	});
	$(".c-i.arrow-left-over").live("mouseout",function(){
		var a=$(this);
		a.removeClass("arrow-left-over").addClass("arrow-left");
	});
	$(".c-i.arrow-right").live("mousemove",function(){
		var a=$(this);
		a.removeClass("arrow-right").addClass("arrow-right-over");
	});
	$(".c-i.arrow-right-over").live("mouseout",function(){
		var a=$(this);
		a.removeClass("arrow-right-over").addClass("arrow-right");
	});
}($));