(function(exports,$,u){
	function error(str){
		return function(el){
			el.html('<span class="nError nRed animated flash">'+str+'</span>').show();
			return false;	
		}
	}
	function errcbk(str){
		return function(cbk){
			cbk(str);
			return false;
		}
	}
	//最大长度限制
	var maxLimit=u.curry(function(max,str){
		return str.length>max;
	});
	var minLimit=u.curry(function(min,str){
		return str.length<min;
	});

	//判断是否为空
	function isBlank(str){
		if(str==null)return true;
		if(str=="")return true;
		if(u.head(str)==" ")return isBlank(u.tail(str));
		return false;
	}
	//非汉字英文和数字空格
	function notEnNumCn(str){
		return !(/^[\u4E00-\u9FFF|0-9|a-z|A-Z|\(|\)|（|）|\s]+$/.test(str));
	}
	//非数字大写英文字母
	function notUpenNum(str){
		return !(/^[0-9|A-Z]+$/.test(str));
	}
	function length(str){
		return str.length;
	}
	function not(a){
		return !a;
	}
	function notLength(l){
		return u.pipe(length,u.eq(l),not)
	}
	function eqLength(l){
		return u.pipe(length,u.eq(l))
	}
	function notNum(str){
		return !(/^[0-9]+$/.test(str));
	}
	//非身份证号码
	function valiedateCardInput(idCard) {
        //var reg = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X|x)$/;
        //15位和18位身份证号码的正则表达式
        var regIdCard=/^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;

        //如果通过该验证，说明身份证格式正确，但准确性还需计算
        if(regIdCard.test(idCard)){
            if(idCard.length==18){
                var idCardWi=new Array( 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ); //将前17位加权因子保存在数组里
                var idCardY=new Array( 1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2 ); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
                var idCardWiSum=0; //用来保存前17位各自乖以加权因子后的总和
                for(var i=0;i<17;i++){
                    idCardWiSum+=idCard.substring(i,i+1)*idCardWi[i];
                }

                var idCardMod=idCardWiSum%11;//计算出校验码所在数组的位置
                var idCardLast=idCard.substring(17);//得到最后一位身份证号码

                //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
                if(idCardMod==2){
                    if(idCardLast=="X"||idCardLast=="x"){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                    if(idCardLast==idCardY[idCardMod]){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
        } else {
            return false;
        }

    }
	//非英文、数字、汉字、井号（#）、大中小括弧、逗号、句号、分隔符（|）、点
	function notAddress(str){
		return !(/^[\u4E00-\u9FFF|0-9\-|a-z|A-Z|\s|#\{\}\(\)\[\],\.。，（）｛｝【】\|]+$/.test(str));
	}
	//非汉字
	function notCn(str){
		return !(/^[\u4E00-\u9FFF]+$/.test(str));	
	}
	function notEmail(str){
		return !( /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(str));	
	}
	//手机开头
	function phoneStart(str){
		if(str=="13")return true;
		if(str=="14")return true;
		if(str=="15")return true;
		if(str=="17")return true;
		if(str=="18")return true;
		return false;
	}
	var config={
		consignee:[
			[isBlank,errcbk("请输入收货人姓名")],
			[notEnNumCn,errcbk("请输入正确的收货人姓名格式")],
			[maxLimit(10),errcbk("请输入正确的收货人姓名格式")]
		],
		address:[
			[isBlank,errcbk("请选择所在地")]
		],
		detailAddress:[
			[isBlank,errcbk("请填写详细地址")],
			[minLimit(2),errcbk("请填写更详细的地址")],
			[maxLimit(100),errcbk("详细地址为100个字符以内")],
			[notAddress,errcbk("输入格式不正确")]
		],
		"phone-call":[
			[u.all(isBlank),errcbk("请输入电话号码")]
		],
		"phone":[
			[isBlank,u.T],
			[u.pipe(length,u.eq(11),not),errcbk("请输入正确的手机号码")]
			,[notNum,errcbk("请输入正确的手机号码")]
			,[u.pipe(u.take(2),phoneStart,not),errcbk("请输入正确的手机号码")]
		],
		"mtk-phone":[
			[isBlank,errcbk("请输入手机号")]
			,[u.pipe(length,u.eq(11),not),errcbk("请输入正确的手机号码")]
			,[notNum,errcbk("请输入正确的手机号码")]
			,[u.pipe(u.take(2),phoneStart,not),errcbk("请输入正确的手机号码")]
		],
		"call":[
			[isBlank,u.T]
			,[notNum,errcbk("请输入正确的电话号码")]
			,[minLimit(10),errcbk(" 固定电话号码不能低于10位")]
			,[maxLimit(18),errcbk(" 固定电话号码不能大于18位")]
		],
		"email":[
			[isBlank,u.T]
			,[notEmail,errcbk("请输入正确的电子邮箱")]
		],
		"fptt":[//发票抬头
			[isBlank,errcbk("请输入抬头内容")]
			,[notEnNumCn,errcbk("抬头内容输入格式不正确")]
			,[maxLimit(50),errcbk("抬头为50个字符以内")]
		],
		"spsj":[//收票手机
			[isBlank,errcbk("请输入手机号码")]
			,[u.pipe(length,u.eq(11),not),errcbk("请输入正确的手机号码")]
			,[notNum,errcbk("请输入正确的手机号码")]
			,[u.pipe(u.take(2),phoneStart,not),errcbk("请输入正确的手机号码")]
		],
		"spyx":[//收票邮箱
			[isBlank,u.T]
			,[notEmail,errcbk("请输入正确的邮箱")]
		],
		"gfsh":[//购方税号
			[isBlank,u.T]
			,[notUpenNum,errcbk("购方税号为数字和大写英文字母")]
			,[u.anyPass([eqLength(15),eqLength(18),eqLength(20)]),u.T]
			,[u.T,errcbk("购方税号为15位、18位、20位数字和大写英文字母")]
		],
		"zpsjr":[//增票收件人
			[isBlank,errcbk("请输入增票收件人")]
			,[notEnNumCn,errcbk("收件人输入格式不正确")]
			,[maxLimit(20),errcbk("收件人名称最多20个字符（数字、英文、汉字各占一个字符）")]
		],
		"zpsjhm":[//赠票手机号码
			[isBlank,errcbk("请输入手机号码")]
			,[u.pipe(length,u.eq(11),not),errcbk("请输入正确的手机号码")]
			,[notNum,errcbk("请输入正确的手机号码")]
			,[u.pipe(u.take(1),u.eq("1"),not),errcbk("请输入正确的手机号码")]
		],
		"zzshr":[//电子卡纸质发票详细地址
			[isBlank,errcbk("请输入收货人姓名")],
			[notEnNumCn,errcbk("请输入正确的收货人姓名格式")],
			[maxLimit(10),errcbk("请输入正确的收货人姓名格式")]
		],
		"zzxxdz":[//电子卡纸质发票详细地址
			[isBlank,errcbk("请填写详细地址")],
			[minLimit(2),errcbk("请填写更详细的地址")],
			[maxLimit(100),errcbk("详细地址为100个字符以内")],
			[notAddress,errcbk("输入格式不正确")]
		],
		"zzsjhm":[//电子卡纸质发票手机号码
			[isBlank,errcbk("请输入手机号码")]
			,[u.pipe(length,u.eq(11),not),errcbk("请输入正确的手机号码")]
			,[notNum,errcbk("请输入正确的手机号码")]
			,[u.pipe(u.take(1),u.eq("1"),not),errcbk("请输入正确的手机号码")]
		],
		"zzgddh":[//电子卡纸质发票固定电话号码
			[isBlank,u.T]
			,[notNum,errcbk("请输入正确的电话号码")]
			,[minLimit(10),errcbk(" 固定电话号码不能低于10位")]
			,[maxLimit(18),errcbk(" 固定电话号码不能大于18位")]
		],
		"zzyj":[//电子卡纸质发票邮箱
			[isBlank,u.T]
			,[notEmail,errcbk("请输入正确的邮箱")]
		],
		"zpyjdz":[//邮寄地址
			[isBlank,errcbk("请输入邮寄地址")]
			,[minLimit(2),errcbk("请填写更详细的地址")]
			,[maxLimit(100),errcbk("详细地址最多100个字符")]
			,[notAddress,errcbk("输入格式不正确")]
		],
		"mdhyjf":[//使用国美门电器门店会员积分 兑换
			[u.pipe(u.prop("txt"),isBlank),errcbk("请输入门店会员积分")]
			,[u.pipe(u.prop("txt"),parseFloat,u.gte(0)),errcbk("请输入正确的门店积分")]
			,[u.pipe(u.prop("txt"),notNum),errcbk("请输入整数")]
			,[u.pipe(u.juxt([
				u.pipe(u.prop("txt"),parseInt),
				u.pipe(u.prop("max"),parseInt)
				]), u.apply(u.lte),not),
			errcbk("门店会员积分不能大于本次最多可使用积分")]
		],
		"hwg-name":[//海外购 姓名
			[u.anyPass([isBlank,minLimit(2),maxLimit(20),notCn]),errcbk("请您填写真实姓名")]
		],
		"hwg-card":[//海外购 身份证
			[isBlank,errcbk("请输入正确的身份号")]
			,[u.pipe(valiedateCardInput,not),errcbk("请输入正确的身份号")]
		],
		"presell-phone":[//预售尾款手机号验证
			[isBlank,errcbk("请输入手机号码")]
			,[u.pipe(length,u.eq(11),not),errcbk("请输入正确的手机号码")]
			,[notNum,errcbk("请输入正确的手机号码")]
			,[u.pipe(u.take(2),phoneStart,not),errcbk("请输入正确的手机号码")]
		]
	}
	function vacond(conds,str,fillel){
		if(u.is(Function,fillel)) fillel("");
		else fillel.html("").hide();
		var cnd=u.head(conds);
		if(cnd==null) return true;
		if(u.head(cnd)(str)){
			return u.last(cnd)(fillel);
		}
		return vacond(u.tail(conds),str,fillel);
	}
	function validate(vaFillelkeys){
		function vakey(key){
			return vacond(config[key[1]],u.head(key),u.last(key));
		}
		return u.pipe(u.map(vakey),u.all(u.eq(true)))(vaFillelkeys);
	}
	function validateItem(key,str,fillel){
		return validate([[str,key,fillel]]);
	}
	exports.validate={
		validate:validate,
		validateItem:validateItem,
		vacond:vacond
	}
}(this,$,util))