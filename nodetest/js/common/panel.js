!function(exports,u,ui,tpl){

	function confirm(data){
		data.title=data.title||false;
		var dialog = $.gDialog({
			html: tpl.comp_confirm(data),
			zIndex:10000,
			modal:{
				zIndex:1000
			}
		});
		dialog.show();
		var div=dialog.$dialog;

		//点击按钮
		div
		.find("[g-btn-path]")
		.on("click",function(){
			var path=$(this).attr("g-btn-path");
			var fnfn=u.pipe(u.split(","),u.concat(u.__,["click"]),u.path(u.__,data));
			var fn=fnfn(path);
			return fn(dialog);
		});

		//点击关闭
		div
		.find("[g-panel-close]")
		.on("click",function(){
			data.close(dialog);
		});
		return dialog;
	}


	function alertClose(html,fn){
		var body=u.ifn(u.is(Object),u.identity,u.always({
			type:TYPE.WARN,
			content:html
		}))(html);
		var dialog = $.gDialog({
			html: tpl.comp_alert({
				close:true,
				type:body.type,
				body:body.content
			}),
			zIndex:10000,
			modal:{
				zIndex:1000
			}
		});
		dialog.show();
		dialog.$dialog.find("[g-panel-close]").on("click",function(){
			return fn(dialog);
		});
	}
	function alert(html){
		return alertClose(html,ui.hide);
	}

	function error(html){
		return alert({type:TYPE.ERROR,content:html});
	}
	function success(html){
		return alert({type:TYPE.SUCCESS,content:html});
	}
	function confirmOKCanel(type,html,okfn,canelfn){
		var title=false;
		if(u.is(Object,html)){
			title=html.title;
			html=html.body;
		}
		return confirm({
			type:type,
			title:title,
			body:html,
			btns:[{
				clazz:"btn btn-primary",
				btnName:"确定",
				click:okfn
			},{
				clazz:"btn btn-default",
				btnName:"取消",
				click:canelfn
			}],
			close:canelfn
		})
	}
	var c_confirmOkCanel=u.curry(confirmOKCanel);

	var dialog=null;
	var dialog_show=false;
	var dialog_time=null;
	var dialog_num=0;
	var dialog_delay=200;
	function maskHide(){
		// if(dialog_num>0)dialog_num--;
		// if(dialog_num>1)return;
		// clearTimeout(dialog_time);
		// if(dialog&&dialog_show)dialog.hide();
		// dialog_show=false;
	}
	function mask(){
		// dialog_num++;
		// if(dialog_show)return;
		// function show(){
		// 	dialog = $.gDialog({
		// 		html: '<div class="loading animated flipInX" style="width:100px;height:100px;"></div>',
		// 		zIndex:10000,
		// 		modal:{
		// 			opacity:0.2,
		// 			backgroundColor:"#FFFFCC"
		// 		}
		// 	});
		// 	dialog.show();
		// }
		// dialog_time=ui.delay(dialog_delay,show,null,dialog_time);
		// dialog_show=true;

	}
	var TYPE={
		ERROR:"error",
		WARN:"warn",
		SUCCESS:"success"
	}
	exports.panel={
		confirm:confirm,
		alertClose:alertClose,
		TYPE:TYPE,
		alert:alert,
		error:error,
		success:success,
		mask:mask,
		maskHide:maskHide,
		confirmOKCanelType:c_confirmOkCanel,
		confirmOKCanel:c_confirmOkCanel(TYPE.WARN),
		confirmOk:c_confirmOkCanel(TYPE.WARN,u.__,u.__,ui.hide)
	}
}(this,util,util_ui,GTPL);