;/**
 * 节能补贴登陆控件
 *
 **/

window.g = window.g || {};

//#region g.url
/*
 各个域名在公共头中已经定义
 * g.url.w3 www域名
 * g.url.g g域名
 */
(function () {
    var $ = jQuery;
    window.g = window.g || {};

    //有些公共头定义的公共变量 多了一个右斜线
    //这个函数的功能是把这个右斜线去掉
    function del_youXieXian(str) {
        if (str.substr(str.length - 1, 1) == "/") {
            return str.substr(0, str.length - 1);
        } else {
            return str;
        }
    }

    var url = {
        w: del_youXieXian(staSite),
        item: del_youXieXian(staSite.replace('www', 'item')),
        //cookieDomain: del_youXieXian(cookieDomain),
        js: del_youXieXian(stageJsServer),
        css: del_youXieXian(stageCssServer),
        img: del_youXieXian(stageImageServer),
        login: del_youXieXian(staSite.replace('www', 'login')),
        reg:del_youXieXian(staSite.replace('www', 'reg')),
        //w3: staSite,
        desc: del_youXieXian(staSite.replace('www', 'desc')),//'http://desc.atguat.com.cn'
        contextPath: contextPath,
        getParam: function (param) {
            var result = {};

            if (location.search.indexOf("?") != -1) {

                var str = location.search.substr(1);

                strs = str.split("&");

                for (var i = 0; i < strs.length; i++) {

                    if (strs[i].split('=').length !== 2) {//过滤不规则url
                        continue;
                    }
                    result[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
                }
            }

            if (result[param] == undefined) {
                result[param] = "";
            }

            return result[param];
        },
        g: del_youXieXian(dynSite)
    }
    g.url = url;
})();
//#endregion

//#region g.ajax
; (function () {
    var $ = jQuery;
    ajax = function () {

        if ((typeof arguments[0]) != "string") {
            return;
        }

        var url = undefined;
        var data = {};
        var success = undefined;
        var ajaxConfig = {};

        //判断输入的参数
        if (arguments.length == 1) {//g.ajax('a.jsp')
            url = arguments[0];
            data = {};
            //success = function () { };
        } else if (arguments.length == 2) {//g.ajax('a.jsp',{})
            if ((typeof arguments[1]) == "function") {
                url = arguments[0];
                success = arguments[1];
            } else {
                url = arguments[0];
                data = arguments[1];
            }
        } else if (arguments.length == 3 && (typeof arguments[2]) == "function") {  //g.ajax('a.jsp',{},function(){})
            url = arguments[0];
            data = arguments[1];
            success = arguments[2];
        } else if (arguments.length == 3 && (typeof arguments[2]) == "object") {//g.ajax('a.jsp',{},{timeout:1000})
            url = arguments[0];
            data = arguments[1];
            ajaxConfig = arguments[2];
        } else if (arguments.length == 4) {
            url = arguments[0];
            data = arguments[1];
            ajaxConfig = arguments[2];
            success = arguments[3];
        }

        if ((typeof ajaxConfig.site) == "string" && ajaxConfig.site.indexOf('g') >= 0) {
            url = g.url.g + url;
        } else if ((typeof ajaxConfig.site) == "string" && ajaxConfig.site.indexOf('w') >= 0) {
            url = g.url.w + url;
        } else if ((typeof ajaxConfig.site) == "string" && ajaxConfig.site.indexOf('f') >= 0) {
            url = url;
        } else if (data.site && data.site.indexOf('g') >= 0) {
            url = g.url.g + url;
        } else if (data.site && data.site.indexOf('w') >= 0) {
            url = g.url.w + url;
        } else if (data.site && data.site.indexOf('f') >= 0) {
            url = url;
        } else if(data.site && data.site.indexOf('s') >= 0){
            url = "http://ss" + cookieDomain + url;
        } else {//没有配置 就给g域名
            url = g.url.g + url;
        }

        var ajaxdata = {
            type: 'get',
            url: url,
            data: data,
            dataType: 'jsonp',
            jsonpName: (data.callback == undefined ? ("cb_" + parseInt((Math.random() * 1000000000000000))) : data.callback),
            success: function (data) {
                success && success(data)
            }
        };

        if (data.callback) {
            delete data.callback;
        }

        if ((typeof ajaxConfig) == "object") {
            $.extend(ajaxdata, ajaxConfig)
        }

        return $.ajax(ajaxdata);
    }
    window.g = window.g || {};
    g.ajax = ajax;
})();
//#endregion

//#region 登录组件

//弹层
; (function (exports) {
    var $ = jQuery;
    var callbackUrl = g.url.g.indexOf(location.host) >= 0 ? ("http://" + location.host + "/ec/homeus/glogin_callback.html") : "http://" + location.host + "/glogin_callback.html";
    var conf = {
        loaded:false,
        login_url: (function () {
            if (g.url.login.indexOf("atgsit") > 0 || g.url.login.indexOf("atguat") > 0 || g.url.login.indexOf("gomeprelive") > 0) {
                return g.url.login
            }else {
                return g.url.login.replace('http', 'https');
            }
        })() + '/popLogin.no?callbackHost=' + callbackUrl + "&orginURI=" + location.href,
        reg_url: (function () {
            if (g.url.reg.indexOf("atgsit") > 0 || g.url.reg.indexOf("atguat") > 0 || g.url.reg.indexOf("gomeprelive") > 0) {
                return g.url.reg
            }else {
                return g.url.reg.replace('http', 'https');
            }
        })() + '/register/index/pop.no?callbackHost=' + callbackUrl + "&orginURI=" + location.href,

        verifyUserUrl: g.url.login + '/redirectResetPwd.no?loginType=1',//login.atgsit.com.cn/redirectResetPwd.no?loginType=1
        loginbg: $('<div style="position:fixed;top:0;left:0; width:100%;height:100%;  background:#000;opacity:0.15;  filter:alpha(opacity=15); display:none;"></div>'),
        loginfrm: $('<iframe id="loginFrame" scrolling="no"  frameborder="0" style="z-index:2; width:462px;height:605px; position:fixed;   background-color:transparent; " allowTransparency="true"></iframe>'),
        zIndex: 10000,
        show: function (userName) {
             
            conf.loginbg
                 .show()
                 .css('zIndex', conf.zIndex);             

            conf.loginfrm.remove();

            conf.loginfrm = $('<iframe scrolling="no"  frameborder="0" style="z-index:2; width:462px;height:605px; position:fixed;   background-color:transparent; " allowTransparency="true"></iframe>');
            $('body').append(conf.loginfrm);
            conf.loginfrm.attr('src', conf.src + "&orginURI=" + location.href + ((userName ==undefined ||userName =="")?'':"&userName=" + userName) + "&_t=" + parseInt((Math.random() * 1000000000000)));
            conf.loginfrm.show().css({
                left: ($(window).innerWidth() - conf.loginfrm.width()) / 2,
                top: ($(window).height() - conf.loginfrm.height()) / 2,
                zIndex: conf.zIndex + 1
            });

        },
        verifyUser_Show: function () {
            conf.loginbg
              .show()
              .css('zIndex', conf.zIndex);

            conf.loginfrm.show().css({
                left: ($(window).innerWidth() - conf.loginfrm.width()) / 2,
                top: ($(window).height() - conf.loginfrm.height()) / 2,
                zIndex: conf.zIndex + 1
            }).attr('src', conf.verifyUserUrl +  '&callbackHost=' + callbackUrl + "&orginURI=" + location.href    + "&_t=" + parseInt((Math.random() * 1000000000000)));
        },
        login_open: function (userName) {
            //document.domain = 'gome.com.cn';
            conf.loginbg
              .show()
              .css('zIndex', conf.zIndex);

            conf.loginfrm.remove();

            conf.loginfrm = $('<iframe id="loginFrame" scrolling="no"  frameborder="0" style="z-index:2; width:462px;height:605px; position:fixed;   background-color:transparent; " allowTransparency="true"></iframe>');
            $('body').append(conf.loginfrm);
            //console.log(conf);
            conf.loginfrm.attr('src', conf.login_url + ((userName ==undefined ||userName =="")?'':"&userName=" + userName) + "&_t=" + parseInt((Math.random() * 1000000000000)));
            conf.loginfrm.show().css({
                left: ($(window).innerWidth() - conf.loginfrm.width()) / 2,
                top: ($(window).height() - conf.loginfrm.height()) / 2,
                zIndex: conf.zIndex + 1
            });

            /**
             *
             * @type {number}
             */
            var callback = window.setInterval(function() {

                var ifr = document.getElementById('loginFrame');
                try{
                    //console.log(ifr.src);//?option_callback=1
                    if(ifr.src == 'http://www.gome.com.cn/glogin_callback.html') {
                        //alert(ifr.src);

                        var option_callback = 1;

                        option_callback = parseInt(option_callback);
                        //console.log("option_callback:" + option_callback)

                        //以下值 只能添加 不能删除
                        switch (option_callback) {
                            case 0:
                                g.login.callback();
                                break;
                            case 1://登录成功
                                g.login.callback(true);
                                break;
                            case 2://校验用户
                                g.login.verifyUser();
                                break;
                            case 3://弹出登录框
                                g.login.login_open(getParam("userName"));
                                break;
                            case 4://弹出注册
                                g.login.reg_open();
                                break;
                            case 10://嵌入登录层 回调
                                g.embedLogin.callback(true);
                                break;
                            case 11://嵌入登录层 登录
                                g.embedLogin.start_login();
                                break;
                            case 12://嵌入登录层 注册
                                g.embedLogin.start_reg();
                                break;
                            default:
                                g.login.callback();
                                break;
                        }

                        clearInterval(callback);
                    }

                }catch(ex){
                    clearInterval(callback);
                }
            }, 1000);
        },
        reg_open: function () {

            conf.loginbg
            .show()
            .css('zIndex', conf.zIndex);

            conf.loginfrm.remove();

            conf.loginfrm = $('<iframe scrolling="no"  frameborder="0" style="z-index:2; width:462px;height:605px; position:fixed;   background-color:transparent; " allowTransparency="true"></iframe>');
            $('body').append(conf.loginfrm);
            conf.loginfrm.attr('src', conf.reg_url + "&_t=" + parseInt((Math.random() * 1000000000000)));
            conf.loginfrm.show().css({
                left: ($(window).innerWidth() - conf.loginfrm.width()) / 2,
                top: ($(window).height() - conf.loginfrm.height()) / 2,
                zIndex: conf.zIndex + 1
            });
        },
        close: function () {
            conf.loginbg.hide();
            conf.loginfrm.hide().removeAttr("src");
            //document.domain = location.host;
        }, 
        callback: function () { }
    } 
    login = function (fn,reg) {
        if (conf.loaded == false) {
            conf.loaded = true;
            $('body').append(conf.loginbg);
        }

        if (login.running == true) {
            return;
        }
        login.running = true;

        conf.fn = null;

        g.ajax('/ec/homeus/global/login/loginstatus.jsp').done(function (data) {
            login.running = false;

            if (data.securityStatus > 3) {
                fn && fn();
            } else {                
                conf.fn = fn;                                
                ("reg" == reg) ? conf.reg_open() : conf.login_open();//通常走登录 不走注册                
            }
        });
 
    };

    login.callback = function (logined) {

        conf.close();

        if (logined) {
            if (window.signData) {
                signData.lazyCartEnd = false;
                signData.loginEnd = false;
                signData.gloginfn = conf.fn;
                signData.init();
            } else {
                conf.fn && conf.fn();
            }           
        };       
    }

    login.close = function () { };
    login.verifyUser = function () {//弹层校验用户
        conf.close();
        conf.verifyUser_Show();
    }    
    login.login_open = function (userName) {
        conf.close();
        conf.login_open(userName);
    }
    login.reg_open = function () {
        conf.close();
        conf.reg_open()
    }

    login.config = function (obj) {
        conf = $.extend(conf, obj);
    }

    exports.login = login;
})(g);