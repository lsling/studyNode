(function($,tpl,u){
	function split(a){
		return function(b){
			return b.split(a);
		}
	};    
	function respInfofn() {
	    var getArg = window.location.href;
	    var querystring=u.pipe(split("?"),u.last,split("&"),u.map(split("=")),u.fromPairs);
	    var q2 = querystring(getArg);
	    return q2.rspInfo;
	};
	request
	.reqp('http://success'+cookieDomain+'/order/loadPSInfo',{rspInfo:respInfofn()})
/*	.reqp('http://success.atguat.com.cn/order/loadPSInfo?rspInfo=6f726465724e6f3d313030303030373131363426636173686965724e6f3d3030303030303030303030303030303030303030303030303538353235303532267061794d6f6e65793d3134393030267061794d6f64653d3032302670617942616e6b3d434d424348494e412670617954696d653d32303136303530343132303432352672656d61726b3d686f6d65536974652673656375726974795369676e3d443437323543384332333541343437444431364643443637343143323244373426736974654163636f756e743df4f358a3a0af07ca6ec8&ca')*/
	.then(function(data){
		//data.data.paymentType ="default";
		//data.paymentType ="coupon";
		//success true时
		var tplmap={
			payment_fail:tpl.payment_fail
			,payment_default:tpl.payment_default
			,payment_coupon:tpl.payment_coupon
			,payment_balance:tpl.payment_balance
			,payment_deposit:tpl.payment_deposit
		};
		//success false
		if(data.success == false){
			$("#payment-success").html("").html(tplmap.payment_fail(null));
			return;
		}else{
			var name="payment_"+data.data.paymentType;
			$("#payment-success").html("").html(tplmap[name](data));
		}
		//其他逻辑
		}).fail(function(){
		//跳转到失败页
		$("#payment-success").html("").html(tplmap.payment_fail(null));
	});

//loadPSInfo 
}($,GTPL,util));         
// 接口异常拦截 
/*var datafalse = 
{
    "success": false,
    "errCode": "EX_0000",
    "errMsg": "There's an exception(pRequestInfo): 6f726465724e6f3d313030303030373131363426636173686965724e6f3d3030303030303030303030303030303030303030303030303538353235303532267061794d6f6e65793d3134393030267061794d6f64653d3032302670617942616e6b3d434d424348494e412670617954696d653d32303136303530343132303432352672656d61726b3d686f6d65536974652673656375726974795369676e3d443437323543384332333541343437444431364643443637343143323244373426736974654163636f756e743df4f358a3a0af07ca6ec8&ca&debug",
    "data": null
};
*/
