/**配送方式*/
!function(exports,req,u,ui,uw,tpl){
	var TYPE="7listOfItem7";
	//shopping.js中 动态设置进来的东西
	var $$={
		getPickingUpStores:null
	};
	var emit=uw.emit(TYPE);
	function div(im){
		return im('div');
	}
	function datfn(im){
		return im("data");
	}
	function transData(data){
		//时间段头部 星期几 和余月月-日日
		function headfn(a){
			var ymd=a.yd.split(" ")[0].split("-");
			return {
				label:a.label,
				md:ymd[1]+"-"+ymd[2]
			}
		}
		//限时达 横向数据转纵向数据
		function xsddata(xsd){
			var headarr=[{
				label:"时间段"
			}];
			headarr=u.concat(headarr,u.map(headfn,xsd.concrete));
			var range=[];
			for(var i=0;i<xsd.concrete[0].items.length;i++)range.push(i);

			function findItem(i){
				return u.concat([xsd.concrete[0].items[i]],u.map(function(a){
					return a.items[i];
				},xsd.concrete));
			}
			var bodyarr=u.map(findItem,range);
			return {
				head:headarr,
				body:u.map(u.zipWith(u.assoc("head"),headarr),bodyarr),
				code:"XSD",
				postBackCode:xsd.postBackCode,
				type:xsd.type
			}
		}
		//如果是限时达进行转换
		function isxsdfn(a){
			if(a.code=="XSD")return xsddata(a);
			return a;
		}
		//时间
		function assocTimes(a){
			return u.assoc("times",u.map(isxsdfn,a.times),a);
		}
		//支付方式
		function paymentfn(a){
			if(a.payments==null){
				a.payments=[];
			}
			if(a.payments.length!=0)return u.assoc("payments",[{"code":"SOME_UP","selected":true}],a);
			return a;
		}
		var head=u.head(data.shoppinginfo);
		var tail=u.tail(data.shoppinginfo);
		var sinfo=u.concat([assocTimes(head)],u.pipe(u.map(paymentfn),u.map(assocTimes))(tail));
		function gomeNoFee(data){
			function fn1(info){
				return u.assoc("express",u.map(u.assoc("shippingFee",null),info.express),info);
			}
			if(data.shopId=="GOME"){
				return u.assoc("shoppinginfo",u.map(fn1,data.shoppinginfo),data);
			}
			return data;
		}
		//是否显示对应商品
		function visibleCorrespondItems(shopId){
			if(shopId !=="GOME") return false;
			if($page.site=="home") return true;
			return false;
		}
		return u.pipe(
			//限时达时间转换，支付方式与上方一致转换
			u.assoc("shoppinginfo",sinfo)
			//自营商品屏蔽运费
			,gomeNoFee
			//获取选中的项
			,u.assoc("selectedfn",u.find(u.prop("selected")))
			//获取限时达选中的时间并格式化如果没有选中的返回null
			,u.assoc("xsdSelectedTime",xsdSelectedTime)
			,u.assoc("visibleCorrespondItems",visibleCorrespondItems)
			)(data);
	}
	//计时达->选中的时间
	function xsdSelectedTime(xsd){
		var selectedItem=u.find(u.prop("selected"));
		var item=u.reduce(function(result,itemarr){
			if(result)return result;
			var item=selectedItem(u.tail(itemarr));
			if(item)return item;
			return null;
		},null,xsd.body);
		if(item){
			return item.head.md+"("+item.head.label+") "+$config.formathhmm(item.startTime)+" "+$config.formathhmm(item.endTime);
		}	
		return "";
	}
	var io=u.curry(function(im,data){
		im("data",data);
		return im;
	});
	//选择快递
	var selectExpressfn=u.curry(function(data,path){
		var selectedItem=u.path(path,data);
		var expressPath=u.take(3,path);
		var changeSelected=u.map(u.ifn(u.eq(selectedItem),u.assoc("selected",true),u.assoc("selected",false)));
		var changeExpress=u.pipe(u.path(expressPath),changeSelected);
		//如果是门店自提
		if(selectedItem.code=="Gome Picking Up"){
			emit("selectPickingUp",{
				shop:data,
				selectedExpress:selectedItem
			});
		}else{
			emit("selectExpress",{
				shop:data,
				selectedExpress:selectedItem
			});
		}
		return u.assocPath(expressPath,changeExpress(data),data);
	});
	
	//map所有的时间选项
	function mapShoppintTime(fn,times){
		function runfn(code){
			return function(item){
				return fn(code,item);
			}
		}
		function mapXSDBody(bodyItem){
			return u.concat([u.head(bodyItem)],u.map(runfn("XSD"),u.tail(bodyItem)));
		}
		function dotime(time){
			if(time.code=="DAY"){
				return u.assoc("items",u.map(runfn("DAY"),time.items),time);
			}
			if(time.code=="XSD"){
				return u.assoc("body",u.map(mapXSDBody,time.body),time);
			}
			return runfn(time.code)(time);
		}
		return u.map(dotime,times);
	}
	//选择配送时间
	function selectShoppingTime(shop,timesPath,stimePath,sitemPath){
		var sitem=u.path(sitemPath,shop);
		var stime=u.path(stimePath,shop);
		var xsdflag=true;
		function setSelected(code,item){
			if(sitem.code=="XSD"&&code=="XSD"){
				if(item.available&&xsdflag){
					xsdflag=false;
					sitem=item;
					return u.assoc("selected",true,item);
				}else{
					return u.assoc("selected",false,item);
				}
			}else{
				if(item==sitem)return u.assoc("selected",true,item);
				return u.assoc("selected",false,item);
			}
		}
		var newTimes=mapShoppintTime(setSelected,u.path(timesPath,shop));
		emit("selectTime",{
			shop:shop,
			stime:stime,
			sitem:sitem
		});
		/*
		*更改当前操作配送方式的时间 
		*将当前选中的时间设为选中状态
		*如果选中限时达单选按钮  查找最近可用的时间选项选中
		*/
		return u.assocPath(timesPath,newTimes,shop);
	}
	function render(im){
		div(im).html(tpl.listOfItem_method(datfn(im)));
		alternation(im);
	}
	function alternation(im){
		ui.gpipes(div(im),{
			changeComment:changeComment
		});
		//对应商品
		div(im).find("[map-item]").each(function(){
			var el=$(this);
			ui.hoverUp(el.find("[hoverup-1],[hover-1]"))(el.find("[hover-1]"));
		});
		//保存备注
		var commentTimeid=null;
		function changeComment(note,tip){
			if(note.val().length>30){
				tip.show();
				note.addClass("error");
				return;
			}
			note.removeClass("error");

			tip.hide();
			io(im,u.assoc("comments",note.val(),datfn(im)));
			commentTimeid=ui.delay(1000,function(){
				emit("saveComments",{shop:datfn(im),cm:note.val()});
			},null,commentTimeid);
		}
		//选择快递
		div(im).find("[g-e-path]").on("click"
			,ui.jqpipe(ui.attr("g-e-path")
				,u.split(",")
				,selectExpressfn(datfn(im))
				,io(im)
				,render));
		//修改支付方式
		div(im).find("[g-p-path]").on("click"
			,ui.jqpipe(ui.attr("g-p-path")
				,u.split(",")
				,u.path(u.__,datfn(im))
				,dialogPayment));
		function dialogPayment(payments){
			function bodyfn(){
				return tpl.listOfItem_method_payment(payments);
			}
			var dialog=panel.confirm({
				body:bodyfn,
				close:ui.hide,
				btns:[]
			});
				// btns:[{
				// 	clazz:"btn btn-primary btn-w83 mr50",
				// 	btnName:"保存付款方式",
				// 	click:u.pipe(ui.hide,u.partial(emit,["savePayment",u.find(u.prop("selected"),payments)]))
				// },{
				// 	clazz:"btn btn-default btn-w83",
				// 	btnName:"取消",
				// 	click:ui.hide
				// }],
			dialog.$dialog.find("[g-list-save]").on("click",function(){
				emit("savePayment",u.find(u.prop("selected"),payments));
				ui.hide(dialog);
			});
			dialog.$dialog.find("[g-list-canel]").on("click",function(){
				ui.hide(dialog);
			});
			dialog.$dialog.find("[idx]").on("click",function(){
				dialog.hide();
				var idx=$(this).attr("idx");
				var p=u.map(u.assoc("selected",false),payments);
				dialogPayment(u.assocPath([idx,"selected"],true,p));
			});

		}
		//鼠标划过弹出
		div(im).find("[hoverup]").each(ui.jq(ui.hoverUpBySelector(u.__,"[hover]")));
		//配送时间选择
		div(im).find("[g-t-path]").on("click",function(){
			var el=$(this);
			var selectedPath=el.attr("g-t-path").split(",");
			var selectedTimePath=u.take(4,selectedPath);
			var timePath=u.take(3,selectedPath);
			var shop=datfn(im);
			u.pipe(selectShoppingTime,io(im),render)(shop,
				timePath,
				selectedTimePath,
				selectedPath);
		});
		//门店自提修改
		div(im).find("[g-picking-up]").on("click",function(){
			var path=$(this).attr("g-picking-up").split(",");
			var selectedItem=u.pipe(u.path(path),u.find(u.prop("selected")));
			var item =selectedItem(datfn(im));
			$$.getPickingUpStores(item.city).then(function(data){
				if(data.success==false) return panel.alert(data.errMsg);
				function filter(list){
					function fn1(area){
						return u.assoc("gomeStoreShippings",u.filter(u.pipe(u.prop("enabled"),u.eq("1")),area.gomeStoreShippings),area);
					}
					return u.map(fn1,list);
				}
				dialogPickingUp(im,{pickingUP:item,list:filter(data.data)});
			});
		});
		
	}
	function dialogPickingUp(im,data){
		var storeId=data.pickingUP.storeId;
		function isSelectArea(area){
			if(area.gomeStoreShippings)return u.find(u.pipe(u.prop("gomeStoreId"),u.eq(storeId)),area.gomeStoreShippings);
			return false;
		}
		function isSelectStore(store){
			return store.gomeStoreId==storeId;
		}
		function selectedArea(list){
			var r = u.find(isSelectArea,list);
			return r==null?list[0]:r;
		}
		data.selectedArea=selectedArea;
		data.isSelectArea=isSelectArea;
		var dialog = $.gDialog({
			html:tpl.listOfItem_pickingUp(data),
			modal:{
			}
		});
		dialog.show();
		var div=dialog.$dialog;
		function renderStores(div,area){
			area=area||{};
			area.isSelectStore=isSelectStore;
			div.find("#store-content").html(tpl.listOfItem_pickingUp_stores(area));
			div.find("[store-idx]").on("click",function(){
				var idx = $(this).attr("store-idx");
				var store=area.gomeStoreShippings[idx];
				storeId=store.gomeStoreId;
				renderStores(div,area);
			});
		}
		renderStores(div,selectedArea(data.list));

		//下拉框设置
		div.find("[g-select]").on("click",function(){
			div.find("[g-select-body]").toggle();
			return false;
		});
		div.on("click",function(){
			// if($(this).is("[g-select]"))return;
			div.find("[g-select-body]").hide();
		});
		//点击下拉框选项
		div.find("[s-idx]").on("click",function(){
			var idx=$(this).attr("s-idx");
			renderStores(div,data.list[idx]);
			div.find("[g-title]").html($(this).html());
		});
		//保存门店自提
		div.find("#pks-saveStore").on("click",function(){
			emit("selectPickingUp",{
				shop:datfn(im),
				selectedExpress:{
					code:"Gome Picking Up",
					storeId:storeId
				}
			});
			dialog.hide();
		});
		//点击关闭
		div
		.find("[g-close]")
		.on("click",function(){
			dialog.hide();
		});
	}
	function make(div,data){
		data=transData(data);
		function r(a,data1){
			if(a=="div")return div;//操作范围
			if(data1!=null){
				data=data1;
				return r;
			}
			if(a=="data")return data;//最新数据
		}
		render(r);
		return r;
	}
	exports.listOfItemMethod={
		make:make,
		TYPE:TYPE,
		$$:$$
	};
}(this,request,util,util_ui,util_watch,GTPL);