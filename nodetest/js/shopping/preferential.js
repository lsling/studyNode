/**促销优惠*/
!function(exports,req,u,ui,uw,tpl,validate){
	var TYPE="7preferential7";
	var emit=uw.emit(TYPE);
	function div(im){
		return im('div');
	}
	function dfn(im){
		return im('data');
	}
	function set(path,val,im){
		return im('data',transData(u.assocPath(path,val,dfn(im))));
	}
	//获取本次最多可使用 门店积分
	function maxUseJF(im){
		return dfn(im).vrbsos.sp.availablePoint;
	}
	//用户未开启支付密码
	function isNotUnsealPwd(im){
		return dfn(im).verifyStatus=="VERIFY_NOT_ACTIVATED";
	}
	//用户未开启手机验证
	function isNotMobileActive(im){
		return dfn(im).verifyStatus=="MOBILE_NOT_ACTIVATED";
	}
	function transData(data){
		var MORE_LIMIT=6;
		//蓝券
		function getBCS(){
			var list=data.vrbsos.bcs;
			return {
				bottom:list.length>MORE_LIMIT,
				list:$config.shoppingAtom.lq_more=="Y"?u.take(MORE_LIMIT,list):list
			}
		}
		//红券
		function getRCS(){
			var list=data.vrbsos.rcs;
			return {
				bottom:list.length>MORE_LIMIT,
				list:$config.shoppingAtom.hq_more=="Y"?u.take(MORE_LIMIT,list):list
			}
		}

		//店铺券
		function getDCS(){
			var list=data.vrbsos.scs;
			return {
				bottom:list.length>MORE_LIMIT,
				list:$config.shoppingAtom.dpq_more=="Y"?u.take(MORE_LIMIT,list):list
			}
		}

		//电子券
		function getES(){
			var list=data.vrbsos.es;
			return {
				bottom:list.length>MORE_LIMIT,
				list:$config.shoppingAtom.dzq_more=="Y"?u.take(MORE_LIMIT,list):list
			}
		}
		return u.pipe(
			u.assoc("getBCS",getBCS)
			,u.assoc("getRCS",getRCS)
			,u.assoc("getES",getES)
			,u.assoc("getDCS",getDCS))(data);
	}
	function render(im){
		div(im).html(tpl.preferential_main(dfn(im)));
		alternation(im);
	}
	
	function alternation(im){
		
		function setStage(a,b){
			$config.shoppingAtom[a]=b;
		}
		function _render(){
			render(im);
		}
		//选中数据
		function assocPathSelected(pathstr,value,flag){
			var keyPre=value=="true"?"":"canel";
			emit(keyPre+"select"+flag,u.path(pathstr.split("."),dfn(im)));
			var path=u.concat(pathstr.split("."),["checked"]);
			var pathSelected=u.concat(pathstr.split("."),["selected"]);
			set(path,value=="true"?true:false,im);
			set(pathSelected,value=="true"?true:false,im);
			//晴空支付密码错误提示
			$config.shoppingAtom.yzm_tip="";
		}
		//设置值
		function setValueStage(name,el){
			$config.shoppingAtom[name]=el.val();
		}
		//这是最新时间long值
		function setTimeLongStage(name){
			$config.shoppingAtom[name]=new Date-0;
		}
		//使用国美门电器门店会员积分 兑换
		function changeInto(){
			$config.shoppingAtom.mdhyjf_tip="";
			if(!validate.validate([[{
				txt:$config.shoppingAtom.mdhyjf,
				max:maxUseJF(im)
			},"mdhyjf",function(msg){$config.shoppingAtom.mdhyjf_tip=msg}]])){
				_render();
				return;
			}
			emit("changeInto",{
				text:$config.shoppingAtom.mdhyjf,
				errorfn:function(data){
					$config.shoppingAtom.mdhyjf_tip=data.errMsg;
					_render();
				},
				okfn:function(){
					$config.shoppingAtom.mdhyjf = "";
				}
			});
		}
		//激活优惠券
		function activeYCode(){
			$config.shoppingAtom.yhj_tip="";
			var temp=[$config.shoppingAtom.yhj_bm,$config.shoppingAtom.yhj_jhm,$config.shoppingAtom.yhj_yzm];
			emit("activeYCode",{
				cpno:$config.shoppingAtom.yhj_bm,
				acode:$config.shoppingAtom.yhj_jhm,
				c:$config.shoppingAtom.yhj_yzm,
				capCd:$config.shoppingAtom.yhj_img,
				errorfn:function(data){
					$config.shoppingAtom.yhj_tip=data.errMsg;
					$config.shoppingAtom.yhj_bm=temp[0];
					$config.shoppingAtom.yhj_jhm=temp[1];
					$config.shoppingAtom.yhj_yzm=temp[2];
					_render();
				}
			});
			$config.shoppingAtom.yhj_bm="";
			$config.shoppingAtom.yhj_jhm="";
			$config.shoppingAtom.yhj_yzm="";
			$config.shoppingAtom.yhj_tip="";
		}
		//使用电子券
		function dzqAction(){
			if($config.shoppingAtom.dzqma=="")return panel.alert("请输入电子券密码");
			emit("dzqAction",{txt:$config.shoppingAtom.dzqma});
			$config.shoppingAtom.dzqma="";
		}
		//推荐人员工号
		function ryghAction(){
			emit("ryghAction",{txt:$config.shoppingAtom.rygh,errfn:function(data){
				$config.shoppingAtom.rygh_tip=data.errMsg;
				_render();
			}});
			$config.shoppingAtom.rygh="";
			$config.shoppingAtom.rygh_tip="";
		}
		//取消推荐人员
		function ryghCanel(){
			emit("ryghCanel",null);
		}
		//绑定国美E卡
		function bindECard(){
			if(!($config.shoppingAtom.ecard_1) || !($config.shoppingAtom.ecard_2) || !($config.shoppingAtom.ecard_3) || !($config.shoppingAtom.ecard_4)){
				panel.error('请输入正确的16位密码');
				return
			}
			if(!($config.shoppingAtom.ecard_yzm)){
				panel.error("请输入验证码");
				return
			}
			emit("bindECard",{				
					//参数，在shopping发ajax 并且把此函数名，写在shopping最下面，前且定义此函数值为发请求的新建函数名
					"ecartNumber":$config.shoppingAtom.ecard_1 + $config.shoppingAtom.ecard_2 +$config.shoppingAtom.ecard_3 +$config.shoppingAtom.ecard_4 ,
					"imageCode":$config.shoppingAtom.ecard_yzm,
					"capcd":$config.shoppingAtom.yhj_img
				})
			// $config.shoppingAtom.ecard_yzm_tip=""
			// if($config.shoppingAtom.ecard_yzm){
			// 	emit("bindECard",{				
			// 		//参数，在shopping发ajax 并且把此函数名，写在shopping最下面，前且定义此函数值为发请求的新建函数名
			// 		"ecartNumber":$config.shoppingAtom.ecard_1 + $config.shoppingAtom.ecard_2 +$config.shoppingAtom.ecard_3 +$config.shoppingAtom.ecard_4 ,
			// 		"imageCode":$config.shoppingAtom.ecard_yzm,
			// 		"capcd":$config.shoppingAtom.yhj_img
			// 	})
			// }else{
			// 	$config.shoppingAtom.ecard_yzm_tip='验证码不能为空';
			// 	_render();
			// }
		}
		//支付密码验证
		function payRegAction(){
			$config.shoppingAtom.yzm_tip="";
			emit("payRegAction",{
				captcha:$config.shoppingAtom.yzm,
				capcd:$config.shoppingAtom.yzm_img,
				paypasswd:$config.shoppingAtom.yzm_pw,
				errorfn:function(data){
					$config.shoppingAtom.yzm_tip=data.errMsg;
					_render();
				}
			});
			$config.shoppingAtom.yzm="";
			$config.shoppingAtom.yzm_pw="";
		}
		//是否ctrl+v按键
		function isCtrl_v(e){

		}
		div(im).find('.bind-e-card-wrap').each(function(i,v){
			var divitem=$(this);
			divitem.find("input.e-num")
			.on("keyup",function(e){
				var val=$(this).val().replace(/[^a-zA-Z0-9]/g,'');
				if(val.length>=4){
					$(this).val(u.take(4,val));
					var next=$(this).nextAll("input.e-num").eq(0);
					if(val.length==4)return next.focus();
					next.val(u.drop(4,val)).focus().trigger("keyup");
				}else{
					$(this).val(val);
				}
			})//黏贴时清空输入框
			.on("paste",function(){
				$(this)
				.val('')
				.nextAll("input.e-num")
				.val('');
			});
		})
		div(im).find("[more-btn]").each(function(){
			$(this).hover(function(){
				$(this).find(".line_up").css({"border-color":"#f00"});
				$(this).find(".moreyhq").css({
					"border-color":"#f00",
					"color":"#f00"
				});
				var icon=$(this).find(".moreyhq").find("i");
				if(icon.is(".arrowdown"))icon.removeClass("arrowdown").addClass("arrowdown_red");
				else icon.removeClass("arrowup2").addClass("arrowup_red");
			},function(){
				$(this).find(".line_up").css("border-color","");
				$(this).find(".moreyhq").css({
					"border-color":"",
					"color":""
				});
				var icon=$(this).find(".moreyhq").find("i");
				if(icon.is(".arrowdown_red"))icon.removeClass("arrowdown_red").addClass("arrowdown");
				else icon.removeClass("arrowup_red").addClass("arrowup2");
			});
		});
		ui.gpipes(div(im),{
			setStage:setStage,
			render:_render,
			assocPathSelected:assocPathSelected,
			setValueStage:setValueStage,
			setTimeLongStage:setTimeLongStage,
			changeInto:changeInto,
			activeYCode:activeYCode,
			dzqAction:dzqAction,
			ryghAction:ryghAction,
			payRegAction:payRegAction,
			ryghCanel:ryghCanel,
			bindECard:bindECard
		});
	}

	function make(div,data){
		function r(a,data1){
			if(a=="div")return div;//操作范围
			if(data1!=null){
				data=data1;
				return r;
			}
			if(a=="data")return data;//最新数据
		}
		render(r);
		return r;
	}
	exports.preferential={
		make:u.curry(make),
		TYPE:TYPE,
		transData:transData,
		render:render,
		isNotUnsealPwd:isNotUnsealPwd
	};
}(this,request,util,util_ui,util_watch,GTPL,validate);