/**
*核对订单页入口
*authorization 只有海外购才会引入js auhorization.js
*此模块 维护 各个小模块之间的关系 和 接口交互
*各站点名称
	home    主站
    secondHand  残次品站点
    warranty    延保站点
    contractPhone   合约机站点
    groupOn 团购站点
    rushBuy 抢购站点
    haiwaigou   海外购站点
    presell 预售站点
    directionalCoupon   定向券站点
	实体卡站点: gomeEntityCard
	电子卡站点：gomeVirtualCard
*/
!function(
	exports,
	u,
	ui,
	uw,
	req,
	address,
	payment,
	listOfItem,
	commitOrder,
	preferential,
	invoice,
	intor,
	listOfItemMethod){
	var TYPE="shopping";

	//是否实体卡站点
	function isGomeEntityCardSite(){
		return $page.site=="gomeEntityCard";
	}
	//是否电子卡
	function isGomeVirtualCardSite(){
		return $page.site=="gomeVirtualCard";
	}
	//是否是海外购
	function isHwg(){
		return $page.site=="haiwaigou";
	}
	//是否节能补贴
	function isAllowance(){
		return $page.site=="allowance";
	}
	//购物车URL
	function cartUrl(){
		if($page.site=="home")return "/";
		if($page.site=="haiwaigou")return "/";
		if($page.site=="allowance") return "/";
		if($page.site=="gomeEntityCard") return "http://card"+cookieDomain;
		if($page.site=="gomeVirtualCard") return "http://card"+cookieDomain;
		if($page.site=="warranty") {
			var str=window.location.href.split("?")[1];
			if(str!=null)str=str.split("&")[0];
			return "/warranty/cart?"+str
		}
		return "/"+$page.site+"/cart";
	}
	//分支函数
	function branch(fn1,fn2){
		return function(data){
			return [fn1(data),fn2(data)];
		}
	}
	//带错误处理的分支
	function branchErrorWarp(errfn,successfn){
		return branch(whenErrorWarp(errfn),intor.unlessError(successfn));
	}
	//不带错误处理的分支处理
	function branchWarp(errfn,successfn){
		return branch(intor.whenError(errfn),intor.unlessError(successfn));
	}
	//get请求
	function getapi(path,params){
		panel.mask();
		return onlyGetapi(path,params).always(panel.maskHide);
		// return onlyGetapi(path,params)
	}
	function onlyGetapi(path,params){
		// params=params||{};
		// params._t=new Date-0;
		if(isHwg()){
			return req.getApi("home",path,params);
		}
		if($page.site=="allowance") return req.getApi("home",path,params);
		return req.getApi($page.site,path,params);
	}
	
	//返回购物车写区域cookie
	function writeAreaCookie(){
		if($config.shoppingInstenceAtom.address==null)return false;
		var item=address.selectedAddress($config.shoppingInstenceAtom.address);
		if(item==null)return false;
		$.cookie('atgregion', [
			item.origin.address.countyCode,
			item.origin.address.provinceName+item.origin.address.cityName+item.origin.address.countyName+item.origin.address.townName,
			item.origin.address.cityCode,
			item.origin.address.provinceCode,
			item.origin.address.townCode
		].join("|"), {expires:30,path:'/',domain:cookieDomain});
		return true;
	}
	function gotoCart(){
		ui.redir(cartUrl());
	}
	//初始化页面 收获地址变更 提交订单 无货不支持配送 下架校验弹窗
	function notStockNotSupportTip(im){
		//延保站点忽略验证
		if($page.site=="warranty")return false;
		if(im==null){
			panel.alert("商品清单异常");
			return true;
		}
		//无货
		var noinventory=u.pipe(u.prop("inventoryState"),u.eq("NO_GOODS"));
		//不支持配送
		var nosupport=u.pipe(u.prop("inventoryState"),u.eq("DELIVERY_NOT_SUPPORTED"));
		//下架状态
		var itemoff=u.pipe(u.prop("inventoryState"),u.eq("OFF"));
		var items=listOfItem.filterAllItems(u.anyPass([noinventory,nosupport,itemoff]),im);
		var tipfn=u.cond([
				[isGomeEntityCardSite,u.always("无货")],
				[nosupport,u.always("该区域暂不支持配送")],
				[noinventory,u.always("该区域无货")],
				[itemoff,u.always("已下架")]
			]);
		if(u.isEmpty(items)){
			return false;
		}
		//修改购物车
		function modifycart(){
			writeAreaCookie();
			ui.redir(cartUrl());
		}
		//修改收货地址
		function modifyAddress(dialog){
			address.modify($config.shoppingInstenceAtom.address);
			dialog.hide();
		}
		function btnfn(){
			if(isGomeEntityCardSite() || isGomeVirtualCardSite()){
				return [{
					clazz:"btn btn-primary btn-w83 mr50",
					btnName:"去逛逛",
					click:modifycart
				}];		
			}
			//如果商品已下架
			if(itemoff(items[0])){
				return [{
					clazz:"btn btn-primary btn-w83 mr50",
					btnName:"返回购物车修改",
					click:modifycart
				}];	
			}

			return [{
					clazz:"btn btn-primary btn-w83 mr50",
					btnName:"返回购物车修改",
					click:modifycart
			},{
				clazz:"btn btn-default",
				btnName:"更换收货地址",
				click:modifyAddress
			}];

		}

		panel.confirm({
			type:panel.TYPE.WARN,
			body:[
				'<p class="name_over2" style="width:280px;">'
				,items[0].itemName
				,"</p>"
				,items.length>1?"等":""
				,"商品"
				,'<span class="fontRed">'
				,tipfn(items[0])
				,"</span>"
			].join(""),
			btns:btnfn(),
			close:ui.hide
		});
		return true;
	}
	function errfn(data){
		function alertBackCart(){
			var btnName="返回购物车"
			if(isGomeEntityCardSite() || isGomeVirtualCardSite()){
				btnName="去逛逛"
			}
			panel.confirm({
				type:panel.TYPE.WARN,
				body:data.errMsg,
				btns:[{
					clazz:"btn btn-primary",
					btnName:btnName,
					click:u.pipe(writeAreaCookie,gotoCart)
				}],
				close:false
			})
		}
		/*
		返回购物车
		*/
		if(data.status=="GO_CART") return gotoCart();
		// if(data.errCode=="003000001") return gotoCart();
		// if(data.errCode=="001001018") return gotoCart();
		// if(data.errCode=="001001019") return gotoCart();
		// if(data.errCode=="001001020") return gotoCart();
		// if(data.errCode=="001001021") return gotoCart();
		// if(data.errCode=="001001022") return gotoCart();
		// if(data.errCode=="0010010110") return gotoCart();
		// if(data.errCode=="0010017000") return gotoCart();
		/*
		弹出窗口点击返回购物车
		*/
		if(data.status=="CLICK_GO_CART")return alertBackCart();
		// if(data.errCode=="0010360043") return alertBackCart();
		// if(data.errCode=="0010360040") return alertBackCart();
		// if(data.errCode=="0010360041") return alertBackCart();
		// if(data.errCode=="0010360042") return alertBackCart();
		// if(data.errCode=="0010070050") return alertBackCart();
		//去登录页面
		if(data.status=="GO_LOGIN")return ui.redir("http://login"+cookieDomain+"/login");


		return panel.error($config.renderError(data));
	}
	//遇到错误 弹出提示 不执行回调
	var commonErrorWarp=u.pipe(intor.unlessError,intor.commonerror(errfn));
	//遇到错误 弹出提示 执行回调
	var whenErrorWarp=u.pipe(intor.whenError,intor.commonerror(errfn));
	//遇到错误 弹出提示 永远执行回调
	var alwayErrorWarp=u.pipe(intor.commonerror(errfn));

	//渲染收获地址
	var addressWarp=u.pipe(intor.setInstanceAtom("address"),intor.addressTransData,commonErrorWarp);
	var renderAddress=addressWarp(address.make($("#address")));

	//渲染支付方式
	var paymentWarp=u.pipe(intor.setInstanceAtom("payment"),intor.paymentTransData,commonErrorWarp);
	var renderPayment=paymentWarp(payment.make($("#payment")));

	//渲染发票
	var invoiceWarp=u.pipe(intor.setInstanceAtom("invoice"),intor.invoiceTransData,commonErrorWarp);
	var renderInvoice=invoiceWarp(invoice.make($("#invoice")));

	//配送方式
	var listOfItemWarp=u.pipe(intor.setInstanceAtom("listOfItem"),intor.listOfItemTransData,commonErrorWarp);
	var renderListOfItem=listOfItemWarp(listOfItem.make($("#listOfItem")));

	//优惠券 
	var preferentialWarp=u.pipe(intor.setInstanceAtom("preferential"),intor.preferentialTransData,commonErrorWarp);
	var renderPreferential=preferentialWarp(preferential.make($("#preferential")));
	
	//价格展示
	var commitOrderWarp=u.pipe(intor.setInstanceAtom("commitOrder"),intor.transData,commonErrorWarp);
	var renderCommitOrder=commitOrderWarp(commitOrder.make($("#commitOrder")));

	
	//保存收获地址
	var addAddressUrl="consignee/addAddress";
	var updateAddressUrl="consignee/updateAddress";
	if(isAllowance()){
		addAddressUrl="consignee/saveAllowanceAddress";
		updateAddressUrl="consignee/saveAllowanceAddress";
	}
	function hwgBiz(){
		var	renderAuthorization=commonErrorWarp(intor.transData(authorization.make($("#hwg-auth"))));
		var	loadAuth=function(data){
			if(data.success) uw.emit(TYPE,"loadAuth",{});
			getapi("haiwaigou/getIdentification?_="+new Date().getTime()).then(renderAuthorization);
		};
		uw.watch(authorization.TYPE,"saveAction",function(data){
			getapi("haiwaigou/saveIdentification",{
				idCardNumber:data.card,
				idCardRealName:data.name
			}).then(alwayErrorWarp(loadAuth));
		});
		return {
			renderAuthorization:renderAuthorization
		}
	}
	function virtualCardBiz(){
		//渲染商品清单
		var warp=u.pipe(intor.setInstanceAtom("listOfItem"),intor.transData,commonErrorWarp);
		var renderListOfItemShopping=warp(listOfItem.makeShopping($("#listOfItem")));
		//渲染身份认证
		var renderEntityCard=u.pipe(intor.setInstanceAtom("entityCard"),intor.transData,commonErrorWarp)(entityCard.make($("#entityCard")));
		function savePhone(data){
			getapi("transport/saveVirtualCardSMSMobile",{mobile:data.phone}).then(alwayErrorWarp(function(){}));
		}
		uw.watch(entityCard.TYPE,"savePhone",savePhone);
		return {
			renderListOfItemShopping:renderListOfItemShopping,
			renderEntityCard:renderEntityCard
		}
	}
	var reloadAll=u.cond([
		[isGomeVirtualCardSite,u.pipe(u.once(virtualCardBiz),reloadVirtualCardSite)],
		[isHwg,u.pipe(u.once(hwgBiz),reloadHwg)],
		[u.T,reloadPrimary]
	]);
	reloadAll();

	function reloadHwg(biz){
		return req.parall(
				getapi("order/initHWGOrder")
				,getapi("haiwaigou/getIdentification?_="+new Date().getTime())
				)
		.then(req.res(function(alldata,identityData){
			var autho=biz.renderAuthorization(identityData);
			//没有授权
			if(authorization.notAuthorization(autho)){
				ui.redir($config.URL.authorization);
				return;
			}
			//没有进行过身份认证
			if(authorization.notAuth(autho)){
				uw.watchOnce(TYPE,"loadAuth",function(data){
					reloadAll();
				})
				return;
			}
			commonErrorWarp(function(data){
				renderAddress({success:true,data:data.data.cis});
				renderPayment({success:true,data:data.data.pg});
				renderListOfItem({success:true,data:data.data.dsgs});
				renderPreferential({success:true,data:data.data.vrbsos});
				renderCommitOrder({success:true,data:data.data.checkoutSummary});
				
				//如果地址不是是展开情况 渲染发票
				if(address.notPupup($config.shoppingInstenceAtom.address)){
					renderInvoice({success:true,data:data.data.ivh});
				}
				renderRefP();
			})(alldata);
		}));
	}
	function reloadVirtualCardSite(biz){
		
		//初始化订单
		return getapi('order/initOrder').then(commonErrorWarp(function initorderApiCallback(data){
			$("#address").hide();
			biz.renderEntityCard({success:true,data:data.data.smnpc});
			renderPayment({success:true,data:data.data.pg});
			biz.renderListOfItemShopping({success:true,data:data.data.gvccis});
			renderPreferential({success:true,data:data.data.vrbsos});
			renderCommitOrder({success:true,data:data.data.checkoutSummary});
			renderInvoice({success:true,data:data.data.ivh});
			//渲染人员推荐号
			renderRefP();

		}));
	}
	function reloadPrimary(){
		//初始化订单
		return getapi('order/initOrder').then(commonErrorWarp(function initorderApiCallback(data){
			renderAddress({success:true,data:data.data.cis});
			if($page.site=="presell"){//如果是预售站点
				$config.shoppingAtom.deliveryPreSell=data.data.deliveryPreSell;
			}
			renderPayment({success:true,data:data.data.pg});
			renderListOfItem({success:true,data:data.data.dsgs});
			renderPreferential({success:true,data:data.data.vrbsos});
			renderCommitOrder({success:true,data:data.data.checkoutSummary});
			
			//如果地址不是是展开情况 渲染发票
			if(address.notPupup($config.shoppingInstenceAtom.address)){
				renderInvoice({success:true,data:data.data.ivh});
			}
			//渲染人员推荐号
			renderRefP();

		}));
	}

	


	
	//*********************收获地址交互***********************************
	//UI上面的数据 转换为接口数据
	function addressUiDataToParams(data){
		var areas=data.address.split(".");
		return param={
			firstName:data.consignee,
			address:data.detailAddress,
			mobile:data.phone,
			phoneNumber:data.call,
			state:areas[0],
			city:areas[1],
			county:areas[2],
			town:areas[3],
			email:data.email,
			isDefault:data.defaultAddress
		};
	}
	//UI上面的数据 转换为接口数据 携带ID
	function addressUiDataToParamsWithId(data){
		return u.pipe(addressUiDataToParams,u.assoc("id",data.id))(data);
	}
	
	//库存提示触发
	var reloadC = branchErrorWarp(reloadAll,function(data){
		//无货不支持配送提示
		uw.watchOnce(listOfItem.TYPE,"renderEnd",notStockNotSupportTip);
		return reloadAll(data)
	});
	//修改收获地址
	function apiUpdateAddress(data){
		onlyGetapi(updateAddressUrl,ui.filterNullOrEmptyObj(data)).then(reloadC);
	}
	//新增收获地址
	function apiNewAddress(data){
		onlyGetapi(addAddressUrl,ui.filterNullOrEmptyObj(data)).then(reloadC);
	}
	//默认收获地址
	function apiSetDefaultAddress(item){
		getapi('consignee/setDefaultAddress',{id:item.origin.owerId}).then(whenErrorWarp(reloadAll));
	}
	//删除收获地址
	function apiRemoveAddress(item){
		onlyGetapi('consignee/removeAddress',{id:item.origin.owerId}).then(reloadC);
	}
	//选中收获地址
	function apiSelectedAddress(owerId) {
		onlyGetapi('consignee/selectAddress',{id:owerId})
		.then(reloadC); 
	}
	//ui接口之间关系描述
	uw.watchDSL(address.TYPE,{
		selected:u.pipe(u.path(["origin","owerId"]),apiSelectedAddress),
		updateAddress:u.pipe(addressUiDataToParamsWithId,apiUpdateAddress),
		newAddress:u.pipe(addressUiDataToParams,apiNewAddress),
		setDefaultAddress:apiSetDefaultAddress,
		removeAddress:apiRemoveAddress
	});

	//**************支付方式交互****************************

	//保存支付方式 出错情况下 刷新自己 不出错情况下刷新 配送方式 用户资产 价格展示 支付方式
	function apiSavePayment(code){
		onlyGetapi('payment/savePayment',{pm:code}).then(alwayErrorWarp(reloadAll));
	}
	//保存门店支付方式
	function apiSavePaymentStore(data){
		onlyGetapi('payment/savePayment',{pm:data.code,storeid:data.id,storename:data.name}).then(alwayErrorWarp(reloadAll));
	}
	function rerenderCommitOrder(){
		var im=$config.shoppingInstenceAtom.commitOrder;
		commitOrder.render(im);
	}
	//保存尾款手机号
	function saveTailPhone(data){
		onlyGetapi("transport/savePreSellSMSMobile",{
			mobile:data.phone,
			sno:data.shopNo,
			sgid:data.shippingGroupId
		}).then(alwayErrorWarp(reloadAll));
	}
	uw.watchDSL(payment.TYPE,{
		savePayment:u.pipe(u.prop("c"),apiSavePayment),
		renderCommitOrder:rerenderCommitOrder,
		saveTailPhone:saveTailPhone,
		savePaymentStore:apiSavePaymentStore
	});
	
	//*************配送方式********************
	//保存快递
	function apiSelectExpress(data){
		onlyGetapi("transport/saveShippingMethod",{
			sm:data.selectedExpress.code,
			shopno:data.shop.shopId,
			sgid:data.shop.shippingGroupId
		}).then(alwayErrorWarp(reloadAll));
	}
	//保存门店自提
	function selectPickingUp(data){
		onlyGetapi("transport/saveGomeStore",{
			shopno:data.shop.shopId,
			sm:data.selectedExpress.code,
			sgid:data.shop.shippingGroupId,
			storeid:data.selectedExpress.storeId
		}).then(alwayErrorWarp(reloadAll));		
	}
	//保存配送清单上的支付方式
	function apiSaveShipPayment(data){
		onlyGetapi("payment/savePayment",{t:data.code,pm:data.parentCode}).then(alwayErrorWarp(reloadAll));
	}
	function apiSaveComments(data){
		onlyGetapi("transport/saveComments",{cm:data.cm,shopno:data.shop.shopId,sgid:data.shop.shippingGroupId})
		.then(whenErrorWarp(reloadAll));
	}
	//保存配送时间
	function apiSelectTime(data){
		var param=null;
		if(data.stime.code=="DAY"){
			param={
				udt:data.sitem.code,
				fdt:data.stime.type,
				ft:data.stime.fixedTime,
				udtl:data.stime.selectSlot
			}
		}
		if(data.stime.code=="XSD"){
			param={
				udt:data.stime.postBackCode,
				fdt:data.stime.type,
				ft:data.sitem.endTime,
				udtl:data.sitem.code
			}
		}
		if(data.stime.code=="JSD"){
			param={
				udt:data.stime.postBackCode,
				fdt:data.sitem.type,
				ft:data.sitem.fixedTime,
				udtl:data.sitem.slotCode
			}
		}
		param.shopno=data.shop.shopId;
		param.sgid=data.shop.shippingGroupId;
		getapi("transport/saveDeliveryTime",ui.filterNullOrEmptyObj(param)).then(alwayErrorWarp(reloadAll));
	}
	//保存挂架安装
	function apiGJAZ(a){
		getapi("transport/saveRackInstall",{racktype:a}).then(whenErrorWarp(reloadAll));
	}
	uw.watchDSL(listOfItem.TYPE,{
		selectExpress:apiSelectExpress
		,selectPickingUp:selectPickingUp
		,savePayment:apiSaveShipPayment
		,saveComments:apiSaveComments
		,selectTime:apiSelectTime
		,gjaz:apiGJAZ
		,backCart:u.pipe(writeAreaCookie,gotoCart)
	})
	//*************发票交互*********************
	//刷新发票模块
	function apiGetInvoice(){
		getapi("invoice/getInvoice").then(renderInvoice);
	}
	//保存不开发票
	function apiNoSaveInvoice(flag){
		onlyGetapi("invoice/saveInvoice",{need:false}).then(whenErrorWarp(apiGetInvoice));
	}
	//保存开发票
	function apiSaveInvoice(data){
		var param={
			need:true,
			typeCode:data.selectedInvoce.invoiceType.code
		};
		param.headCode=(data.selectedHead||{}).code;
		param.head=(data.selectedHead||{}).content;
		param.taxpayerNo=data.selectedInvoce.taxPayerNo;
		param.details=data.selectedContentType.code;
		param.elecMobile=data.selectedInvoce.mobilePhone;
		param.elecMail=data.selectedInvoce.email;
		param.VATName=data.selectedInvoce.consigneeName;
		param.VATAddress=data.selectedInvoce.registeredAddress;
		param.VATPhone=data.selectedInvoce.consigneeMobilePhone;
		if(data.selectedInvoce.consigneeInfo){
			param.firstName=data.selectedInvoce.consigneeInfo.name;
			param.state=data.selectedInvoce.consigneeInfo.address.provinceCode
			param.city=data.selectedInvoce.consigneeInfo.address.cityCode;
			param.county=data.selectedInvoce.consigneeInfo.address.countyCode;
			param.town=data.selectedInvoce.consigneeInfo.address.townCode;
			param.address=data.selectedInvoce.consigneeInfo.address.detailedAddress;
			param.mobile=data.selectedInvoce.consigneeInfo.mobileNumber;
			param.phoneNumber=data.selectedInvoce.consigneeInfo.phoneNumber;
			param.email=data.selectedInvoce.consigneeInfo.email
		}
		getapi("invoice/saveInvoice",ui.filterNullOrEmptyObj(param)).then(alwayErrorWarp(apiGetInvoice));
	}
	
	uw.watchDSL(invoice.TYPE,{
		noSaveInvoice:apiNoSaveInvoice,
		saveInvoice:apiSaveInvoice,
		closeInvoice:apiGetInvoice
	});

	//***************各种券*********************
	//查询推荐人员工号
	function renderRefP(){
		onlyGetapi("coupon/getRef").then(intor.unlessError(function(data){
			$config.shoppingAtom.referrerInfo=data.data;
			var im=$config.shoppingInstenceAtom.preferential;
			preferential.render(im);
		}));
	}
	//勾选蓝券
	function apiSelectB(data){
		onlyGetapi("coupon/selectBlueCoupon",{tid:data.id}).then(alwayErrorWarp(reloadAll));	
	}
	//取消勾选蓝券
	function apiCanelSelectB(data){
		onlyGetapi("coupon/cancelBlueCoupon",{tid:data.id}).then(alwayErrorWarp(reloadAll));	
	}
	//勾选红券
	function apiSelectR(data){
		onlyGetapi("coupon/selectRedCoupon",{tids:data.id}).then(alwayErrorWarp(reloadAll));
	}
	//取消勾选红券
	function apiCalenSelectR(data){
		onlyGetapi("coupon/cancelRedCoupon",{tids:data.id}).then(alwayErrorWarp(reloadAll));
	}
	//勾选店铺券
	function apiSelectD(data){
		onlyGetapi("coupon/selectShopCoupon"
			,{tid:data.couponNo,shopno:data.shopNo,sgid:data.sgid})
		.then(alwayErrorWarp(reloadAll));
	}
	//取消勾选店铺券
	function apiCanelSelectD(data){
		onlyGetapi("coupon/cancelShopCoupon"
			,{tid:data.couponNo,shopno:data.shopNo,sgid:data.sgid})
		.then(alwayErrorWarp(reloadAll));
	}
	//勾选电子券
	function apiSelectDZ(data){
		onlyGetapi("payment/selectECoupon"
			,{eno:data.ecouponCode})
		.then(alwayErrorWarp(reloadAll));
	}
	//取消勾选电子券
	function apiCanelSelectDZ(data){
		onlyGetapi("payment/cancelECoupon"
			,{eno:data.ecouponCode})
		.then(alwayErrorWarp(reloadAll));
	}
	//勾选国美在线积分
	function apiSelectZXJF(data){
		onlyGetapi("payment/useIntegral").then(alwayErrorWarp(reloadAll));
	}
	//取消勾选国美在线积分
	function apiCanelselectZXJF(data){
		onlyGetapi("payment/cancleIntegral").then(alwayErrorWarp(reloadAll));
	}
	//勾选账户余额
	function apiSelectZHYE(data){
		onlyGetapi("payment/useVirtualAccount").then(alwayErrorWarp(reloadAll));
	}
	//取消勾选账户余额
	function apiCanelselectZHYE(data){
		onlyGetapi("payment/cancelVirtualAccount").then(alwayErrorWarp(reloadAll));
	}
	//门店积分兑换
	function changeInto(data){
		onlyGetapi("payment/pointExchange",{pa:data.text}).then(branchWarp(data.errorfn,u.pipe(u.tap(data.okfn),reloadAll)));
	}
	//激活优惠券
	function activeYCode(data){
		onlyGetapi("coupon/activateCoupon",{
			cpno:data.cpno,
			acode:data.acode,
			captcha:data.c,
			capcd:data.capCd
		}).then(branchWarp(data.errorfn,reloadAll));
	}
	//使用电子券
	function dzqAction(data){
		onlyGetapi("payment/verifyECoupon",{eno:data.txt}).then(alwayErrorWarp(reloadAll));
	}
	//推荐人员工号
	function ryghAction(data){
		onlyGetapi("payment/applyStoreSeller",{sellerid:data.txt}).then(branchWarp(data.errfn,renderRefP));
	}
	//支付密码
	function payRegAction(data){
		function doErr(rdata){
			//如果密码被锁定
			if(rdata.errCode=="0010330040"){				
				return reloadAll();
			}
			return data.errorfn(rdata);
		}
		onlyGetapi("payment/validatePayPwd",{
			paypasswd:data.paypasswd
		}).then(branchWarp(doErr,reloadAll));
	}
	//取消推荐人员工
	function ryghCanel(data){
		onlyGetapi("payment/cancelStoreSeller").then(alwayErrorWarp(renderRefP));
	}
	//勾选国美E卡支付
	function apiSelectEC(data){
		onlyGetapi("card/selectECard",{
			cno:data.cardCode
		}).then(alwayErrorWarp(reloadAll));
	}
	//取消勾选国美E卡支付
	function apicanelselectEC(data){
		onlyGetapi("card/cancelECard",{
			cno:data.cardCode
		}).then(alwayErrorWarp(reloadAll));
	}
	//绑定国美E卡
	function bindECard(data){
		req.reqp("http://safe"+cookieDomain+"/myaccount/prepaidCard/fingerPrint"
			,{}
			,'ckdata001')
		.then(function(){
			req.reqp("http://safe"+cookieDomain+"/myaccount/prepaidCard/bindCard"
				,{
					"prepaidCardNo": data.ecartNumber
					,"imageCode":data.imageCode
					,"capCd":data.capcd
				},"ckdata002")
			.then(function(data){
				if(data.error){
					panel.error(data.error.message);
					$config.shoppingAtom.ecard_yzm="";
					reloadAll()
				}else{
					panel.success("美通卡绑定成功！")
					$config.shoppingAtom.ecard_4="";
					$config.shoppingAtom.ecard_3="";
					$config.shoppingAtom.ecard_2="";
					$config.shoppingAtom.ecard_1="";
					$config.shoppingAtom.ecard_yzm="";
					reloadAll()
				}
			})
		});
	}
	//未开启支付密码弹窗提示；开启走请求接口
	function checkPaymentPassword(fn){
		return function(data){
			if(preferential.isNotUnsealPwd($config.shoppingInstenceAtom.preferential)){
				panel.alert($config.renderError({errCode:"f1"}));
				onlyGetapi("coupon/getUserAllAssets").then(reloadAll);
				return;
			}
			return fn(data);
		}
	}
	uw.watchDSL(preferential.TYPE,{
		selectB:apiSelectB
		,canelselectB:apiCanelSelectB
		,selectR:checkPaymentPassword(apiSelectR)
		,canelselectR:apiCalenSelectR
		,selectD:apiSelectD
		,canelselectD:apiCanelSelectD
		,selectDZ:apiSelectDZ
		,selectEC:apiSelectEC
		,canelselectEC:apicanelselectEC
		,canelselectDZ:apiCanelSelectDZ
		,selectZXJF:checkPaymentPassword(apiSelectZXJF)
		,canelselectZXJF:apiCanelselectZXJF
		,selectZHYE:checkPaymentPassword(apiSelectZHYE)
		,canelselectZHYE:apiCanelselectZHYE
		,changeInto:changeInto
		,activeYCode:activeYCode
		,dzqAction:dzqAction
		,ryghAction:ryghAction
		,payRegAction:payRegAction
		,ryghCanel:ryghCanel
		,bindECard:bindECard
	});

	//***************价格展示*******************
	
	
	//提交订单  成功跳转至提交订单成功页面  遇到错误弹出错误提示并刷新价格
	function apiCommit(){
		var r=notStockNotSupportTip($config.shoppingInstenceAtom.listOfItem);
		if(r){
			return reloadAll();
		}
		
		if(isGomeVirtualCardSite()){
			if(!entityCard.validatefn($config.shoppingInstenceAtom.entityCard)){
				window.scrollTo(0,0);
				commitOrder.render($config.shoppingInstenceAtom.commitOrder);
				return false;
			}
		}
		//跳转至提交订单成功页
		getapi("checkout/commit").then(branch(intor.unlessError(function(data){
			if(data.data.jumpPage=="cashier"){
				ui.redir("http://success"+cookieDomain+"/order/paymentInfo?orderIdsStr="+data.data.cartId+"&userId="+$.cookie("sid")+"&isCommittedPage=true");
			}else{
				ui.redir("/order-success?o="+data.data.cartId);
			}
		}),whenErrorWarp(reloadAll)));
	}

	uw.watchDSL(commitOrder.TYPE,{
		commit:apiCommit
	});

	


	//配置配送清单
	function getPickingUpStores(citycode){
		return getapi("transport/getGomeStores",{citycode:citycode});
	}
	listOfItemMethod.$$.getPickingUpStores=getPickingUpStores;

}(
	this,
	util,
	util_ui,
	util_watch,
	request,
	address,
	payment,
	listOfItem,
	commitOrder,
	preferential,
	invoice,
	interceptor,
	listOfItemMethod);
