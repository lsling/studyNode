!function(exports,u,ui,uw,req,panel,tpl,validate){
	var TYPE="7address7";
	var emit=uw.emit(TYPE);
	function datafn(im){
		return im("data");
	}
	function div(im){
		return im('div');
	}
	function make(div,data){
		init(div,data);
		function r(flg){
			return {div:div,data:data}[flg]
		}
		emit("renderEnd",r);
		return r;
	}
	//收获地址滚动条位置
	function scrollTopAction(div){
		$config.shoppingAddressAtom.scrollY=0;
	}
	function setScrollAction(div){
		$config.shoppingAddressAtom.scrollY=div.find("#address_scroll_div").scrollTop();
	}
	function scrollAction(div){
		div.find("#address_scroll_div").scrollTop($config.shoppingAddressAtom.scrollY);
	}
	function init(div,data){
		if(data.open){
			var selectedAddress=u.find(u.prop("selected"),data.list)||{};
			selectedAddress.close=false;
			selectedAddress.type="noAddress";
			newAddress(div,selectedAddress);
		}
		//渲染收货地址主模块
		div.html(tpl.address_main(data));
		lastdata=data;
		scrollAction(div);
		div.find("#address_scroll_div").scroll(function(){setScrollAction(div)});
		//新增收货人地址 鼠标划过变红
		// div.find("#id_newAddress_btn")
		// .hover(function(){
		// 	$(this).find("em").removeClass("addicon").addClass("add_icon-red");
		// },function(){
		// 	$(this).find("em").removeClass("add_icon-red").addClass("addicon");
		// });
		//鼠标划过 其下方操作按钮显示
		div
		.find("[g-hover-up]")
		.each(function(){
			$(this).hover(findAndShow("[g-hover]"),findAndHide("[g-hover]"))
		});
		//点击新增按钮
		div.find("#id_newAddress_btn").click(u.partial(newAddress,[div,{close:true,type:"new",data:data}]));
		//点击更多
		div.find("[g-more-path]").on("click",function(){
			var el=$(this);
			var value=el.attr("g-value");
			$config.shoppingAddressAtom.more=value;
			init(div,data);
		});
		//选中收获地址
		div.find("[g-sbtn-path]").on("click",function(){
			var div1=$(this);
			var path=div1.attr("g-sbtn-path").split(",");
			function selectfn(data){
				var item=u.path(path,data);
				emit("selected",item);
				return u.map(function(a){
					if(a==item)return u.assoc("selected",true,a);
					return u.assoc("selected",false,a);
				},data.list);
			}
			init(div,u.pipe(u.assoc("list", selectfn(data)))(data));
		});
		
		//设为默认地址
		div.find("[g-default-path]").on("click"
			,ui.jqpipe(ui.attr("g-default-path")
				,u.split(",")
				,u.converge(reqDefaultAddress,[u.path(u.__,data),u.always(data)])
				,u.partial(init,[div])));
		//删除
		div.find("[g-delete-path]").on("click"
			,ui.jqpipe(ui.attr("g-delete-path")
				,u.split(",")
				,u.path(u.__,data)
				,reqDeleteAddress));
		//修改
		div.find("[g-modify-path]").on("click"
			,ui.jqpipe(
				ui.attr("g-modify-path")
				,u.split(",")
				,u.path(u.__,data)
				,u.assoc("type","modify")
				,u.assoc("close",true)
				,u.partial(newAddress,[div])));
	}
	var findAnd = u.curry(function(selector,fn){
		return ui.jq(u.pipe(ui.find(selector),fn));
	});
	function findEmAddClass(clazz){
		return ui.jqpipe(ui.find("em"),ui.addClass(clazz));
	}
	function findEmRemoveClass(clazz){
		return ui.jqpipe(ui.find("em"),ui.removeClass(clazz));
	}
	function findBAddRemoveClass(clazz,rclazz){
		return ui.jqpipe(ui.find("b"),ui.addClass(clazz),ui.removeClass(rclazz));
	}
	var findAndShow = findAnd(u.__,ui.show);
	var findAndHide = findAnd(u.__,ui.hide);


	function reqDefaultAddress(item,data){
		emit("setDefaultAddress",item);
		function isdefault(a){
			return a.isdefault;
		}
		function defaultAddressfn(item){
			return u.map(u.ifn(u.eq(item),u.assoc("isdefault",true),u.assoc("isdefault",false)))
		}
		function defaultToTop(arr){
			return u.concat(u.filter(isdefault,arr),u.reject(isdefault,arr));
		}
		return u.assoc("list"
			,u.pipe(defaultAddressfn(item),defaultToTop)(data.list)
			,data);
	}
	function reqDeleteAddress(item){
		panel.confirmOk({title:"删除地址?",body:"您确定要删除该收货地址吗？"},function(dialog){
			dialog.hide();
			emit("removeAddress",item);
		});
	}

	//当前选中的收货地址
	function selectedAddress(im){
		var data=datafn(im);
		return u.find(u.prop("selected"),data.list);
	}
	//修改选中的收获地址
	function modify(im){
		var data=selectedAddress(im);
		data=u.assoc("close",true,data);
		data=u.assoc("type","modify",data);
		return newAddress(div(im),data);
	}
	function newAddress(div,data){
		if(data.type=="new"){
			if(data.data.list.length>=20){
				panel.confirmOk($config.notice.addressConfirm,function(dialog){
					data.type="confirm-new";
					dialog.hide();
					return newAddress(div,data);
				});
				return;
			}
		}
		var dialog = $.gDialog({
			html: tpl.address_new(data),
			modal:{}
		});
		dialog.show();
		ui.gpipes(dialog.$dialog,{
		});
		var uncheckbox=u.pipe(ui.removeClass("checkbox_chose"),ui.addClass("checkboxs"),ui.setAttr("value","0"));
		var checkbox=u.pipe(ui.removeClass("checkboxs"),ui.addClass("checkbox_chose"),ui.setAttr("value","1"));
		var defaltAddressCheckboxCallback=ui.jq(u.ifn(ui.is(".checkbox_chose"), uncheckbox, checkbox));

		//设置默认地址
		$("#id_newAddress [name=defaultAddress]").click(defaltAddressCheckboxCallback);
		
		function errStylefn(err,tipel,textel){
			function fn1(){
				tipel.hide();
				textel.removeClass("error")
			}
			function fn2(err){
				textel.addClass("error");
				tipel.html('<span style="color:#f00;">'+err+"</span>");
				tipel.show();
			}
			return u.ifn(u.isEmpty,fn1,fn2)(err);
		}
		//手机号
		if(data.type=="modify"){
			dialog.$dialog.find("input[g-validate=phone]").one("focus",function(){
				$(this)
				.val('')
				.attr("m","true");
			});
		}

		dialog.$dialog.find("[g-validate]").blur(function(){
			var vval=$(this).attr("g-validate");
			var fillel=dialog.$dialog.find("[g-tip-validate="+vval+"]");
			var textel=dialog.$dialog.find("[g-validate="+vval+"]");
			var vals=$(this).val()||$(this).attr("value");
			function errcbk(err){
				return errStylefn(err,fillel,textel);
			}
			var vas=[[vals,vval,errcbk]];
			if(vval=="phone"||vval=="call"){
				var vals2=[dialog.$dialog.find("[name=phone]").val(),
					dialog.$dialog.find("[name=call]").val()];
				vas.push([vals2,"phone-call",function(err){
					var textel=dialog.$dialog.find('[g-validate=phone-call]');
					var tipel=dialog.$dialog.find('[g-tip-validate=phone-call]');
					return errStylefn(err,tipel,textel);
				}]);
			}
			validate.validate(vas);
		});
		//保存地址
		$("#id_saveAddress").on("click",function(){
			var velkeys=[];
			function getPhoneValidatStr(el){
				if(data.type=="modify"){
					if(el.attr("m")!="true"){
						//如果点击修改，并且没有被修改过手机号
						return "17090114267";
					}
				}
				return el.val();
			}
			dialog.$dialog.find("[g-validate]").each(function(){
				var vval=$(this).attr("g-validate");
				var fillel=dialog.$dialog.find("[g-tip-validate="+vval+"]");
				var textel=dialog.$dialog.find("[g-validate="+vval+"]");
				var vals=$(this).val()||$(this).attr("value");
				if(vval=="phone"){
					vals=getPhoneValidatStr(textel);
				}
				function errcbk(err){
					return errStylefn(err,fillel,textel);
				}
				if(vval=="phone-call"){
					var phonetxt=dialog.$dialog.find("[name=phone]");
					var vals=[getPhoneValidatStr(phonetxt), dialog.$dialog.find("[name=call]").val()];
				}
				velkeys.push([vals,vval,errcbk]);

			});
			if(validate.validate(velkeys)){
				var pobj=ui.searForm($("#id_newAddress"));
				if(data.type=="modify"){
					emit("updateAddress",u.assoc("id",data.origin.owerId,pobj));
				}else{
					emit("newAddress",pobj);
				}
				dialog.hide();
			}
			
			
		});
		var dialogHideCallBack=ui.cbk(dialog,ui.hide);
		//关闭
		$("#closeaddress").click(dialogHideCallBack);
		//选择地区
		var gcdat=(function(data){
			if(data.type!="modify")return null;
			return [
				data.origin.address.countyCode,
				data.origin.address.provinceName+data.origin.address.cityName+data.origin.address.countyName+data.origin.address.townName,
				data.origin.address.cityCode,
				data.origin.address.provinceCode,
				data.origin.address.townCode
			].join("|");
		}(data));
		$("#id_address_select").gCity({
			gc_dat:gcdat,
			gc_ads:'chtm',
			gc_evt:function(){
				var value=this.sid+"."+this.cid+"."+this.xid+"."+this.zid;
				$("#id_address_select").find("[name=address]").attr("value",value);
				$("#id_address_select").find("[show-label]").html(this.chtm);
				$("#id_address_select").find(".add_out,.gCity").hide();
				dialog.$dialog.find("[g-tip-validate=address]").html("");
			}
		});
		ui.hoverUpBySelector($("#id_address_select"),".add_out,.gCity");
		dialog.$dialog.find("[g-area-close]").on("click",function(){
			dialog.$dialog.find(".add_out,.gCity").hide();
		})
	}	

	function notPupup(im){
		return !u.isEmpty(datafn(im).list);
	}
	exports.address={
		make:u.curry(make),
		TYPE:TYPE,
		modify:modify,
		selectedAddress:selectedAddress,
		notPupup:notPupup
	};
}(this,util,util_ui,util_watch,request,panel,GTPL,validate);