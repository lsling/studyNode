/**身份验证 美通卡站点*/
!function(exports,u,ui,uw,tpl,validate){
	var TYPE="7entityCard7";
	var emit=uw.emit(TYPE);
	function div(im){
		return im("div");
	}
	function datfn(im){
		return im('data');
	}
	var set=u.curry(function(im,data){
		im(null,data);
		return im;
	});
	
	function validatefn(im){
		div(im).find("#diqye-mbile").trigger("blur");
		return validate.validateItem("mtk-phone",div(im).find("#diqye-mbile").val(),function(){});
	}
	function render(im){
		div(im).html(tpl.entity_card_main({sm:datfn(im)}));
		var commentTimeid=null;
		div(im).find("#diqye-mbile").on("blur",function(){
			var val=$(this).val();
			var div=$(this).parent("div");
			function errfn(err){
				if(err){
					div.addClass("error");
					div.find(".errfix").html(err);
				}else{
					div.removeClass("error");
				}
			}
			if(!validate.validateItem("mtk-phone",val,errfn)){
				return false;
			}
			emit("savePhone",{phone:val});
		});

	}
	function make(div,data){
		function r(flg,data1){
			if(data1)data=data1;
			return {
				div:div,
				data:data
			}[flg];
		}
		render(r);
		return r;
	}
	exports.entityCard={
		make:u.curry(make),
		TYPE:TYPE,
		validatefn:validatefn
	};
}(this,util,util_ui,util_watch,GTPL,validate);