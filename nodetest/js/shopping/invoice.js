/**发票信息*/
!function(exports,req,u,ui,uw,tpl,panel,validate){
	var TYPE="7invoice7";
	var emit=uw.emit(TYPE);
	function div(im){
		return im('div');
	}
	function dfn(im){
		return im('data');
	}
	function clone(obj){
		function copyArray(arr){
			var r=[];
			for(var i=0;i<arr.length;i++)r.push(clone(arr[i]));
			return r;
		}
		function copyObj(a){
			var r={};
			for(var key in a)r[key]=clone(a[key]);
			return r;
		}
		return u.cond([
			[u.is(Array),copyArray],
			[u.is(Object),copyObj],
			[u.T,u.identity]
		])(obj);
	}
	function cloneim(im){

	}
	function setPath(path,val,im){
		im('data',transData(u.assocPath(path,val,dfn(im))));
		return im;
	}
	function render(im){
		var data=dfn(im);
		if(data.open){
			renderModify(im,false);
		}else{
			div(im).html(tpl.invoice_main(dfn(im)));
		}
		alternation(im);
	}
	function setDefaultConsigneeInfo(data){
		//获取纸质发票
		var zzfp=u.find(function(a){return a.invoiceType.code==0},data.invoices);
		if(zzfp.consigneeInfo==null){
			zzfp.consigneeInfo={
				address:{
				}
			}
		}
	}
	function transData(data){
		if(data.invoices.length==0)return data;
		if($config.isGomeVirtualCardSite()){
			setDefaultConsigneeInfo(data)
		}
		var selectedInvoce=u.find(u.path(["invoiceType","selected"]),data.invoices)
		var selectedHead=u.find(u.path(["selected"]),selectedInvoce.headTypes||[]);
		var selectedContentType=u.find(u.path(["selected"]),selectedInvoce.invoiceContentTypes)||{};

		data.selectedInvoce=selectedInvoce;
		data.selectedInvoce.path="invoices,"+u.findIdx(u.path(["invoiceType","selected"]),data.invoices);
		data.selectedHead=selectedHead||{};
		data.selectedHead.path=selectedInvoce.path
		+",headTypes,"
		+u.findIdx(u.path(["selected"]),selectedInvoce.headTypes||[]);

		data.selectedContentType=selectedContentType;
		data.selectedContentType.path=data.selectedInvoce.path
		+",invoiceContentTypes,"
		+u.findIdx(u.path(["selected"]),selectedInvoce.invoiceContentTypes);

		return data;
	}
	function alternation(im){
		//修改操作
		div(im).find("[g-modify-path]").on("click",u.partial(renderModify,[im,true]));
	}
	var selectedfn=u.curry(function(arr,sitem){
		return u.map(function(item){
			if(item==sitem)return u.assoc("selected",true,item);
			return u.assoc("selected",false,item);
		},arr);
	});
	function renderModify(im,haveClose){
		var dialog = $.gDialog({
			html: tpl.invoice_modify(u.assoc("haveClose",haveClose,dfn(im)))
			,modal:{
			}
		});
		dialog.show();
		function renderModifyIn(im1){
			return renderModify(im1,haveClose);
		}
		//选择地区
		var gcdat=(function(data){
			if(data.selectedInvoce.consigneeInfo){
				var consigneeInfo=data.selectedInvoce.consigneeInfo
				return [
					consigneeInfo.address.countyCode,
					consigneeInfo.address.provinceName+consigneeInfo.address.cityName+consigneeInfo.address.countyName+consigneeInfo.address.townName,
					consigneeInfo.address.cityCode,
					consigneeInfo.address.provinceCode,
					consigneeInfo.address.townCode
				].join("|");
			}
		}(dfn(im)));
		dialog.$dialog.find("#id_address_select").gCity({
			gc_dat:gcdat,
			gc_ads:'chtm',
			gc_evt:function(){
				var value=this.sid+"."+this.cid+"."+this.xid+"."+this.zid;
				$("#id_address_select").find("[name=address]").attr("value",value);
				$("#id_address_select").find("[show-label]").html(this.chtm);
				$("#id_address_select").find(".add_out,.gCity").hide();
				dialog.$dialog.find("[g-tip-validate=address]").html("");
				var div=dialog.$dialog.find("#id_address_select");
				var path=div.attr("value-path").split(",");
				div.find("[name='address']").removeClass('error')
				setPath(path,{
					"provinceCode": this.sid, 
					"provinceName":this.snam, 
					"cityCode": this.cid, 
					"cityName": this.cnam, 
					"countyCode": this.xid, 
					"countyName": this.xnam, 
					"townCode": this.zid, 
					"townName": this.znam,
					"detailedAddress":dialog.$dialog.find("[name='detailedAddress']").val()
				},im);
			}
		});
		dialog.$dialog.find('[name="address"]').toggle(function(){
			dialog.$dialog.find('.add_out,.gCity').show()
		},function(){
			dialog.$dialog.find('.add_out,.gCity').hide()
		})
		dialog.$dialog.find("[g-area-close]").on("click",function(){
			dialog.$dialog.find(".add_out,.gCity").hide();
		})
		//关闭操作
		dialog.$dialog.find("[g-close]").on("click",function(){
			ui.hide(dialog);
			emit("closeInvoice",null);
		});
		//开具发票
		dialog.$dialog.find("[g-value-path]").on("click",function(){
			var el=$(this);
			var path=ui.gvaluePath(el);
			var value=ui.gvalue(el);
			dialog.hide();
			renderModifyIn(setPath(path,value,im));
		});
		//发票类型
		dialog.$dialog.find("[g-i-path]").on("click",ui.jqpipe(
			ui.attr("g-i-path"),
			u.split(","),
			u.path(u.__,dfn(im)),
			function(sitem){
				dialog.hide();
				return setPath(["invoices"],u.map(function(invoice){
					if(sitem==invoice)return u.assocPath(["invoiceType","selected"],true,invoice);
					return u.assocPath(["invoiceType","selected"],false,invoice);
				},dfn(im).invoices),im);
			},
			renderModifyIn));
		//发票抬头
		dialog.$dialog.find("[g-h-path]").on("click",ui.jqpipe(
			ui.attr("g-h-path"),
			u.split(","),
			u.path(u.__,dfn(im)),
			function(sitem){
				dialog.hide();
				var path=dfn(im).selectedInvoce.path.split(",").concat(["headTypes"]);
				return setPath(path,selectedfn(u.path(path,dfn(im)),sitem),im);
			},
			renderModifyIn));
		
		//发票内容
		dialog.$dialog.find("[g-c-path]").on("click",ui.jqpipe(
			ui.attr("g-c-path"),
			u.split(","),
			u.path(u.__,dfn(im)),
			function(sitem){
				dialog.hide();
				var path=dfn(im).selectedInvoce.path.split(",").concat(["invoiceContentTypes"]);
				return setPath(path,selectedfn(u.path(path,dfn(im)),sitem),im);
			},
			renderModifyIn));
		//输入框和数据映射
		dialog.$dialog.find("[value-path]").on("keyup",function(){
			var el=$(this);
			var path=u.pipe(ui.attr("value-path"),u.split(","))(el);
			var value=el.val();
			setPath(path,value,im);
		});
		//tip提示
		ui.hoverUp(dialog.$dialog.find("[g-tip=dzfp],[g-tip-for=dzfp]"))(dialog.$dialog.find("[g-tip-for=dzfp]"));
		//手机号加星前端策略
		dialog.$dialog.find("[no-modify]").one("mousedown",function(){
			$(this)
			.removeAttr("no-modify")
			.val('')
			.trigger("keyup");
		})
		//失去焦点验证
		dialog.$dialog.find("[g-validate]").blur(function(){
			var vval=$(this).attr("g-validate");
			var path=u.pipe(ui.attr("value-path"),u.split(","))($(this));
			var fillel=dialog.$dialog.find("[g-tip-validate="+vval+"]");
				var textel=dialog.$dialog.find("[g-validate="+vval+"]");
			function errcbk(msg){
				if(u.isEmpty(msg)){
					textel.removeClass("error");
				}else{
					textel.addClass("error");
				}
				fillel.html(msg);
				setPath(u.concat(u.init(path),[vval]),msg,im);
			}
			var vals=$(this).val()||$(this).attr("value");
			var vas=[[vals,vval,errcbk]];
			validate.validate(vas);
		});
		dialog.$dialog.find("#saveInvoice").on("click",function(){
			var data=transData(dfn(im));
			if(data.invoiceNeedType=="N"){
				emit("noSaveInvoice",false);
				dialog.hide();
				render(im);
				return;
			}
			var velkeys=[];
			dialog.$dialog.find("[g-validate]").each(function(){
				var vval=$(this).attr("g-validate");
				var path=u.pipe(ui.attr("value-path"),u.split(","))($(this));
				var fillel=dialog.$dialog.find("[g-tip-validate="+vval+"]");
				var textel=dialog.$dialog.find("[g-validate="+vval+"]");
				function errcbk(msg){
					if(u.isEmpty(msg)){
						textel.removeClass("error");
					}else{
						textel.addClass("error");
					}
					fillel.html(msg);
					setPath(u.concat(u.init(path),[vval]),msg,im);
				}
				var vals=$(this).val()||$(this).attr("value");
				if($(this).attr("no-modify")!="y"){
					velkeys.push([vals,vval,errcbk]);
				}

			});
			if(validate.validate(velkeys)){
				// console.log(data);
				// $.each(data.invoices,function(i,v){
				// 	console.log(v);
				// })
				emit("saveInvoice",data);
				dialog.hide();
				render(setPath(["open"],false,im));
			}
		});
	}

	function make(div,data){
		function r(a,data1){
			if(a=="div")return div;//操作范围
			if(data1!=null){
				data=data1;
				return r;
			}
			if(a=="data")return data;//最新数据
		}
		render(r);
		return r;
	}
	exports.invoice={
		make:u.curry(make),
		transData:transData,
		TYPE:TYPE
	};
}(this,request,util,util_ui,util_watch,GTPL,panel,validate);