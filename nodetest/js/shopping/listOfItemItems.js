/**商品列表*/
!function(exports,req,u,ui,uw,tpl){
	var TYPE="7listOfItem7";
	
	function div(im){
		return im('div');
	}
	function dfn(im){
		return im("data");
	}
	function associm(path,val,im){
		var data=dfn(im);
		var ndata=u.assocPath(path,val,data);
		im(null,ndata);
		return im;
	}
	function render(im){
		im("div").html(tpl.listOfItem_items(dfn(im)));
		alternation(im);
	}
	function alternation(im){
		ui.gpipes(div(im),{
			gjaz:gjaz
		});
		//挂件安装
		function gjaz(gjid,path,value){
			uw.emit(TYPE,"gjaz",gjid);
			var a=associm(path.split("."),value,im);
			render(a);
		}
	}
	function make(div,data){
		function r(a,data1){
			if(a=="div")return div;//操作范围
			if(data1!=null){
				data=data1;
				return r;
			}
			if(a=="data")return data;//最新数据
		}
		render(r);
		return r;
	}
	exports.listOfItemItems={
		make:make,
		TYPE:TYPE
	};
}(this,request,util,util_ui,util_watch,GTPL);