/**价格展示*/
!function(exports,req,u,ui,uw,tpl){
	var TYPE="7commit-order7";
	
	function div(im){
		return im('div');
	}
	function dfn(im){
		return im('data');
	}
	function render(im){
      var data=dfn(im);
      if(data.ppcAmount==null)data.ppcAmount=null;
		div(im).html(tpl.commitOrder_main(dfn(im)));
		alternation(im);
	}
	function alternation(im){
		div(im).find("#id_commit").on("click",function(){
			div(im).find("#id_commit_div").html("<span class=\"fontRed strong pr20\">提交中. . .</span>");
			uw.emit(TYPE,"commit","commit");
		});
		div(im).find("[agreen-play]").on("click",function(){
			$config.shoppingAtom.presell_tyzfdj=!$config.shoppingAtom.presell_tyzfdj;
			return render(im);
		});
	}
	function make(div,data){
		function r(a,data1){
			if(a=="div")return div;//操作范围
			if(data1!=null){
				data=data1;
				return r;
			}
			if(a=="data")return data;//最新数据
		}
		render(r);
		return r;
	}
	exports.commitOrder={
		make:u.curry(make),
		render:render,
		TYPE:TYPE
	};
}(this,request,util,util_ui,util_watch,GTPL);
