/**配送方式*/
!function(exports,req,u,ui,uw,tpl,listMethod,listOfItemItems){
	var TYPE="7listOfItem7";

	function div(im){
		return im(0);
	}
	function datfn(im){
		return im(1);
	}
	function path(arr,im){
		return u.path(arr,datfn(im));
	}
	function transData(data){
		function mapcat(fn,da){
			return u.concats(u.map(fn,da));
		}
		var mapassoc=u.curry(function (name,fn,da){
			return u.assoc(name,u.map(fn,da[name]),da);
		});
		function findItem(a){
			return u.find(function(item){
				return item.itemId==a;
			},mapcat(function(item){
				return mapcat(u.path(["items"]),item.allItems);
			},data));
		}
		return u.map(mapassoc("shoppinginfo",mapassoc("items",findItem)),data);
	}
	function render(im){
		div(im).html(tpl.listOfItem_main(datfn(im)));
		//配送信息
		div(im).find("[info-path]").each(function(){
			var el=$(this);
			var arr=el.attr("info-path").split(",");
			listMethod.make(el,path(arr,im));
		});
		//商品清单
		div(im).find("[list-path]").each(function(){
			var el=$(this);
			var arr=el.attr("list-path").split(",");
			listOfItemItems.make(el,path(arr,im));
		});
		
		div(im).find("#back-cart").click(function(){
			uw.emit(TYPE,"backCart",{});
		});
		//鼠标划过弹出
		div(im).find("[hoverup]").each(ui.jq(ui.hoverUpBySelector(u.__,"[hover]")));
	}
	function renderShopping(im){
		div(im).html(tpl.listOfItem_main(datfn(im)));
		//鼠标划过弹出
		div(im).find("[hoverup]").each(ui.jq(ui.hoverUpBySelector(u.__,"[hover]")));
	}
	function filterItems(fn,data){
		var allitem=u.map(u.pipe(u.prop("allItems"),u.map(u.prop("items"))),data);
		function flatten(list){
			return u.reduce(u.concat,[],u.map(u.ifn(u.is(Array),flatten,u.of),list));
		}
		return u.filter(fn,flatten(allitem));
	}
	function arg2(a,b){
		return b;
	}
	function virtualCardItems(fn,im){
		var data = datfn(im);
		return util.filter(fn,data);
	}
	//过滤所有商品
	//[配送清单]->[item]
	var filterAllItems=u.cond([
		[$config.isGomeVirtualCardSite,virtualCardItems]
		,[u.T,u.converge(filterItems,[u.identity,u.pipe(arg2,datfn)])]
	]);


	//根据商品获取店铺
	function getShopByItem(item,im){
		var fn1=u.find(u.eq(item));
		var fn2=u.find(u.pipe(u.prop("items"),fn1));
		var shop=u.find(u.pipe(u.prop("allItems"),fn2),datfn(im));
		return shop;
	}
	//判断商品是否国美自营
	function isGome(item,im){
		return getShopByItem(item,im).shopId=="GOME";
	}

	
	function make(div,data){
		function resultfn(flag,newdata){
			if(flag==0)return div;
			if(flag==1)return data;
			if(flag==3)data=newdata;
			return data;
		}
		render(resultfn);
		uw.emit(TYPE,"renderEnd",resultfn);
		return resultfn
	}
	function makeShopping(div,data){
		function resultfn(flag,newdata){
			if(flag==0)return div;
			if(flag==1)return data;
			if(flag==3)data=newdata;
			return data;
		}
		renderShopping(resultfn);
		uw.emit(TYPE,"renderEnd",resultfn);
		return resultfn
	}
	exports.listOfItem={
		make:u.curry(make),
		makeShopping:u.curry(makeShopping),
		transData:transData,
		filterAllItems:filterAllItems,
		getShopByItem:getShopByItem,
		isGome:isGome,
		TYPE:TYPE
	};
}(this,request,util,util_ui,util_watch,GTPL,listOfItemMethod,listOfItemItems);