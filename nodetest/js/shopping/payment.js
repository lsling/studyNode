/**支付方式*/
!function(exports,req,u,ui,uw,tpl,gstore,validate){
	var TYPE="7payment7";
	var emit=uw.emit(TYPE);
	function div(im){
		return im("div");
	}
	function datfn(im){
		return im('data');
	}
	var set=u.curry(function(im,data){
		im(null,data);
		return im;
	});
	
	function init(im){
		var el=div(im);
		var data=datfn(im);
		el.html(tpl.payment_main(data));
		
		alternation(im);
	}

	
	function transData(data){
		var labelMap={
			mailRemittance:{
				label:"邮局汇款",
				desc:"线下邮局汇款后1-3个工作日内到账",
				name:"查看账户信息",
				href:"http://help.gome.com.cn/article/236-0-0.html"
			},
			storesPayment:{
				label:"门店付款",
				desc:"网站下单后，在国美门店付款，支持现金、pos机刷卡",
				name:"查看门店支付",
				href:"http://help.gome.com.cn/article/235-0-0.html"
			},
			onlinePayment:{
				label:"在线支付",
				desc:"即时到帐，支持大部分储蓄卡及信用卡，第三方支付平台",
				name:"查看全部银行及限额",
				href:"http://help.gome.com.cn/article/233-0-0.html"
			},
			installment:{
				label:"信用卡分期",
				desc:"用信用卡分期支付，按月还款，减轻资金周转压力",
				name:"查看分期付款帮助",
				href:"http://help.gome.com.cn/article/234-0-0.html"
			},
			companyTransfer:{
				label:"公司转账",
				desc:"通过公司转账后1-3个工作日内到账",
				name:"查看账户信息",
				href:"http://help.gome.com.cn/article/237-0-0.html"
			},
			cashOnDelivery:{
				label:"货到付款",
				desc:"送货上门后再收款，支持现金、POS机刷卡",
				name:" 查看运费及配送范围",
				href:"http://help.gome.com.cn/article/232-0-0.html"
			}
		}
		return {
			list:u.map(function(item){
				return u.assoc("tip",labelMap[item.c],item);
			},data),
			presellModifyStatus:"done"
		};
	}
	function alternation(im){
		var el=div(im);
		var data=datfn(im);
		//鼠标划过弹出
		div(im).find("[hoverup]").each(ui.jq(ui.hoverUpBySelector(u.__,"[hover]")));
		//鼠标划过问号弹出提示
		div(im).find("[g-hoverup-tip]").each(ui.jq(function(el){
			var tip=el.attr("g-hoverup-tip");
			var selector="[g-hover-tip="+tip+"]";
			ui.hoverUp(el.add(selector))(div(im).find(selector));
		}));
		//点击门店付款
		div(im).find("[code=storesPayment]").on("click",function(){
			var storeitem = u.find(u.pipe(u.prop("c"),u.eq("storesPayment")),datfn(im).list);
			gstore.make(el.find("#mendianfukuan_store"),{
				provice:storeitem.a.provinceCode
				,city:storeitem.a.cityCode
				,area:storeitem.a.countyCode
				,store:storeitem.ps&&storeitem.ps.code
				,current:3
				,selectfn:function(store){
					store.code="storesPayment";
					emit("savePaymentStore",store);
				}
			});
			el.find("#mendianfukuan_tip").show();
			return false;
		});
		//隐藏门店付款框
		div(im).find("[g-close]").on("click",u.partial(ui.hide,[el.find("#mendianfukuan_tip")]));
		//选中操作
		el.find("[g-path]").on("click",ui.jqpipe(
			ui.attr("g-path"),
			u.split(","),
			u.path(u.__,data.list),
			function(sitem){
				emit("savePayment",sitem);
				return u.map(function(item){
					if(sitem==item)return u.assoc("selected",true,item);
					return u.assoc("selected",false,item);
				},datfn(im).list);
			},
			transData,
			set(im),
			init));
		//修改尾款手机号码
		div(im).find("[presell-modify-phone]").on("click",function(){
			set(im,u.assoc("presellModifyStatus","modify",datfn(im)));
			emit("renderCommitOrder",null);
			init(im);
		});
		//取消尾款手机号码
		div(im).find("[presell-canel-phone]").on("click",function(){
			set(im,u.assoc("presellModifyStatus","done",datfn(im)));
			init(im);
			emit("renderCommitOrder",null);
		});
		//尾款手机号码验证
		div(im).find("[presell-text-phone]").on("keyup",function(){
			var val=$(this).val();
			function errcbk(a){
				div(im).find("[presell-error-phone]").html(a);
			}
			if(validate.validateItem("presell-phone",val,errcbk)){
				div(im).find("[presell-btn-phone]")
				.removeClass("btn-disabled")
				.addClass("btn-primary");
			}else{
				div(im).find("[presell-btn-phone]")
				.removeClass("btn-primary")
				.addClass("btn-disabled");
			}
		});
		//保存尾款手机号
		div(im).find("[presell-btn-phone]").on("click",function(){
			var phone=div(im).find("[presell-text-phone]").val();
			emit("saveTailPhone",{
				phone:phone,
				shopNo:$config.shoppingAtom.deliveryPreSell.shopNo,
				shippingGroupId:$config.shoppingAtom.deliveryPreSell.shippingGroupId
			});
		});
	}
	//判断预售手机号是否是不在修改状态
	function isTailPhoneStatusDone(im){
		return datfn(im).presellModifyStatus=="done";
	}
	//是否使用在线支付
	function isUseOnline(im){
		var data=datfn(im);
		//onlinePayment
		return u.find(function(item){
			return item.c=="onlinePayment"&&item.selected;
		},data.list);
	}
	function make(div,data){
		function r(flg,data1){
			if(data1)data=data1;
			return {
				div:div,
				data:data
			}[flg];
		}

		init(r);
		return r;
	}
	exports.payment={
		make:u.curry(make),
		isTailPhoneStatusDone:isTailPhoneStatusDone,
		transData:transData,
		isUseOnline:isUseOnline,
		TYPE:TYPE
	};
}(this,request,util,util_ui,util_watch,GTPL,gstore,validate);