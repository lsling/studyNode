<div class="clearfix cart-deleted-item" act-deleted-item data-id="${pid}-${sid}">
	<div class="fl deleted-col-1">
		<a href="${url}" target="_blank">
			${title}
		</a> 
	</div>
	<div class="fr">
		<a 
			href="javascript:;"
			act-add-wishlist 
			data-pid="${pid}"
			data-sid="${sid}" 
			data-cid="${cid}"
			>
				移入收藏夹
			</a> 
	</div>
	<div class="fr">
		<a href="javascript:;" 
			act-rebuy
			data-pid="${pid}"
			data-sid="${sid}"
			data-count="${count}"
			data-hwg="${hwg}"
			>
			重新购买
		</a> 
	</div>
	<div class="fr"> 
		${count}
	</div>
	<div class="fr"> 
		¥ ${price}
	</div> 
</div> 