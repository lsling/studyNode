<!-- 延保购物车 -->
<div class="container cart-ext">
	<div class="cart-main-header clearfix"> 
		<div class="cart-col-1"> 
			可购延保服务的商品
		</div>
		<div class="cart-col-2">
			选择延保服务
		</div>
		<div class="cart-col-3">
			数量
		</div> 
	</div>
	<div class="cart-shop-header">
		<div class="cart-col-1">
			您可以对订单${orderId}的以下商品选购延保服务
		</div> 
		<div class="cart-col-2">
			${data.warrantyCartSubtotalVO.itemCount}件商品  
			&nbsp;&nbsp;
			小计：<span class="cart-shop-amount ffyh">¥${data.warrantyCartSubtotalVO.totalAmount.toFixed(2) || '0.00'}</span>
		</div>
	</div>

	<div class="cart-shop-goods"> 
		{{each(iwcsv, wcsv) data.commerceItemVOs}}  
			<div class="cart-shop-good cart-shop-good-common clearfix"> 
				<div class="cart-col-1">
				</div>
				<div class="cart-col-2">
					<a href="${wcsv.itemURL}" target="_blank">
						{{if wcsv.itemImageURL}}
							<img src="${wcsv.itemImageURL}" alt="">
						{{/if}}
					</a>
				</div>
				<div class="cart-col-3">
					<div class="cart-good-name">
						<a href="${wcsv.itemURL}" target="_blank">
							${wcsv.itemName}
						</a> 
					</div>  
				</div>
				<div class="cart-col-4">  
					{{each(iwi, warranty) wcsv.warrantyItems}}
						<a 
							class="cart-ext-item
							{{if warranty.isSelected}}
								cart-ext-item-selected
							{{/if}}
							ffyh
							"   
							href="javascript:;"  
							data-infos="${wcsv.commerceItemId}|${warranty.warrantySkuId}|${warranty.warrantyProId}|${warranty.commerceItemId}|${wcsv.productId}"
							act-add-warranty 
							>
							<i class="c-i  cart-ext-icon
							{{if warranty.warrantyType == "0" }}
								 extend_icon	
							{{else}}
								cu  
							{{/if}}">
							</i><!--主站和延保独立站点warrantyType字段值不一样-->
							${warranty.warrantyType == "1" ? "特惠" : ""}延长保修${warranty.fixedYears}年
										${warranty.price}元
							{{if warranty.isSelected}}
								${data._hasSelected=true,''}
								<i class="c-i ext-selected"></i>
							{{/if}}
						</a> 
					{{/each}}
				</div> 
				<div class="cart-col-5 warrantyqutityfixed"> 
					${wcsv.quantity}
				</div> 
			</div> 
		{{/each}}
	</div>  
</div> 
<div class="cart-bottom-wrap">
	<div class="cart-bottom-info"> 
		<div class="container">
			<div class="cart-col-1"> 
				&nbsp;  
			</div>
			<div class="cart-col-2"> 
				&nbsp;
			</div>
			<div class="cart-col-3"> 
				&nbsp; 
			</div>
			<div class="cart-col-4 cart-totle-price"> 
				总共（不含运费）：
				<span class="cart-price-red cart-price-big ffyh">￥${ data.warrantyCartSubtotalVO.totalAmount.toFixed(2) || '0.00' }</span>
			</div> 
			<div class="cart-col-5"> 
				<a href="javascript:" 
					act-init-order    
					class="order-btn
					{{if !data._hasSelected}}
						init-disabled
					{{/if}}
					"
				>	
					<span class="btn-normal" style="font-family:microsoft YaHei; font-size:16px;font-weight:bold;">去结算<em class="c-i arrowRight-white" style="margin-left:6px;"></em></span>
					<div class="btn-loading">结算中<i></i></div> 
					
				</a> 
			</div> 
		</div>
	</div>		
</div> 
<!-- /延保购物车 -->