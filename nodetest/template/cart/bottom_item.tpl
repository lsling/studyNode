<div class="cart-scroll-item fl">
	{{if isOnlyShow}}
		<div class="cart-scroll-image">
			<img src="${image}" alt=""> 
		</div>
	{{else}}
		<a 
		class="cart-scroll-image"
		{{if maima_param}}
			maima_param="{{html maima_param}}"
		{{/if}}
		 href="${url}" target="_blank">
			<img src="${image}" alt="">
			<div class="cart-scroll-tip">
				加入成功
			</div>
		</a>
	{{/if}}
	<div class="cart-scroll-title">
		{{if isOnlyShow}}
			${title}
		{{else}} 
			<a 
			class="black" 
			{{if maima_param}}
				maima_param="{{html maima_param}}"
			{{/if}}
			href="${url}" target="_blank">${title}</a>
		{{/if}}
	</div> 
	<div class="cart-scroll-price ffyh">
		￥${price}
	</div> 
	<a 
		class="cart-scroll-btn
			{{if isOnlyShow}}
				cart-scroll-btn-disabled
			{{/if}}  
		" href="javascript:"
		act-add-cart
		{{if maima_param}}
			maima_param="{{html maima_param}}"
		{{/if}}
		data-sid="${sid}"
		data-pid="${pid}"
		data-gid="${giftId}"
		{{if isHWG}}
			data-hwg="${isHWG}"
		{{/if}}
	>加入购物车</a>
</div> 

