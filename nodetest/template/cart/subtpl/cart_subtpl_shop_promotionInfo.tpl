{{if promotionInfo && promotionInfo.length}} 
	{{if promotionInfo.length == 1}}
		<!-- 无下拉框 --> 
		<!-- 促销开始标签 -->
		<div {{if promotionInfo[0].satisfied}}class="cart-shop-goods-promotion"{{/if}}>
			<div class="cart-icon-notice">
				{{html $config.renderCartLabel(promotionInfo[0].type,"促销")}} 
			</div>
			{{html promotionInfo[0].label}}
		   
		{{if !promotionInfo._selectedPromotion}}
			${promotionInfo._selectedPromotion=promotionInfo[0],''}
		{{/if}} 
		<!-- /无下拉框 --> 
	{{else}} 
		{{each(npi, onePromotion) promotionInfo}}
			{{if onePromotion.selected}}
				${promotionInfo._selectedPromotion=onePromotion,''}
			{{/if}}
		{{/each}}
		{{if !promotionInfo._selectedPromotion}}
			${promotionInfo._selectedPromotion=promotionInfo[0],''}
		{{/if}}
		
		{{if promotionInfo._selectedPromotion}}
			<!-- 促销开始标签 -->
			<div 
				{{if promotionInfo._selectedPromotion.satisfied}}class="cart-shop-goods-promotion"{{/if}}
			>
				<div class="cart-icon-notice"> 
					{{html $config.renderCartLabel(promotionInfo[0].type,"促销")}}
				</div>
				<div gui-widget="select"class="gui-select gui-select-with-arrow">
					<select name="" id="" act-change-promotion>
						{{each(npi, onePromotion) promotionInfo}} 
							<option value="${shop.shopId}|${onePromotion.promotionId}"
								{{if onePromotion.selected}}
									selected
								{{/if}}
								>
								${onePromotion.label}
							</option> 
						{{/each}} 
					</select>
				</div>  
			 
		{{/if}}
	{{/if}} 

	{{if 
		promotionInfo._selectedPromotion.zengPinCommerceItemVOs &&
		promotionInfo._selectedPromotion.zengPinCommerceItemVOs.length 
	}}
		&nbsp;&nbsp;
		<div class="cart-zengpin cart-prom" gui-popup> 
			<a class="cart-zengpin-trigger" href="javascript:">
				查看赠品
			</a>
			<div class="cart-zengpin-box" gui-popupbox> 
				<div class="cart-box-arrow">
					<i>◆</i><i class="cart-arrow-cover">◆</i>
				</div>
				<div class="cart-box-border"> 
					<div class="border-none">
						<div class="cart-zengpin-wrap">
							{{each(zpi, zengpin) promotionInfo._selectedPromotion.zengPinCommerceItemVOs}}
								{{if zpi}}
									、
								{{/if}}
								${zengpin.itemName} x ${zengpin.quantity}
							{{/each}} 
						</div> 
					</div> 
				</div>
			</div>
		</div> 
	{{/if}}

	{{if promotionInfo._selectedPromotion.url}}
		&nbsp; 
		<a style="position:absolute;left:423px;" href="${promotionInfo._selectedPromotion.url}?${promotionInfo._selectedPromotion.promotionId}" 
			target="_blank">
			去凑单
		</a>
	{{/if}}
</div> 
<!-- 
	促销结束标签，
	注：两个开始标签但是结束标签只有这一个，
	即：不同条件下使用不同的开始标签，但是结束标签就这一个 
-->
{{/if}} 