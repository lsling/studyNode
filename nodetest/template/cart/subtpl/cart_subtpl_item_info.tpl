{{if shop.shopId == 'GOME' && oneCommerceItemsGroup.promotionHeads && oneCommerceItemsGroup.promotionHeads.length}}
	<div class="cart-item-line"></div>
{{/if}} 
<div class="cart-col-1">  
	{{if good.commerceitemVOFlag == "SUB"}}
	 	<!-- 子商品没有勾选 -->
	 	<div class="c-i dot  {{if !good.selected }} dotgrey {{/if}}"></div>
	{{else hasCheckbox}}
		<div 
			act-check-item
			gui-checkbox
			gui-class-unchecked="checkboxs" 
			gui-class-checked="checkbox_chose" 
			gui-class-disabled="no_check"
			data-cid="${good.itemId}"
			class="c-i checkboxs"
			{{if 
						typeof(shop.shopInfoSummary.hwgTaxAmount) == 'number' &&
						data.cartProfile.currentHWGShopId && 
						data.cartProfile.currentHWGShopId != shop.shopId
					}}
				act-alert="海外购不同店铺的商品请分开结算"
			{{/if}}
			>
				<input 
					type="checkbox"
					name="good"
					{{if good.selected }}checked{{/if}}
					data-title="${good.itemName}"
					data-url="${good.itemURL}"
					data-sid="${good.skuId}"
					data-pid="${good.productId}"  
					{{if 
						oneCommerceItemsGroup.promotionHeads && 
						oneCommerceItemsGroup.promotionHeads.length && 
						(
							oneCommerceItemsGroup.promotionHeads[0].type=="GOME_JIA_JIA_HUAN_GOU" || 
							oneCommerceItemsGroup.promotionHeads[0].type=="GOME_DA_PEI_GOU"
						)
					}}
						 data-price="${parseFloat(good.listPrice * good.quantity).toFixed(2) || 0}"
					{{else}}
						 data-price="${parseFloat(good.amount).toFixed(2) || 0}"
					{{/if}} 
					data-count="${good.quantity}"
					{{if good.cType=='HWG'}}
						data-hwg="true"
					{{/if}}  
					value="${good.itemId}"
					{{if !hasDeleteArea || good.commerceitemVOFlag == "SUB"}}
						data-save="false"
					{{else}}
						data-save="true"
					{{/if}}
					{{if
						(typeof(shop.shopInfoSummary.hwgTaxAmount) == 'number' && 
						data.cartProfile.currentHWGShopId && 
						data.cartProfile.currentHWGShopId != shop.shopId)
						|| 
						good.reserveStatus == 'YY' || good.inventoryState == "OFF"   
					}}
						disabled
					{{/if}} 
				> 	  
		</div>
	{{else}}
		&nbsp;
	{{/if}}
</div>
<div class="cart-col-2">
	<a href="${good.itemURL}" target="_blank">
		{{if good.itemImageURL}}
			<img src="${good.itemImageURL}" alt="">
		{{/if}}
	</a>
</div>

 <div class="fl">
	<div class="clearfix">
		
			
		<div style="" class="cart-col-3">
			<div  class="cart-good-name">
				{{if good.cType=='HWG'}}
					<i class="c-i hwg" title="海外购商品"></i>
				{{/if}}
				<a href="${good.itemURL}" target="_blank">
					${ $config.limitStr(good.itemName, 52) }
				</a> 
			</div> 
			<div class="cart-good-info clearfix"> 
				{{if good.warrantyItems && good.warrantyItems.length}}
					<div 
					{{if siteName!='secondHand' && good.inventoryState == "OFF"}}
					{{else}}
						gui-widget="select"
					{{/if}}
					class="gui-select gui-select-with-arrow cart-guarantee">
						<select name="" id="" act-change-warranty>
							${good.warrantySelected=false,''}
							${good.warrantySelectedItem=false,''}
							{{each(i, warranty) good.warrantyItems}}
							<option value="add|${good.itemId}|${warranty.warrantySkuId}|${warranty.warrantyProductId}" 
								{{if warranty.selected}}
									${good.warrantySelected=warranty.mainItemId,''}
									${good.warrantySelectedItem=warranty,''}
									selected
								{{/if}}
								{{if warranty.warrantyType == "SPECIAL"}}
									data-icon-class="c-i cu"
								{{else}}
									data-icon-class="c-i extend"
								{{/if}}
								>
								${warranty.warrantyType == "SPECIAL" ? "特惠" : ""}延长保修${warranty.numOfYear}年
									${warranty.price}元
							</option>
							{{/each}}
							<option value="del|${good.warrantySelected}"  
								data-icon-class="c-i noextend"
								{{if !good.warrantySelected}}
									selected
								{{/if}}   
								>
								暂不购买延保
							</option>
						</select> 
					</div>   
				{{/if}}
				{{if good.freeShippingItem}}
					<span class="icons c-i freight" title="免运费">
					</span> 
				{{/if}} 
			</div> 
			{{if good.commerceItemVOs}}
				{{each(i, gift) good.commerceItemVOs}}
				<div class="cart-good-info clearfix">
					[赠品]${gift.itemName} X${gift.quantity}
				</div>
				{{/each}}
			{{/if}}
			{{if good.cType=='HWG'}}
				<div class="cart-good-info3 clearfix">
					税费：¥${ (+good.taxAmount || 0).toFixed(2) || '0.00'}
				</div>
			{{/if}}
		</div>

		<!--sales property vos -->	
		{{if good.salesPropertyVOs &&  good.salesPropertyVOs.length }}
				<span class = "cart-col-salesPro">
					{{each(i, salesPro) cartApp.salesPropertyfn(good.salesPropertyVOs)}}
						
							<div class="cart-good-pro"> 
								<div class="cart-good-key" title="">${salesPro.labelKey}：</div>	
								<div class="cart-good-value" title="${salesPro.labelVal}" >${salesPro.labelVal}</div>
							</div> 
							</br>
					
					{{/each}}
				</span>
		{{else}}
			<span class = "cart-col-salesPro"></span>

		{{/if}}	
		<!--sales property vos -->
		<div class="cart-col-4">
			<div class="cart-good-real-price">
				{{if good.listPrice &&　good.listPrice == -1}}
					暂无售价 
				{{else}}
				 	${parseFloat(good.salePrice).toFixed(2) || 0}
				{{/if}}
			</div>
			{{if good.memberPrice && good.memberPrice.memberPrice}} 
				<div class="red">
					会员价
				</div> 
			{{/if}} 
			{{if good.promotions && good.promotions.length}}
				{{if shop.shopId == 'GOME'}} 
					<div class="cart-coupon cart-prom" 
						act-good-prom 
						{{if siteName!='secondHand' && good.inventoryState == "OFF"}}
						{{else}}
							gui-popup 
						{{/if}}
						gui-popup-animate="1" 
						gui-popup-toggle="1"
						> 
						<div class="cart-coupon-trigger">
							促销优惠 
							<i class="c-i cart-arrow arrowdown_red"></i>
						</div>
						<div class="cart-coupon-box" gui-popupbox>  
							<div class="cart-box-arrow">
								<i>◆</i><i class="cart-arrow-cover">◆</i>
							</div>
							<div class="cart-box-border" gui-popupbox-animate> 
								<div class="cart-coupon-items border-none">
									<form>
										{{each(i, promotion) good.promotions}}
										<dl class="clearfix">
											<dt>
												<input type="radio"  name="cart-coupon${iog}"
												{{if promotion.selected}}
													checked
													data-default="true"
												{{/if}}
												value="${promotion.promotionId}"
												>
											</dt>
											<dd>
												${promotion.label}
											</dd>
										</dl>
										{{/each}}
									</form>
									<div class="cart-coupon-btns cart-box-group-a">
										<a href="javascript:;" class="cart-btn cart-red-bg" 
											gui-popupclose 
											act-claimGomePromotion
											data-cpid="${good.itemId}"
											>确认</a>
										<a href="javascript:;" class="cart-btn" gui-popupclose>取消</a>
									</div>
								</div> 
							</div> 
						</div>
					</div> 
				{{else}}
					<div class="red">   
						多买优惠
					</div>
				{{/if}}
				
			{{/if}}
		</div>
		<div class="cart-col-5">
			{{if 
				hasChangeCount && 
				good.commerceitemVOFlag != "SUB" && 
				good.reserveStatus != 'YY' &&
				good.reserveStatus != 'QG'  
			}}
				<div
					gui-count
					{{if siteName!='secondHand' && good.inventoryState == "OFF"}} 
						gui-count-disabled="true" 
					{{/if}}		

					gui-count-min="1"
					gui-count-animate="1"
					gui-count-change-interval="1000"
					act-change-count
					data-cid="${good.itemId}"
					data-limit="${good.limitNum}"
					class="gui-count cart-count" 
					data-limit-${good.skuId.toLowerCase()}-${good.productId.toLowerCase()}="${good.limitNum}"
					> 
					<input 
						dytest class="
						{{if siteName!='secondHand' && good.inventoryState == "OFF"}} 
						gui-count-disabled
						{{/if}}		
						"
					type="text" value="${good.quantity}">  
				</div> 
				{{if good.itemId==$config.cartAtom.limitTip}} 
				<div class="limit-tip">商品数量不能大于${good.limitNum}件</div>
				{{/if}}
			{{else}}
				<div>
					${good.quantity}
				</div>
			{{/if}}  
			{{html cartApp.itemState(good)}}
			{{if good.inventoryState != "NO_GOODS"}}
				{{if siteName!='secondHand' && good.limitBuyGoods}}
					<div class="red">   
						限购${good.limitNum}件
					</div>
				{{/if}} 
			{{/if}} 

			{{if good.onTheRoad}}
				<div class="green">
					在途
				</div> 
			{{/if}} 

			{{if good.reserveStatus == 'YY'}}
				<div class="red">
					预约中
				</div>
			{{else good.reserveStatus == 'QG'}}
			 	<div class="red">
					预约抢购中
				</div>
			{{/if}}

	 
		</div>
		<div class="cart-col-6 ">
			<div class="cart-good-amount">
					${$config.formatAmount(good.amount)}
			</div> 
		</div>
	 </div> 
	{{if good.warrantySelectedItem}}
		<div class="clearfix cart-warranty-item">
			<div class="cart-col-3" style="margin:0px 132px 0 0px; color:#a5a5a5; font-family:Nsimsun;"> 
				[延保]
				${good.warrantySelectedItem.warrantyType == "SPECIAL" ? "特惠" : ""}延长保修${good.warrantySelectedItem.numOfYear}年
			</div>
			<div class="cart-col-4"> 
				<div class="warrantypricefixed">${$config.limitStr($config.formatAmount(good.warrantySelectedItem.price)+'',12)} 
				</div>
			</div>
			<div class="cart-col-5"> 
				${good.warrantySelectedItem.quantity}  
			</div>
			<div class="cart-col-6">
				<div class="warrantyfixed"> ${$config.formatAmount(good.warrantySelectedItem.amount)}</div>
				
			</div>
		</div>
	{{/if}}
 </div> 


<div class="cart-col-7">
	<div class="cart-good-fun delfixed">
		<a href="javascript:;" 
			act-remove-item 
			data-title="${good.itemName}"
			data-url="${good.itemURL}"
			data-sid="${good.skuId}"
			data-pid="${good.productId}"
			{{if 
				oneCommerceItemsGroup.promotionHeads && 
				oneCommerceItemsGroup.promotionHeads.length && 
				(
					oneCommerceItemsGroup.promotionHeads[0].type=="GOME_JIA_JIA_HUAN_GOU" || 
					oneCommerceItemsGroup.promotionHeads[0].type=="GOME_DA_PEI_GOU"
				)
			}}
				data-price="${parseFloat(good.listPrice * good.quantity).toFixed(2) || 0}"
			{{else}}
				 data-price="${parseFloat(good.amount).toFixed(2) || 0}"
			{{/if}} 
			data-count="${good.quantity}"
			data-cid="${good.itemId}" 
			{{if !hasDeleteArea || good.commerceitemVOFlag == "SUB"}}
				data-save="false"
			{{else}}
				data-save="true"
			{{/if}}
			{{if good.cType=='HWG'}}
				data-hwg="true"
			{{/if}}
			> 
			删除
		</a> 
	</div>
	{{if good.commerceitemVOFlag == "SUB"}}
		<!-- 子商品没有加入收藏夹和到货通知 -->
	{{else}}
		{{if hasAddFavorites}}
			<div class="cart-good-fun">
				<a href="javascript:"
					data-sid="${good.skuId}"
					data-pid="${good.productId}"
					data-cid="${good.itemId}"
					act-add-wishlist>移入收藏夹</a>
			</div>
		{{/if}} 
		{{if good.inventoryState == "NO_GOODS"}}
		<div class="cart-good-fun">
			<a href="javascript:"  
				data-sid="${good.skuId}"
				data-pid="${good.productId}"
				data-cid="${good.itemId}"
				data-price="${parseFloat(good.salePrice).toFixed(2) || 0}"
				data-fav="${hasAddFavorites}"
				act-arrival-notice>到货通知</a>
		</div> 
		{{/if}}  
	{{/if}}
</div>