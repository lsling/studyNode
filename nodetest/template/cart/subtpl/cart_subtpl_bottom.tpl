<div class="cart-bottom-info
		{{if data.cartProfile.containsHwg}}
			cart-bottom-hwg
		{{/if}}
	"> 
	{{if data.cartProfile.containsHwg}}
	<div class="cart-bottom-notice">
		<div class="container">
			海关规定：由海外邮寄的物品，每单限额1000元；由港澳台邮寄的物品，每单限额800元；由保税区邮寄的物品，每单限额2000元。
		</div>
	</div>
	{{/if}}
	<div class="container">
		<div class="cart-col-1">
			{{if hasCheckbox}}
				<div 
					act-check-all-items
					gui-checkbox 
					gui-class-unchecked="checkboxs" 
					gui-class-checked="checkbox_chose" 
					gui-class-disabled="disabled"
					class="c-i checkboxs">
						<input type="checkbox"
						{{if data.allItemsSelected}}
							checked
						{{/if}}
						>
				</div> 
			{{else}}
				&nbsp;
			{{/if}}
			
		</div>
		<div class="cart-col-2">
			{{if hasCheckbox}}
				全选
			{{else}}
				&nbsp;
			{{/if}}
		</div>
		<div class="cart-col-3"> 
			{{if hasCheckbox}}
				<a href="javascript:" act-batch-remove-item class="black del-goods" data-save="${hasDeleteArea}">
					删除选中商品
				</a> 
			{{else}}
				&nbsp;
			{{/if}}
			
		</div>
		<div class="cart-col-4 cart-totle-price">
			已选 
			<span class="cart-price-red" style="font-size:14px;font-weight:900;">
				${ data.cartProfile.itemCount }
			</span>
			件商品
			<span class="white-space"></span>
			总共（不含运费）：
			<span class="cart-price-red cart-price-big ffyh" style="font-size:18px;font-weight:900;">￥${ data.cartProfile.totalAmount.toFixed(2) || '0.00' }</span>
				<br> 
			{{if data.cartProfile.discAmount || (data.rCouponProfileVOList && data.rCouponProfileVOList.length)}}
				<!--已节省 优惠券可能要改逻辑<br> -->
				{{if data.cartProfile.reCouponCount  && !data.cartProfile.discAmount}}
						<span class="{{if data.cartProfile.containsHwg}} fl{{/if}}" style="margin-left:50px;">
							返券：优惠券 X ${data.cartProfile.reCouponCount}
						</span>
				{{/if}}	

				{{if data.cartProfile.discAmount && !data.cartProfile.reCouponCount }}
						<span class="{{if data.cartProfile.containsHwg}}fl{{/if}}" style="margin-left:50px;">
							已节省： <span class="ffyh">-￥</span>${parseFloat(data.cartProfile.discAmount).toFixed(2) || '0.00'}
						</span> 		
				{{/if}}

				{{if data.cartProfile.discAmount && data.cartProfile.reCouponCount }} 
						<span style="display:block;margin-left:-61px" class="{{if data.cartProfile.containsHwg}}fl{{/if}}">
							已节省： <span class="ffyh">-￥</span>${parseFloat(data.cartProfile.discAmount).toFixed(2) || '0.00'}
						</span> 
						<span class="{{if data.cartProfile.containsHwg}} fl ml11{{/if}}">
							返券：优惠券 X ${data.cartProfile.reCouponCount}
						</span>					
				{{/if}}

				

			{{/if}}
			{{if data.cartProfile.containsHwg}}
				{{if data.cartProfile.freeTax}}
					<span class="
					{{if data.cartProfile.containsHwg}} {{/if}}
					" >
					 税费（不含运费税费）：<i class="del">¥${data.cartProfile.hwgTaxAmount.toFixed(2) || '0.00'} </i> </span>
				{{else}}
					<span class="
					{{if data.cartProfile.containsHwg}}ml5{{/if}}
		
					">税费（不含运费税费）： ¥${data.cartProfile.hwgTaxAmount.toFixed(2) || '0.00'} </span>
				{{/if}}
			{{/if}}
		</div> 
		<div class="cart-col-5">
			{{if data.cartProfile.containsHwg}}
				<a href="javascript:" class="flex-btn fl order-btn
					{{if !data.cartProfile.hwgitemChecked}}
						init-disabled
					{{/if}}
					"
					 act-init-order
					 data-hwg="true"
				>
					<span class="btn-normal" style="font-family:microsoft YaHei; font-size:16px;font-weight:bold;">海外购商品结算<em class="c-i arrowRight-white" style="margin-left:6px;"></em></span>
					<div class="btn-loading">结算中<i></i></div>
				</a>
				<div class="flex-split fl"></div>
				<a href="javascript:" class="flex-btn fl order-btn
					{{if !data.cartProfile.normalItemChecked}}
						init-disabled
					{{/if}}
					"
					 act-init-order
				>
					<span class="btn-normal" style="font-family:microsoft YaHei; font-size:16px;font-weight:bold;">普通商品结算<em class="c-i arrowRight-white" style="margin-left:6px;"></em></span>
					<div class="btn-loading">结算中<i></i></div> 
				</a>
			{{else}}
				<a href="javascript:" 
					act-init-order  
					{{if data.cartProfile.hwgitemChecked && !data.cartProfile.normalItemChecked}}
						data-hwg="true"
					{{/if}}
				{{if !data.cartProfile.hwgitemChecked && !data.cartProfile.normalItemChecked}}
					class="init-disabled order-btn"
				{{else}}
					class="order-btn"
				{{/if}}
				>	
					<span class="btn-normal" style="font-family:microsoft YaHei; font-size:16px;font-weight:bold;">去结算<em class="c-i arrowRight-white" style="margin-left:6px;"></em></span>
					<div class="btn-loading">结算中<i></i></div> 
					
				</a>
			{{/if}} 
		</div> 
	</div>
</div>