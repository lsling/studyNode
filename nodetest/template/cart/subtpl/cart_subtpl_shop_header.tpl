<div class="cart-shop-header">
	<div class="cart-col-1">
		{{if hasCheckbox}}
			{{if shop.shopId == 'VNPOP'}}
			&nbsp;
			{{else}}
				<div 
					act-check-shop-item
					gui-checkbox
					gui-class-unchecked="checkboxs" 
					gui-class-checked="checkbox_chose" 
					gui-class-disabled="disabled"
					data-sid="${shop.shopId}"
					class="c-i checkboxs"
					{{if 
								typeof(shop.shopInfoSummary.hwgTaxAmount) == 'number' &&
								data.cartProfile.currentHWGShopId && 
								data.cartProfile.currentHWGShopId != shop.shopId
							}}
						act-alert="海外购不同店铺的商品请分开结算"
					{{/if}}
					>
					<input type="checkbox"
						{{if shop.allItemsSelected}} 
							checked
						{{/if}}  
						{{if 
							typeof(shop.shopInfoSummary.hwgTaxAmount) == 'number' &&
							data.cartProfile.currentHWGShopId && 
							data.cartProfile.currentHWGShopId != shop.shopId
						}}
							disabled
						{{/if}} 
					>
				</div>
			{{/if}} 
		{{else}}
			&nbsp;
		{{/if}} 
		
	</div>
	<div class="cart-col-2">
		{{if shop.shopId == 'GOME'}}  
			<span style="background:#e3101e; color:#fff; padding:5px;">国美自营</span>
		{{else shop.shopId == 'VNPOP'}}
			<span class="black cart-shop-name">联合促销活动</span>
		{{else}}
			<a href="${ shop.shopURL }" title="${ shop.shopName }" class="black cart-shop-name 
				{{if shop.shopCoupons && shop.shopCoupons.length}}	
						cart-shop-name-fixed
				{{/if}}	
			" target="_blank"> 
				${ shop.shopName }
			</a> 
		{{/if}} 

		{{if shop.shopCoupons && shop.shopCoupons.length}}
		<div class="cart-coupon" gui-popup gui-popup-animate="1" gui-popup-toggle="1">
			<a href="javascript:;" class="c-i  cart-coupon-trigger-icon " act-get-coupon data-sid="${shop.shopId}">  <!-- c-i cart-coupon-trigger-icon-->
				<i style="margin-left: 9px; margin-right: 3px;" class="c-i add"></i>
				优惠券
				<i class="c-i cart-arrow arrowdown_red"></i>
			</a> 
			<div class="cart-coupon-box" gui-popupbox>
				<div class="c-i closebtn-new" gui-popupclose></div>
				<div class="cart-box-arrow">
					<i>◆</i><i class="cart-arrow-cover">◆</i>
				</div>
				<div class="cart-box-border" gui-popupbox-animate>
					<div class="cart-coupon-notice">
						<i class="c-i attention" style="float:left;margin:4px 6px 0;"></i>
						领取成功的优惠券可在订单信息页使用
					</div>
					<div class="cart-coupon-items">
						{{each(i, coupon) shop.shopCoupons}} 
						<div class="cart-box-item clearfix">
							<div class="cart-coupon-icon">
								<div class="cart-coupon-icon-inner ">
									 ¥ ${coupon.faceValue}
								</div> 
							</div>
							<div class="cart-coupon-text">
								<div class="cart-coupon-title text-overflow" title="${coupon.description}">
									${coupon.description}
								</div>
								<div class="cart-coupon-dsp">
									${$config.fromatDate(coupon.startDate,"yyyy.MM.dd")}
									- 
									${$config.fromatDate(coupon.expirationDate,"yyyy.MM.dd")}
								</div>
							</div> 
							<a class="cart-coupon-btn"
								act-add-coupon
								data-cpid="${coupon.couponNo}"
								data-aid="${coupon.id}"
								data-quantity="${coupon.quantity}"
								>
								领取
							</a>
						</div> 
						{{/each}}
					</div> 
				</div>
			</div>
		</div> 
		{{else}}
			&nbsp; 
		{{/if}}  
		{{if shop.shopId != 'GOME' && shop.shopId!= 'VNPOP'}}
			<span class="cart-kf" data-customer_service_id="${shop.shopId}" data-shopname="${ shop.shopName }"  style="color:#ccc; position:relative;top:5px;{{if shop.shopCoupons && shop.shopCoupons.length}} top:1px !important{{/if}}">
				<i style="cursor: pointer;" class="c-i c-kf-off"></i>
				<span  class="contact-customer-word">联系客服</span>
			</span>
		{{/if}}
		<!-- <span data-customer_service_id="3">自营客服</span>
		<span data-customer_service_id="80010178">带ID的客服</span> -->
	</div>
	<div class="cart-col-3">
		&nbsp;
	</div>
	<div class="cart-col-4">
		 &nbsp;
	</div>
	<div class="cart-col-5"><!--已和产品确认小计模块全部隐藏 共4块 赠品模块也一样-->
		<!-- {{if parseFloat(shop.shopInfoSummary.totalAmount) > 0}}
			小计：<span class="ffyh cart-shop-amount">￥${ parseFloat(shop.shopInfoSummary.totalAmount).toFixed(2) || '0.00' }</span>
			{{if shop.shopInfoSummary.promoDiscAmount}} 
			<span class="red">
				优惠${shop.shopInfoSummary.promoDiscAmount.toFixed(2) || '0.00'}元  
			</span> 
			{{/if}}
		{{/if}}
		{{if shop.shopInfoSummary.rCouponCount}}
			<span>
				返优惠券${shop.shopInfoSummary.rCouponCount}张
			</span> 
		{{/if}}
		{{if typeof(shop.shopInfoSummary.hwgTaxAmount) == 'number'}}
			&nbsp;&nbsp;&nbsp;&nbsp;
			{{if shop.shopInfoSummary.freeTax}}
				税费：<i class="del">&nbsp;¥${shop.shopInfoSummary.hwgTaxAmount.toFixed(2) || '0.00'}&nbsp;</i> 
			{{else}}
				税费：&nbsp;¥${shop.shopInfoSummary.hwgTaxAmount.toFixed(2) || '0.00'} 
			{{/if}}
			<span class="gray">（税费总额小于等于50免征）</span>
		{{/if}} -->
	</div> 
</div>