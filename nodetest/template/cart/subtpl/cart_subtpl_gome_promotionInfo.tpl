<!-- 自营促销 -->
<div class="cart-shop-info
	{{if promotionInfo[0].satisfied}}
	cart-shop-goods-promotion
	{{/if}}
	">
	{{if ig}}
		<div class="cart-group-line"></div>
	{{/if}}
	<div class="cart-icon-notice">
		{{html $config.renderCartLabel(promotionInfo[0].type,"促销")}} 
		<b></b>
		<i></i>
	</div>
	<!--促销提示语-->
	{{html $config.renderCartPromtionNotice(promotionInfo[0])}}
	{{if promotionInfo[0].jjhgCiVOs && promotionInfo[0].jjhgCiVOs.length}}
		&nbsp; 
		<div class="cart-add-buy" 
			gui-popup gui-popup-event="click" 
			gui-popup-animate="1"
			gui-popup-toggle="1" 
			> 
			<a href="javascript:;" style="text-decoration:none; position:relative;left:0px;" class="cart-add-buy-text"> 
				${promotionInfo[0]._hasSelected=false,''}

				{{each(jjhgi, jjhgItem) promotionInfo[0].jjhgCiVOs}}  
					{{if jjhgItem.selected}} 
						${promotionInfo[0]._hasSelected=true,''}
					{{/if}}
				{{/each}}
				
				{{if promotionInfo[0].satisfied}}
					{{if promotionInfo[0]._hasSelected}}
						<span class = "exch" >重新换购</span>
					{{else}}
						<span class = "exch" >换购商品</span>
					{{/if}} 
				{{else}}
					<span class = "exch" style="">查看换购商品</span>
				{{/if}} 
			</a>  
			<div class="cart-box-arrow" gui-popupbox>
				<i>◆</i><i class="cart-arrow-cover">◆</i>
			</div>
		</div> 
		<div class="cart-add-buy-list" gui-popup-sibling> 
			<div class="cart-add-buy-list-inner">
				<div class="cart-add-buy-header">
					可换购最多 
					<span class="cart-red-text">
					${promotionInfo[0].maxNum}
					</span>
					件，已选 
					<span class="cart-red-text">1</span>
					件

					<div class="c-i closebtn-new" gui-popupclose></div>
				</div>
				<div class="cart-add-buy-row clearfix 
					{{if promotionInfo[0].jjhgCiVOs.length > 6}}
					cart-add-buy-row-scroll
					{{/if}}
					"
					data-max="${promotionInfo[0].maxNum}"
					> 
					{{each(jjhgi, jjhgItem) promotionInfo[0].jjhgCiVOs}}
						{{if jjhgi % 3 == 0}}
							<div class="cart-add-buy-line"></div>
						{{/if}}
						<div class="cart-add-buy-item fl clearfix"> 
							<div class="cart-add-buy-item-left">
								{{if promotionInfo[0].satisfied && jjhgItem.available}}
								<div 
									gui-checkbox 
									act-jjg-check
									gui-class-unchecked="checkboxs" 
									gui-class-checked="checkbox_chose" 
									gui-class-disabled="disabled"
									class="c-i checkboxs">
										<input 
											type="checkbox" 
											value="${jjhgItem.skuId}_${jjhgItem.productId}" 
											{{if jjhgItem.selected}}
												checked
											{{/if}}
											{{if !promotionInfo[0].satisfied}}
												disabled
											{{/if}}
										>
								</div> 
								<div class="cart-add-buy-tip">
									<div class="c-i attention"></div>
									最多可选择${promotionInfo[0].maxNum}件
								</div>
								{{else}}
									&nbsp;
								{{/if}}
							</div> 
							<div class="cart-add-buy-item-right">
								<div class="cart-add-image fl"> 
									<a href="${jjhgItem.itemURL}" target="_blank">
										{{if jjhgItem.itemImageURL}}
											<img src="${jjhgItem.itemImageURL}" alt="">
										{{/if}}
									</a> 
								</div>
								<div class="cart-add-detail">
									<a href="${jjhgItem.itemURL}" target="_blank">${jjhgItem.itemName}</a>
									<div class="cart-add-buy-price ffyh">
										￥${parseFloat(jjhgItem.salePrice).toFixed(2) || '0.00'}
									</div> 
								</div> 
							</div>
						</div>
					{{/each}} 
				</div>

				<div class="cart-add-buy-footer cart-box-group-a">
					<a 
						href="javascript:;" 
						class="cart-btn cart-red-bg"
						gui-popupclose
						act-add-jjg
						data-id="${promotionInfo[0].promotionId}"
						>
						确认
					</a>
					<a href="javascript:;" class="cart-btn" gui-popupclose>取消</a>
				</div> 
			</div> 
		</div> 
	{{/if}}
	{{if promotionInfo[0].url}}
		&nbsp; 
		<a href="${promotionInfo[0].url}?${promotionInfo[0].promotionId}" target="_blank">
			去凑单&nbsp;<span class="jt">&gt;</span>
		</a>
	{{/if}}
	
	{{if oneCommerceItemsGroup.pomotionSummary && oneCommerceItemsGroup.pomotionSummary.promtion}}
		<div class="cart-pomotion-summary">
			{{if oneCommerceItemsGroup.pomotionSummary.subAmount != 0}}
				<span class="ffyh">
					￥${parseFloat(oneCommerceItemsGroup.pomotionSummary.subAmount).toFixed(2) || '0.00'}
				</span> 
			{{/if}}
			{{if oneCommerceItemsGroup.pomotionSummary.promoDiscAmount>0}}
				<span class="red"> 
					减${parseFloat(oneCommerceItemsGroup.pomotionSummary.promoDiscAmount).toFixed(2) || '0.00'}元  
				</span> 
			{{/if}} 
		</div> 
	{{/if}}

</div>
<!-- /自营促销 --> 