<div class="cart-main-header clearfix">
	<div class="cart-col-1"> 
		{{if hasCheckbox}}
			<div 
				act-check-all-items
				gui-checkbox 
				gui-class-unchecked="checkboxs" 
				gui-class-checked="checkbox_chose" 
				gui-class-disabled="disabled"
				class="c-i checkboxs">
					<input type="checkbox"
					{{if data.allItemsSelected}}
						checked
					{{/if}}
					>
			</div>
		{{else}}
			&nbsp;
		{{/if}}
		
	</div>
	<div class="cart-col-2">
		{{if hasCheckbox}}
			全选
		{{else}}
			&nbsp;
		{{/if}} 
	</div><!-- $page.site 主站 团购 抢购   style --> 
	<div style="width:330px;" class="cart-col-3">
		商品信息
	</div>
	<div class="cart-col-4">
		单价（元）
	</div>
	<div class="cart-col-5">
		数量
	</div>
	<div class="cart-col-6">
		金额（元）
	</div>
	<div class="cart-col-7">
		操作
	</div>
</div>