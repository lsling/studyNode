<div class="container">
	{{html GTPL.cart_subtpl_header($data)}}
</div>
<div class="container" 
	act-list
	{{if prevHeight}}
		style="height:${prevHeight}px;"
	{{/if}}
	>
	<div> 
		{{each(is, shop) data.siVOs}}  
			{{html GTPL.cart_subtpl_shop_header({
				shop:shop,
				data:data,
				hasCheckbox:hasCheckbox
			})}}
			<div class="cart-shop-goods {{if shop.shopId != 'GOME'}}cart-shop-goods-normal{{/if}}">
				{{if shop.commerceItemsGroups && shop.commerceItemsGroups.length}}
					<!-- 促销分组列表 -->
					{{each(ig, oneCommerceItemsGroup) shop.commerceItemsGroups}}   
						
						<!-- 单个促销分组 --> 
						{{if oneCommerceItemsGroup.promotionHeads && oneCommerceItemsGroup.promotionHeads.length}}
							{{if shop.shopId == 'GOME'}} 
								{{each(ih, promotionInfo) oneCommerceItemsGroup.promotionHeads}}
									{{if promotionInfo.length}}
										{{html GTPL.cart_subtpl_gome_promotionInfo({
											promotionInfo:promotionInfo,
											oneCommerceItemsGroup:oneCommerceItemsGroup
										})}}
									{{/if}}
								{{/each}}
							{{else}}
								<div class="cart-shop-info">
									{{if ig}}
										<div class="cart-group-line"></div>
									{{/if}}
								{{each(ih, promotionInfo) oneCommerceItemsGroup.promotionHeads}}
									<!-- 联营促销 -->
									{{html GTPL.cart_subtpl_shop_promotionInfo({
											promotionInfo:promotionInfo,
											shop:shop
										})}}
									<!-- /联营促销 -->
								{{/each}}
								</div> 
							{{/if}} 
						{{/if}}
						<!-- 本组商品列表 -->
						{{if oneCommerceItemsGroup.commerceItemsGroup && oneCommerceItemsGroup.commerceItemsGroup.length}} 
							{{each(iog, good) oneCommerceItemsGroup.commerceItemsGroup}}
								<div 
								class="cart-shop-good 
									{{if siteName!='secondHand' && good.inventoryState == "OFF"}} 
										cartgoodoff
									{{/if}}	
									clearfix
									{{if (!oneCommerceItemsGroup.promotionHeads || !oneCommerceItemsGroup.promotionHeads.length) 
										&& ig==0 && iog==0 }} cart-shop-good-common{{/if}}
									{{if good.selected}} cart-shop-good-checked{{/if}}  
									{{if iog==commerceItemsGroup.length-1}}cart-item-last{{/if}}
									" 
								data-good="true" 
								data-sid="${good.skuId}"
								data-pid="${good.productId}" 
								bigdata-pid="${good.productId}"
								>  
								{{html GTPL.cart_subtpl_item_info({
									shop:shop,
									oneCommerceItemsGroup:oneCommerceItemsGroup,
									good:good,
									hasCheckbox:hasCheckbox,
									data:data,
									hasDeleteArea:hasDeleteArea,
									hasChangeCount:hasChangeCount,
									siteName:siteName,
									hasAddFavorites:hasAddFavorites
								})}}
								</div>
							{{/each}}
						{{/if}}
						<!-- /本组商品列表 -->

						<!-- /单个促销分组 -->
						
					{{/each}}
				{{/if}} 
			</div>
		{{/each}}
	</div>
</div>
<div class="cart-bottom-wrap">
	{{html GTPL.cart_subtpl_bottom($data)}}
</div>  