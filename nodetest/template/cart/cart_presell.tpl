<div class="container">
	{{html GTPL.cart_subtpl_header($data)}}
</div>
<div class="container" 
	act-list
	{{if prevHeight}}
		style="height:${prevHeight}px;"
	{{/if}}
	>
	<div> 
		{{each(is, shop) data.siVOs}}  
			{{html GTPL.cart_subtpl_shop_header({
				shop:shop,
				data:data,
				hasCheckbox:hasCheckbox
			})}}
			<div class="cart-shop-goods {{if shop.shopId != 'GOME'}}cart-shop-goods-normal{{/if}}">
				{{if shop.commerceItemsGroups && shop.commerceItemsGroups.length}}
					{{each(ig, oneCommerceItemsGroup) shop.commerceItemsGroups}}
						<!-- 本组商品列表 -->
						{{if oneCommerceItemsGroup.commerceItemsGroup && oneCommerceItemsGroup.commerceItemsGroup.length}} 
								{{each(iog, good) oneCommerceItemsGroup.commerceItemsGroup}} 
								<div 
									class="cart-shop-good clearfix cart-item-last"
									data-good="true" 
									data-sid="${good.skuId}"
									data-pid="${good.productId}" 

									bigdata-pid="${good.productId}"
									>  
									<div class="cart-col-1">
										&nbsp;
									</div>
									<div class="cart-col-2">
										<a href="${good.itemURL}" target="_blank">
											{{if good.itemImageURL}}
												<img src="${good.itemImageURL}" alt="">
											{{/if}}
										</a>
									</div>

									<div class="fl">
										<div class="clearfix">
											<div class="cart-presale-wrap">
												<div class="cart-presale-top clearfix">
													<div style="width:330px;" class="cart-col-3">
														<div class="cart-good-name" style="height:15px;">
															{{if good.cType=='HWG'}}
																<i class="c-i hwg" title="海外购商品"></i>
															{{/if}}
															<a href="${good.itemURL}" target="_blank">
																${ $config.limitStr(good.itemName, 52) }
															</a> 
														</div> 		
														<div class="cart-good-info clearfix"> 
															{{if good.warrantyItems && good.warrantyItems.length}}
																<div gui-widget="select"class="gui-select gui-select-with-arrow cart-guarantee">
																	<select name="" id="" act-change-warranty>
																		${good.warrantySelected=false,''}
																		${good.warrantySelectedItem=false,''}
																		{{each(i, warranty) good.warrantyItems}}
																		<option value="add|${good.itemId}|${warranty.warrantySkuId}|${warranty.warrantyProductId}" 
																			{{if warranty.selected}}
																				${good.warrantySelected=warranty.mainItemId,''}
																				${good.warrantySelectedItem=warranty,''}
																				selected
																			{{/if}}
																			{{if warranty.warrantyType == "SPECIAL"}}
																				data-icon-class="c-i cu"
																			{{else}}
																				data-icon-class="c-i extend"
																			{{/if}}
																			>
																			<!-- ${warranty.displayName}--> 
																			${warranty.warrantyType == "SPECIAL" ? "特惠" : ""}延长保修${warranty.numOfYear}年
																			<span class="ffyh">
																					${warranty.price}元
																				<!--￥${warranty.price.toFixed(2) || '0.00'}-->
																			</span> 
																		</option>
																		{{/each}}
																		<option value="del|${good.warrantySelected}"  
																			data-icon-class="c-i noextend"
																			{{if !good.warrantySelected}}
																				selected
																			{{/if}}   
																			>
																			暂不购买延保
																		</option>
																	</select> 
																</div>   
															{{/if}}
															{{if good.freeShippingItem}}
																<span class="icons c-i freight" title="免运费">
																</span> 
															{{/if}} 
														</div> 
														{{if good.commerceItemVOs}}
															{{each(i, gift) good.commerceItemVOs}}
															<div class="cart-good-info clearfix">
																[赠品]${gift.itemName} X${gift.quantity}
															</div>
															{{/each}}
														{{/if}}
														{{if good.cType=='HWG'}}
															<div class="cart-good-info3 clearfix">
																<!-- 适用税率：${ (+good.taxRate || 0)}%
																&nbsp;&nbsp; -->
																税费：¥${ (+good.taxAmount || 0).toFixed(2) || '0.00'}
																<!-- <div class="c-i help-tip" gui-popup gui-popup-event="mouseover">
																	<div class="help-tip-content" gui-popupbox>
																		<i class="help-tip-arrow"></i>
																		<i class="help-tip-arrow2"></i>
																		关税=商品单价*件数*商品税率<br>注：不同品类商品的税率不同
																	</div>
																</div> -->
															</div>
														{{/if}}
													</div>
													<div class="cart-col-4">
														<div class="cart-good-real-price">
														 	${parseFloat(good.salePrice).toFixed(2) || 0}
														</div>
														{{if good.memberPrice && good.memberPrice.memberPrice}} 
															<div class="red">
																会员价
															</div> 
														{{/if}} 
														{{if good.promotions && good.promotions.length}}
															{{if shop.shopId == 'GOME'}} 
																<div class="cart-coupon cart-prom" 
																	act-good-prom gui-popup 
																	gui-popup-animate="1" 
																	gui-popup-toggle="1"
																	> 
																	<div class="cart-coupon-trigger">
																		促销优惠 
																		<i class="c-i cart-arrow arrowdown_red"></i>
																	</div>
																	<div class="cart-coupon-box" gui-popupbox>  
																		<div class="cart-box-arrow">
																			<i>◆</i><i class="cart-arrow-cover">◆</i>
																		</div>
																		<div class="cart-box-border" gui-popupbox-animate> 
																			<div class="cart-coupon-items border-none">
																				<form>
																					{{each(i, promotion) good.promotions}}
																					<dl class="clearfix">
																						<dt>
																							<input type="radio"  name="cart-coupon${iog}"
																							{{if promotion.selected}}
																								checked
																								data-default="true"
																							{{/if}}
																							value="${promotion.promotionId}"
																							>
																						</dt>
																						<dd>
																							${promotion.label}
																						</dd>
																					</dl>
																					{{/each}}
																				</form>
																				<div class="cart-coupon-btns cart-box-group-a">
																					<a href="javascript:;" class="cart-btn cart-red-bg" 
																						gui-popupclose 
																						act-claimGomePromotion
																						data-cpid="${good.itemId}"
																						>确认</a>
																					<a href="javascript:;" class="cart-btn" gui-popupclose>取消</a>
																				</div>
																			</div> 
																		</div> 
																	</div>
																</div> 
															{{else}}
																<div class="red">   
																	多买优惠
																</div>
															{{/if}}
															
														{{/if}}
													</div>
												</div>
												{{if good.presell}}
													<div class="cart-presale-bottom">
														{{if 
															good.presell.presellStepList && 
															good.presell.presellStepList.length
														}}
															<div class="cart-presale-box clearfix"> 
																<div class="cart-presale-prices">
																	{{each(ipsl, presellStep) cartApp.presellItemfn(good.presell.presellStepList,good.presell)}}
																		<div class="cart-presale-price">
																			{{if presellStep.jindu || presellStep.jindu == 0}}
																				<div class="line" 
																				style="width:{{html presellStep.jindu}}%;background-color:{{html presellStep.linebackgroundcolor}};"></div>
																			{{else}}
																			<div class="line" style="width:100%;background-color:{{html presellStep.linebackgroundcolor}};"></div>
																			{{/if}}
																			
																			<div 
																			style="
																			background-color:{{html presellStep.backgroundcolor}};
																			color:{{html presellStep.fontcolor}};" 
																			class="cart-presale-inner-price">
																				{{html presellStep.showMsg}}
																				<br>
																				<span style="
																					font-size:{{html presellStep.fontsize}};
																					font-weight:{{html presellStep.fontweight}};
																				">
																					¥${presellStep.salePrice}
																				</span> 
																			</div>
																		</div>
																	{{/each}}
																</div> 
															</div> 
														{{/if}}

														<div class="cart-presale-text">
															当前已有
															<span class="red">
																${good.presell.currentApplyCount}
															</span>  
															人预订，您是第
															<span class="num">
																${good.presell.currentApplyRank}
															</span> 
															位。
															{{if good.presell.nextApplyCount>0}}只需
															<span class="num"> 
																${good.presell.nextApplyCount}
															</span> 
															人预订，即可达到下一阶段。
															{{/if}}
														</div>
													</div>
												{{/if}}
											</div>
											<div class="cart-col-5">
												{{if 
													hasChangeCount && 
													good.commerceitemVOFlag != "SUB" && 
													good.reserveStatus != 'YY' &&
													good.reserveStatus != 'QG'  
												}}
													<div
														gui-count 
														gui-count-min="1"
														gui-count-animate="1"
														gui-count-change-interval="1000"
														act-change-count
														data-cid="${good.itemId}"
														data-limit="${good.presell.limitBuy}" 
														class="gui-count cart-count" 
														data-limit-${good.skuId.toLowerCase()}-${good.productId.toLowerCase()}="${good.presell.limitBuy}"
														> 
														<input 

														type="text" value="${good.quantity}">  
													</div> 
													{{if good.itemId==$config.cartAtom.limitTip}} 
														<div class="limit-tip">商品数量不能大于${good.presell.limitBuy}件</div>
													{{/if}} 
												{{else}}
													<div>
														${good.quantity}
													</div>
												{{/if}}  
												{{html cartApp.itemState(good)}}
												{{if good.inventoryState != "NO_GOODS"}}
													{{if siteName!='secondHand' && good.limitBuyGoods}}
														<div class="red">   
															限购${good.presell.limitBuy}件
														</div>
													{{/if}} 
												{{/if}} 

												{{if good.onTheRoad}}
													<div class="green">
														在途
													</div> 
												{{/if}} 

												{{if good.reserveStatus == 'YY'}}
													<div class="red">
														预约中
													</div>
												{{else good.reserveStatus == 'QG'}}
												 	<div class="red">
														预约抢购中
													</div>
												{{/if}}

										 
											</div>
											<div class="cart-col-6">
												<div class="cart-good-amount">
													{{if good.amount}}
														${parseFloat(good.amount).toFixed(2)}
													{{else}}
														0.00
													{{/if}}
												</div> 
											</div>
										</div>
										{{if good.warrantySelectedItem}}
											<div class="clearfix cart-warranty-item">
												<div class="cart-col-3" style="margin:10px 95px 0 -92px; color:#a5a5a5; font-family:Nsimsun;">
													[延保]
													<!-- ${good.warrantySelectedItem.displayName} -->
													${good.warrantySelectedItem.warrantyType == "SPECIAL" ? "特惠" : ""}延长保修${good.warrantySelectedItem.numOfYear}年
												</div>
												<div class="cart-col-4"> 
													${good.warrantySelectedItem.price.toFixed(2) || '0.00'} 
												</div>
												<div class="cart-col-5"> 
													${good.warrantySelectedItem.quantity}  
												</div>
												<div class="cart-col-6">
													${good.warrantySelectedItem.amount.toFixed(2) || '0.00'}
												</div>
											</div>
										{{/if}}
									</div>


									<div class="cart-col-7">
										<div class="cart-good-fun">
											<a href="javascript:;" 
												act-remove-item 
												data-title="${good.itemName}"
												data-url="${good.itemURL}"
												data-sid="${good.skuId}"
												data-pid="${good.productId}"
												{{if 
													oneCommerceItemsGroup.promotionHeads && 
													oneCommerceItemsGroup.promotionHeads.length && 
													(
														oneCommerceItemsGroup.promotionHeads[0].type=="GOME_JIA_JIA_HUAN_GOU" || 
														oneCommerceItemsGroup.promotionHeads[0].type=="GOME_DA_PEI_GOU"
													)
												}}
													data-price="${parseFloat(good.listPrice * good.quantity).toFixed(2) || 0}"
												{{else}}
													 data-price="${parseFloat(good.amount).toFixed(2) || 0}"
												{{/if}} 
												data-count="${good.quantity}"
												data-cid="${good.itemId}" 
												{{if !hasDeleteArea || good.commerceitemVOFlag == "SUB"}}
													data-save="false"
												{{else}}
													data-save="true"
												{{/if}}
												{{if good.cType=='HWG'}}
													data-hwg="true"
												{{/if}}
												> 
												删除
											</a> 
										</div>
										{{if good.commerceitemVOFlag == "SUB"}}
											<!-- 子商品没有加入收藏夹和到货通知 -->
										{{else}}
											{{if hasAddFavorites}}
												<div class="cart-good-fun">
													<a href="javascript:"
														data-sid="${good.skuId}"
														data-pid="${good.productId}"
														data-cid="${good.itemId}"
														act-add-wishlist>移入收藏夹</a>
												</div>
											{{/if}} 
											{{if good.inventoryState == "NO_GOODS"}}
											<div class="cart-good-fun">
												<a href="javascript:"  
													data-sid="${good.skuId}"
													data-pid="${good.productId}"
													data-cid="${good.itemId}"
													data-price="${parseFloat(good.salePrice).toFixed(2) || 0}"
													data-fav="${hasAddFavorites}"
													act-arrival-notice>到货通知</a>
											</div> 
											{{/if}}  
										{{/if}}
									</div>
								</div>
								{{/each}} 
						{{/if}}
					{{/each}}
				{{/if}} 
			</div>
		{{/each}}
	</div>
</div>
<div class="cart-bottom-wrap">
	{{html GTPL.cart_subtpl_bottom($data)}}
</div>  