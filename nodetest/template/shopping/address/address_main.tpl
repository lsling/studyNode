 <div class="clearfix" style="line-height:22px;">
	<h3 class="title-color font14 fl"> 
		收货人信息
	</h3>
	{{if $page.site !="allowance"}}
	<div class="newaddress fr" id="id_newAddress_btn" style="height:22px;line-height:22px;">
		<a class="new_add clearfix i-block" href="javascript:void(0)" style="line-height:22px;">
			<em class="c-i addicon fl"></em><span class="fl i-block" >新增收货人地址</span>
		</a>
	</div>
	{{/if}}
</div>
{{if list&&list.length==1}}
<div class="nInfos pr" id="addname" style="padding-bottom:29px;">
{{else}}
<div class="nInfos pr" id="addname">
{{/if}}
    <div id="addressBox" class="addname-warp">
        {{if $config.shoppingAddressAtom.more=="Y"}}
        <div class="pr over_flow">
        {{else list.length<5}}
        <div class="pr over_flow">
        {{else}}
		<div class="pr h180 over_flow" id="address_scroll_div">
        {{/if}}
            {{if $config.shoppingAddressAtom.more=="Y"}}
            <ul class="addname" g-scroll style="top:0px;">
            {{else list.length<5}}
            <ul class="addname" g-scroll style="top:0px;">
            {{else}}
			<ul class="addname pabs" g-scroll style="top:0px;">
            {{/if}}
				{{each list}}
					{{if $config.shoppingAddressAtom.more=="N"||((typeof selected=="undefined")?false:selected)}}
					{{if $index==list.length-1||$config.shoppingAddressAtom.more=="Y"}}
					<li class="infofirst clearfix pr" g-hover-up style="padding-bottom:1px">
					{{else}}
					<li class="infofirst clearfix pr" g-hover-up>
					{{/if}}
					<div class="hover clearfix">
						{{if selected}}
						<a href="javascript:void 0" class="fl btn btn-check btn-checked mw150 mr10" >
						{{else}}
						<a href="javascript:void 0" class="fl btn btn-check mw150 mr10" g-btn-path="list,${$index}"  g-sbtn-path="list,${$index}">
						{{/if}} 

							<span class="name name_over2 fl">${name}</span><em class="area_over fl w45">${nameArea}</em>
							{{if selected}}
							<i class="c-i chose_icon"></i>
							 {{/if}}
	                      </a>
						<div class="fl clearfix h30 o_f w520">
							<span class="j-cityname mr10">${areas[0]}</span> 
							<span class="h_area mr10">${areas[1]}</span>
							<span class="purview mr10">${areas[2]}</span>
							<span class="deadd mr10 i-block">${areas[3]}</span>
							<span class="tp-cell">
								
								{{if origin.mobileNumber}}
									${origin.mobileNumber}
								{{else}}
									${$config.pwdTelphone(origin.phoneNumber)}
								{{/if}}	
							</span> 
						</div>
						{{if isdefault}}
						<span class="bga5 fontf ml10" style="  padding: 1px 2px 1px 4px;">默认地址</span>
						{{/if}}
						{{if list.length!=1}}
						<span class="changefix pabs hide" g-hover g-delete-path="list,${$index}">删除</span> 
						{{/if}}
						<span class="delobj pabs hide" g-hover g-modify-path="list,${$index}" >修改</span>
	                    {{if $config.isDisabledDefaultAddress(isdefault)}}
	                    {{else}}
	                    <span class="changefix pabs default_add hide" g-hover g-default-path="list,${$index}">设置为默认地址</span>
	                    {{/if}}
					</div>
				</li> 
				{{/if}}
				{{/each}}
			</ul>
		</div> 
    </div>
    {{if list.length>1}}
	    {{if $config.shoppingAddressAtom.more=="Y"}}
	    <a href="javascript:void 0" class="btn link" g-more-path g-value="N">
	    	<span class="fl">更多地址</span>
	    	<i class="c-i fd-icon fl" style="margin-top:5px;"></i>
	    </a>
		{{else}}
		 <a href="javascript:void 0" class="btn link" g-more-path g-value="Y">
		 	<span class="fl">收起地址</span>
	    	<i class="c-i more-icon fl" style="margin-top:5px;"></i>
		 </a>
	    {{/if}}
	
	{{/if}}
</div>
