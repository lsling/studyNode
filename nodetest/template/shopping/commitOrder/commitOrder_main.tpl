<div class="clearfix" style="border-top:solid 1px #e6e6e6;padding-top:13px;">	
	<p class="text-right  clearfix"><span class="w185 fr pr20">¥${$config.formatAmount(amount)}</span><span class="fr"><em class="fontRed">${total}</em>件商品，总金额：</span></p>
	{{if $config.VBLE.youhuiquan2()}}
	<p class="text-right  clearfix"><span class="w185 fr pr20">-¥${$config.formatAmount(coupon)}</span><span class="fr">优惠券：</span></p>
	{{/if}}
	{{if $config.VBLE.jifen()}}
	<p class="text-right  clearfix"><span class="w185 fr pr20">-¥${$config.formatAmount(integral)}</span><span class="fr">积分：</span></p>
	{{/if}}
	{{if ppcAmount!=null}}
	<p class="text-right  clearfix"><span class="w185 fr pr20">-¥${$config.formatAmount(ppcAmount)}</span><span class="fr">美通卡：</span></p>
	{{/if}}
	<p class="text-right  clearfix"><span class="w185 fr pr20">-¥${$config.formatAmount(balance)}</span><span class="fr">余额：</span></p>
	{{if $page.site=="haiwaigou"}}
		 <p class="text-right  clearfix"><span class="w185 fr pr20 }">+¥${$config.formatAmount(hwgAmount)}</span><span class="fr">税费：</span></p>
	{{else $page.site=="allowance"}}
	<p class="text-right  clearfix"><span class="w185 fr pr20">-¥${$config.formatAmount(allowanceAmount)}</span><span class="fr">节能补贴：</span></p>
	{{/if}}
	<p class="text-right  clearfix"><span class="w185 fr pr20" >+¥${$config.formatAmount(haulage)}</span><span class="fr">运费：</span></p>
	<div style="height:28px;"></div>
	<div class="text-right play clearfix">
		<div class="w185 fr" id="id_commit_div">
			{{if $config.isDisabledTJDD()}}
				<a href="javascript:void 0" class="btn btn-disabled btn-large fr"  
				{{if $page.site=="presell"}}
				style="border-bottom-right-radius: 0;height:60px;line-height:60px;" 
				{{else}}
				style="border-bottom-right-radius: 0;"
				{{/if}}
				>提交订单</a>
			{{else}}
			<a href="javascript:void 0" class="btn btn-primary btn-large fr" id="id_commit" 
			{{if $page.site=="presell"}}
			style="border-bottom-right-radius: 0;height:60px;line-height:60px;" 
			{{else}}
			style="border-bottom-right-radius: 0;"
			{{/if}}
			>提交订单</a>
			{{/if}}
		</div>
		{{if $page.site=="presell"}}
		<div class="fr" agreen-play>
			<div
				style="line-height:24px;margin-top:17px;"
			>应付款总金额：<em class="fontRed strong font16">¥${$config.formatAmount(applyAmount)}</em></div>
			<div
				style="line-height:24px;"
			>
			{{if $config.shoppingAtom.presell_tyzfdj}}
			<i class="c-i checkbox_chose fl" style="margin-top:6px;margin-left:42px;"></i>
			{{else}}
			<i class="c-i checkboxs fl" style="margin-top:6px;margin-left:42px;"></i>
			{{/if}}
			同意支付定金</div>
		</div>
		{{else}}
		<span class="fr">
			应付款总金额：<em class="fontRed strong font16">¥${$config.formatAmount(applyAmount)}</em></span>
		{{/if}}
	</div>
{{if $config.VBLE.songhuorenxinxi()}}	
	<div style="border:solid 1px #e6e6e6;border-top:none;padding-top:13px;color:#888;">
	<p class="text-right pr10">
		<span class="mr10">${p}</span>
		<span class="mr10">${c}</span>
		<span class="mr10">${a}</span>
		<span class="mr10">${j}</span>
		<span class="mr10 name_over2" title="${d}" style="width: auto;max-width: 570px;height: 16px;line-height:16px;vertical-align: middle;vertical-align: bottom\0;">${d}</span>
	</p>
	<p class="text-right mb10 pr20 pr10">
		<span class="mr10">${sname}</span>
		<span class="mr10">${phone}</span>
	</p>
	</div>
{{/if}}	
</div>