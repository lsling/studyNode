<h3 class="title-color font14">支付方式</h3>
<div class="content">
	<div class="clearfix ml40">
	{{each(pidx,pay) list}}
		{{if pay.visible}}
        {{if pay.c=="storesPayment"}}<!--门店付款-->
		 <div class="fl pr mr15 zi10" hoverup click-document-pre>
            {{if pay.selected}}
			<a href="javascript:void(0)"  class="btn pr btn-check btn-checked mw120" code="storesPayment" >
				<span class="name max_w160">${pay.tip.label}-${pay.ps.label}</span>
				<b class="c-i tips_icon pabs payway_tips2 hide" hover g-hoverup-tip="${pay.c}"></b>
				<i class="c-i chose_icon"></i>
			</a>
			{{else pay.available==false}}
			<a href="javascript:void(0)"  class="btn pr btn-check btn-disabled mw120" >
				<span class="name max_w160">${pay.tip.label}</span>
				<b class="c-i tips_icon pabs payway_tips2 hide" hover g-hoverup-tip="${pay.c}"></b>
			</a>
			{{else}}
			<a href="javascript:void(0)"  class="btn pr btn-check mw120" code="storesPayment">
				<span class="name max_w160">${pay.tip.label}</span>
				<b class="c-i tips_icon pabs payway_tips2 hide" hover g-hoverup-tip="${pay.c}"></b>
			</a>
			{{/if}}
			<div class="tips_detail pabs hide del2 box-sd1" g-hover-tip="${pay.c}">
				<i class="c-i arrowup pabs tips_arrow" ></i>
				${pay.tip.desc} 
				<a href="${pay.tip.href}" target="_blank" class="ml10">${pay.tip.name}</a>
			</div>
            <div class="pr hide z10" id="mendianfukuan_tip" click-document-hide>
	            <div class="pabs content_area  zi10 " > 
	            	<em class="pabs c-i arrowup"></em>
	            	<a  class="closed c-i closebtn-new pabs" href="javascript:void(0)" g-close></a>
	            	<div id="mendianfukuan_store" class="z10"></div>
	            </div>
            </div>
        </div>
		{{else}}
		<div class="fl pr mr15" hoverup>
			{{if pay.selected}}
			<a href="javascript:void(0)"  class="btn pr btn-check btn-checked mw120" >
				<span class="name">${pay.tip.label}</span>
				<b class="c-i tips_icon pabs payway_tips hide" hover g-hoverup-tip="${pay.c}"></b>
				<i class="c-i chose_icon"></i>
			</a>
			{{else pay.available==false}}
			<a href="javascript:void(0)"  class="btn pr btn-check btn-disabled mw120">
				<span class="name">${pay.tip.label}</span>
				<b class="c-i tips_icon pabs payway_tips hide" hover g-hoverup-tip="${pay.c}"></b>
			</a>
			{{else}}
			<a href="javascript:void(0)" g-path="${pidx}" class="btn pr btn-check mw120">
				<span class="name">${pay.tip.label}</span>
				<b class="c-i tips_icon pabs payway_tips hide" hover g-hoverup-tip="${pay.c}"></b>
			</a>
			{{/if}}
			<div class="tips_detail pabs hide box-sd1" g-hover-tip="${pay.c}">
				<i class="c-i arrowup pabs tips_arrow"></i>
				${pay.tip.desc} 
				<a href="${pay.tip.href}" target="_blank" class="ml10">${pay.tip.name}</a>
			</div>
		</div>
		{{/if}}
		{{/if}}
	{{/each}}
	{{if $page.site=="presell"}}
		{{if presellModifyStatus=="modify"}}
		<div class="fl pr" style="line-height:31px">		
			<div class="input-group">
				<span class="fl">尾款支付信息通知手机号码：</span> 
				<input 
				type="text" 
				value="${$config.shoppingAtom.deliveryPreSell.smsMobileNumber}"
				presell-text-phone
				class="form-control sm r-no-radius" 
				style="width:80px;">
				<a href="javascript:void(0)" 
				class="btn btn-primary sm l-no-radius" presell-btn-phone>确定</a>
				<a href="javascript:void(0)" class="fl ml10" presell-canel-phone>取消</a>
			</div>
			<span class="fontRed pabs" presell-error-phone style="top:24px;left:157px;"></span>
		</div>
		{{else}}
		<div class="fl" style="line-height:31px">		
			<i class="c-i tips fl" style="margin-right:4px;margin-top: 7px;"></i>
			<span>尾款支付信息通知手机号码：${$config.pwdPhone($config.shoppingAtom.deliveryPreSell.smsMobileNumber)}</span> 
			<a href="javascript:void 0" class="ml20" presell-modify-phone>修改</a>
		</div>
		{{/if}}

	{{/if}}
	</div>
</div>