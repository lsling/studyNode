<div class="m_invoice box-sd2">
	<div class="header clearfix">
		<h3>发票信息</h3>
		<i  class="c-i closebtn-new close" g-close></i>
	</div>
	<div class="content {{if selectedInvoce.invoiceType.code=="2"}}zz{{/if}}">
		<div class="col-1">
			{{if invoiceNeedType=="N"}}
				<a href="javascript:void(0)" class="btn btn-check" g-value-path="invoiceNeedType" g-value="Y">开具发票</a>
				<a href="javascript:void(0)" class="btn btn-check btn-checked">不开发票<i class="c-i chose_icon"></i>
				</a>
			{{else invoiceNeedType=="Y"}}
				<a href="javascript:void(0)" class="btn btn-check btn-checked">开具发票<i class="c-i chose_icon"></i></a>
				<a href="javascript:void(0)" class="btn btn-check" g-value-path="invoiceNeedType" g-value="N">不开发票</a>
			{{else invoiceNeedType=="YD"}}
				<a href="javascript:void(0)" class="btn btn-check btn-checked">开具发票<i class="c-i chose_icon"></i></a>
				<a href="javascript:void(0)" class="btn btn-check btn-checked btn-disabled">不开发票</a>
			{{/if}}
		</div>
	{{if invoiceNeedType!="N"}}
		<div class="clearfix col">
			<div class="label"><i class="fontRed">* </i>发票类型：</div>
			<div class="value">
				<div>
				{{each invoices}}
					{{if invoiceType.visible}}
						{{if invoiceType.available==false}}
							<a href="javascript:void(0);" class="btn btn-check btn-disabled">
								${invoiceType.label}
							</a>
						{{else invoiceType.selected}}
							<a href="javascript:void(0);" class="btn btn-check btn-checked">
								${invoiceType.label}
								<i class="c-i chose_icon"></i>
							</a>
						{{else}}
							<a href="javascript:void(0);" class="btn btn-check" g-i-path="invoices,${$index}">
								${invoiceType.label}
							</a>
						{{/if}}
						
						{{if invoiceType.code=="2"&&invoiceType.state=="NO_CERTIFICATION"}}
							<span class="fontGray">
								首次开具增值税发票请
								<a href="${$config.URL.increaseticket}" target="_blank">申请增票认证</a>
								，店铺商品开具请联系商家。
							</span>
						{{/if}}
					{{/if}}
				{{/each}}
				</div>
				<div class="tip">
					{{html $config.renderNotice("fp_tip_"+selectedInvoce.invoiceType.code,selectedInvoce)}}
				</div>
			</div>
		</div>
		{{if selectedInvoce.invoiceType.code=="2"}}
			<div class="clearfix col">
				<div class="label">单位名称：</div>
				<div class="value l30">${selectedInvoce.corpName}</div>
			</div>
			<div class="clearfix col">
				<div class="label">纳税识别号：</div>
				<div class="value l30">${selectedInvoce.taxpayerNo}</div>
			</div>
			<div class="clearfix col">
				<div class="label">注册电话：</div>
				<div class="value l30">${selectedInvoce.registeredPhone}</div>
			</div>
			<div class="clearfix col">
				<div class="label">开户银行：</div>
				<div class="value l30">${selectedInvoce.taxpayerBank}</div>
			</div>
			<div class="clearfix col">
				<div class="label">银行账户：</div>
				<div class="value l30">${selectedInvoce.accountNo}</div>
			</div>
		{{else}}
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>发票抬头：</div>
				<div class="value">
					{{each selectedInvoce.headTypes}}
						{{if visible}}
							{{if selected}}
								<a href="javascript:void(0)" class="btn btn-check btn-checked">${label}<i class="c-i chose_icon"></i></a>
							{{else}}
								<a href="javascript:void(0)" class="btn btn-check" g-h-path="${selectedInvoce.path},headTypes,${$index}">${label}</a>
							{{/if}}
						{{/if}}
					{{/each}}
				</div>
			</div>
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>抬头内容：</div>
				<div class="value">
					{{if selectedHead.code=="0"}}
						<input  type="text" class="form-control"  g-validate="fptt" value="${selectedHead.content}" value-path="${selectedHead.path},content" />
						<span class=" ml15 fontGray">请填写使用人的真实姓名</span>
					{{else}}
						<input  type="text" class="form-control" g-validate="fptt" value="${selectedHead.content}" value-path="${selectedHead.path},content" />
					{{/if}}
					<span class="nError nRed" g-tip-validate="fptt">{{html selectedHead.fptt}}</span>
				</div>
			</div>
			{{if selectedHead.code=="1"}}
				<div class="clearfix col">
					<div class="label">购方税号：</div>
					<div class="value">
						<input  type="text" 
						g-validate="gfsh" 
						class="form-control"  
						value="${selectedInvoce.taxPayerNo}" 
						value-path="${selectedInvoce.path},taxPayerNo"
						placeholder="在上海、南通及嘉兴地区注册为公司的用户需要填写"/>
						<span class="nError nRed" g-tip-validate="gfsh">{{html selectedInvoce.gfsh}}</span>
					</div>
				</div>
			{{/if}}
		{{/if}}
		{{if selectedInvoce.invoiceContentTypes&&selectedInvoce.invoiceContentTypes.length!=0}}
		<div class="clearfix col">
			<div class="label"><i class="fontRed">* </i>发票内容：</div>
			<div class="value">
				{{each selectedInvoce.invoiceContentTypes}}
					{{if selected}}
					<a href="javascript:void(0)" class="btn btn-check btn-checked w54">
						${label}
						<i class="chose_icon c-i"></i>
					</a> 
					{{else}}
						<a href="javascript:void(0)" class="btn btn-check w54" g-c-path="${selectedInvoce.path},invoiceContentTypes,${$index}" >
						${label}
						</a> 
					{{/if}}
				{{/each}}
			</div>
		</div>
		<div class="clearfix col b-col">
			<div class="label b-label b-tip">&nbsp;</div>
			<div class="value b-tip">
				{{if pType=="G"}}
				<p>温馨提示：建议发票内容选择明细，以保证您可以享受厂商或国美在线提供的质保服务。</p>
				{{else pType=="GO"}}
				<p>温馨提示：建议发票内容选择明细，以保证您可以享受厂商或国美在线提供的质保服务。 非自营商品发票的开具和邮寄等情况，由店铺商家自行决定。如有任何疑问，可联系商家确认。</p>
				{{/if}}
			</div>
		</div>
		{{/if}}
		{{if selectedInvoce.invoiceType.code=="1"}}	
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>收票手机：</div>
				<div class="value">
					<input  
					type="text"
					{{if selectedInvoce.mobilePhone}}
					no-modify="y"
					{{/if}}
					g-validate="spsj" 
					class="form-control phone_input" 
					value="${selectedInvoce.mobilePhone}" 
					value-path="${selectedInvoce.path},mobilePhone" 
					/>
					<span class="nError nRed" g-tip-validate="spsj">{{html selectedInvoce.spsj}}</span>
				</div>
			</div>
			<div class="clearfix col">
				<div class="label">收票邮箱：</div>
				<div class="value">
					<input  type="text" g-validate="spyx" class="form-control mail_input" value="${selectedInvoce.email}" value-path="${selectedInvoce.path},email" 
					/>
					<span class="nError nRed" g-tip-validate="spyx">{{html selectedInvoce.spyx}}</span>
				</div>
			</div>
		{{/if}}
		{{if selectedInvoce.invoiceType.code=="2"}}
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>收件人：</div>
				<div class="value">
					<input type="text" g-validate="zpsjr" class="form-control" value="${selectedInvoce.consigneeName}" value-path="${selectedInvoce.path},consigneeName">
					<span class="nError nRed" g-tip-validate="zpsjr">{{html selectedInvoce.zpsjr}}</span>	
				</div>
			</div>
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>手机号码：</div>
				<div class="value">
					<input 
					type="text" 
					{{if selectedInvoce.consigneeMobilePhone}}
					no-modify="y"
					{{/if}}
					g-validate="zpsjhm" 
					class="form-control" 
					value="${selectedInvoce.consigneeMobilePhone}" 
					value-path="${selectedInvoce.path},consigneeMobilePhone">	
					<span class="nError nRed" g-tip-validate="zpsjhm">{{html selectedInvoce.zpsjhm}}</span>	
				</div>
			</div>
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>邮寄地址：</div>
				<div class="value">
					<input type="text" g-validate="zpyjdz" class="form-control" value="${selectedInvoce.registeredAddress}" value-path="${selectedInvoce.path},registeredAddress"> 
					<span class="nError nRed" g-tip-validate="zpyjdz">{{html selectedInvoce.zpyjdz}}</span>
				</div>
			</div>
		{{/if}}
		{{if $page.site=="gomeVirtualCard" && selectedInvoce.invoiceType.code == '0'}}
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>收货人：</div>
				<div class="value">
					<input type="text" g-validate="zzshr" class="form-control" value="${selectedInvoce.consigneeInfo.name}" value-path="${selectedInvoce.path},consigneeInfo,name">
					<span class="nError nRed" g-tip-validate="zzshr">{{html selectedInvoce.zzshr}}</span>	
				</div>
			</div>
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>所在地：</div>
				<div class="value"  id="id_address_select" style="width:auto;" value-path="${selectedInvoce.path},consigneeInfo,address">
					<span style="border-color:#e5e5e5;" > 
             			<a  href="javascript:void(0)" name="address" class="add_select" 
             			g-validate="address" 
             			value="${selectedInvoce.consigneeInfo.address.provinceCode}${selectedInvoce.consigneeInfo.address.cityCode}${selectedInvoce.consigneeInfo.address.countyCode}${selectedInvoce.consigneeInfo.address.townCode}"
             			>
             				{{if selectedInvoce.consigneeInfo.address.provinceName}}
		                    <span show-label>${selectedInvoce.consigneeInfo.address.provinceName}${selectedInvoce.consigneeInfo.address.cityName}${selectedInvoce.consigneeInfo.address.countyName}${selectedInvoce.consigneeInfo.address.townName}</span>
		                    {{else}}
		                    <span show-label class="fl">请选择</span>
		                    {{/if}}
	                   		<i class="c-i select_arrowup ml10 mt10"></i> 
	                  	</a>
	                </span>
	                <span class="nError nRed" g-tip-validate="address"></span>
	      			<div class="pr add_out hide">
	                	<em class="pabs c-i arrowup add_up2"></em>
	        			<i class="pabs c-i closebtn add_close" g-area-close></i>
	                 	<div class="gCity"></div>
      				</div>
					<!-- <input type="text" g-validate="zzszd" class="form-control" value="" value-path="">
					<span class="nError nRed" g-tip-validate="zzshr">{{html selectedInvoce.zzshr}}</span>	 -->
				</div>
				
			</div>
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>详细地址：</div>
				<div class="value">
					<input type="text" g-validate="zzxxdz" class="form-control" name="detailedAddress" value="${selectedInvoce.consigneeInfo.address.detailedAddress}" value-path="${selectedInvoce.path},consigneeInfo,address,detailedAddress"> 
					<span class="nError nRed" g-tip-validate="zzxxdz">{{html selectedInvoce.zzxxdz}}</span>
				</div>
			</div>
			<div class="clearfix col">
				<div class="label"><i class="fontRed">* </i>手机号码：</div>
				<div class="value col2fl">
					<input 
					type="text"
					{{if selectedInvoce.consigneeInfo.mobileNumber}}
					no-modify="y" 
					{{/if}}
					g-validate="zzsjhm" 
					class="form-control fl" 
					value="${selectedInvoce.consigneeInfo.mobileNumber}" 
					value-path="${selectedInvoce.path},consigneeInfo,mobileNumber" 
					style="width:208px"> 
					<p class="lh30 ml5 fl"><span class="nLowLeight">或</span></p>
				</div>
				<div class="col2fl lh30 ml5">固定电话：</div>
				<div class="value col2fl">
					<input type="text" g-validate="zzgddh" class="form-control fl" value="${selectedInvoce.consigneeInfo.phoneNumber}" value-path="${selectedInvoce.path},consigneeInfo,phoneNumber" style="width:208px"> 
				</div>
				<div class="clearfix"></div>
				<span class="nError nRed col2flMsg" g-tip-validate="zzsjhm">{{html selectedInvoce.zzsjhm}}</span>
				<span class="nError nRed" g-tip-validate="zzgddh">{{html selectedInvoce.zzgddh}}</span>
			</div>
			<div class="clearfix col">
				<div class="label">邮件：</div>
				<div class="value">
					<input type="text" g-validate="zzyj" class="form-control" value="${selectedInvoce.consigneeInfo.email}" value-path="${selectedInvoce.path},consigneeInfo,email">	
					<span class=" ml15 fontGray">方便您实时接收订单状态提醒</span>
					<span class="nError nRed" g-tip-validate="zzyj">{{html selectedInvoce.zzyj}}</span>	
				</div>
			</div>
		{{/if}}
	{{/if}}
		<div class="clearfix col">
			<div class="label">&nbsp;</div>
			<div class="value">
				<a href="javascript:void(0)" id="saveInvoice" class="btn btn-primary btn-large" style="margin-top:20px;width: 120px;">
					保存发票信息
				</a>
			</div>
		</div>
		<div class="clearfix col b-col">
			<div class="label b-label b-tip">&nbsp;</div>
			<div class="value b-tip">
				{{if pType=="O"}}
				<p>温馨提示：非自营商品发票的开具和邮寄等情况，由店铺商家自行决定。如有任何疑问，可联系商家确认。</p>
				{{/if}}
				{{if $page.site!="gomeVirtualCard" && $page.site!="gomeEntityCard"}}
				<a href="${$config.URL.invoiceRule}" target="_blank">了解发票制度详情&nbsp;<span class="jt">&gt;</span></a>
				{{/if}}
			</div>
		</div>
	</div>
</div>