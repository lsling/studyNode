<div class="invoice">
	<h3 class="invoice_h3">发票信息</h3>
	<ul class="invoice_lists ml20">
	{{if $page.site=="haiwaigou"}}
	<li>
		<label class="lb_check fl mr28">
			海外购商品不提供发票
		</label>
	</li>
	{{else invoiceNeedType=="Y"||invoiceNeedType=="YD"}}
		<li>
			{{if open==false}}
			{{if selectedHead.content}}
			<span class="mr15">${selectedHead.content}</span>
			{{/if}}
			<span class="mr15">${selectedInvoce.invoiceType.label}</span>
			<span class="mr15">${selectedContentType.label}</span>
				{{if allowUpdate==true}}
				<a href="javascript:void 0" class="ml20 change" g-modify-path="1">修改</a>    
				{{/if}}
			{{/if}}
		</li>
	{{else invoiceNeedType=="N"}}
		<li>
			<label class="lb_check fl mr28">
				不开发票
			</label>
			{{if allowUpdate==true}}
			<a href="javascript:void 0" class="ml20 change" g-modify-path="0">修改</a>
			{{/if}}
		</li>
	{{/if}}
	</ul>
</div>
