<div class="identity_fill pr bgwhite" g-pipe>
	{{if notAuth(im)}}
	{{else}}
	<em class="c-i closebtn pabs close_window" g-click="dialogHide"></em>
	{{/if}}
	<p >请填写正确的收货人身份信息，并尽量保证与支付人身份信息一致，我们将向海关进行申报<br>如不一致可能导致通关失败。您的信息将被妥善保管和使用。</p>
	<div class="">
		<dl class="clearfix">
			<dt class="fl w80 text-right mr20"><b class="fontRed pdl_r5">*</b>姓名：</dt>
			<dd class="fl"><input class="form-control" value="${data.idCardRealName}" style="width:161px;" g-validate="hwg-name"></dd>
			<dd class="fl ml10 fontRed" g-tip-validate="hwg-name"></dd>
		</dl>
		<dl class="clearfix">
			<dt class="fl w80 text-right mr20"><b class="fontRed pdl_r5">*</b>身份证号：</dt>
			<dd class="fl"><input class="form-control" value="${data.idCardNumber}" style="width:161px;" g-validate="hwg-card"></dd>
			<dd class="fl ml10 fontRed" g-tip-validate="hwg-card"></dd>
		</dl>
	</div>
	<div style="clear:both;"></div>
	<div class="identity_btn clearfix mt20">
		<a class="btn btn-primary btn-w83 fl mr20 ml20" href="javascript:void(0)" g-click="saveAction">保存</a>
		{{if notAuth(im)}}
		{{else}}
		<a class="btn btn-default btn-w83  fl ml10" href="javascript:void(0)" g-click="dialogHide">取消</a>
		{{/if}}
	</div>
</div> 