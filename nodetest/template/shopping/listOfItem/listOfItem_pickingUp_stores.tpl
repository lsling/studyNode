{{each gomeStoreShippings}}
	{{if isSelectStore($value)}}
		 <div class="btn_Square_out"> 
		 	<a href="javascript:void(0)" class="btn btn-check btn-checked" title="${name}">
			 	<span class="name addr_over w135">${name}</span><i class="c-i chose_icon"></i>
		 	</a>
			<span>${cityName}</span>
			<span>${countyName} </span>
			<span class="addr_over" 
			style="width:175px;vertical-align: middle;  line-height: 15px;  height: 16px;"
			title="${address}">${address}</span>
			<span>${storePhone}</span>
		 </div>
	{{else}}
		<div class="btn_Square_out"> 
		 	<a href="javascript:void(0)" class="btn btn-check" store-idx="${$index}">
		 		<span class="name addr_over w135">${name}</span>
		 	</a>
			<span>${cityName}</span>
			<span>${countyName} </span>
			<span title="${address}" class="addr_over" style="width:175px;vertical-align: middle;  line-height: 15px;  height: 16px;">${address}</span>
			<span>${storePhone}</span>
		 </div>
	{{/if}}
{{/each}}