<div class="sideright fl">
	{{each(idx,item) allItems}}
		<div class="sideright_in">
			{{if itemPropCode!="NORMAL"&&itemPropCode!="NO_USE"}}
				<div class="mb5">
					<span class="pair_buy mr10 pr">${$config.labels[itemPropCode]}<b class="c-i triangle_icon pabs horn"></b></span>
					<span class="pair_tip">
					{{html $config.renderNotice(itemPropCode,item)}}
					</span>
				</div>
			{{/if}}
			<ul class="gmlist clearfix">
				{{each items}}
					{{if $index==items.length-1||item.itemPropCode=="NORMAL"||item.itemPropCode=="NO_USE"}}
					<li class="gmlist_li_over">
					{{else}}
					<li class="gmlist_li">
					{{/if}}
						<div class="pr">
							{{if item.itemPropCode!="NORMAL"&&item.itemPropCode!="NO_USE"}}
								<b class="o_dot c-i dot pabs"></b>
								{{if $index==items.length-1}}
								<b class="dot_line pabs"></b>
								{{/if}}
							{{/if}}
						</div>
						<div class="clearfix">
							<a class="imgLink mr10 fl" target="_blank" href="${itemURL}">
								<img src="${itemImageURL}" width="80" height="80">
							</a>
							<div class="commoditycon fl">
								<p >
									{{if $value.itemPropCode !="UNKNOWN"}}
									<em class="fontRed mr5">${$config.labels[$value.itemPropCode]}</em>
									{{/if}}
									<a href="${itemURL}" class="p-name" target="_blank" title="${itemName}">
									{{if $page.site=="haiwaigou"}}
									<b class="c-i hwg"></b>
									{{/if}}
										${$config.limitStr(itemName,31)}
									</a>
								</p>
								<!--节能补贴-->
								{{if $page.site=="allowance"}}
								<p class="p-ins">
									<span class="fontRed mr10">[节能]</span>
									<pan class="fontGray">补贴优惠率${allowanceRatio}%</pan>
								</p>
								{{/if}}
								{{if $page.site!="warranty"}}
									<p class="p-ins">
										<span class="fontRed mr10">¥${$config.formatAmount(salePrice)} </span>
										<span class="fontGray mr20 ml5"> x ${quantity} </span>
										<span class="fontGray mr10">{{html $config.labels[$value.inventoryState]}}</span>
										{{if $value.onTheRoad}}
										<span class="font118850 mr10">在途</span>
										{{/if}}
									</p>
									{{if $page.site=="haiwaigou"}}
										<p style="line-height: 23px;    color: #333333; ">税费（含商品及运费税费）：	<span>¥${$config.formatAmount(taxAmount)}</span>	
										</p>	
									{{/if}}
									<p class="clearfix fuwu">
										{{if servers.myf=="Y"}}
										<span class="c-i freight mr10 fl"></span>
										{{/if}}
										{{if servers.i7=="Y"}}
										<span class="c-i seven mr10 fl"></span>
											<span class="fl lh16" style="color:rgb(68, 156, 87);">支持7天无理由退货</span>
										{{else servers.i7=="N"}}
										<span class="c-i seven_no mr10 fl"> </span>
										<span class=" fl lh16"
										style="color:rgb(68, 156, 87);">不支持7天无理由退货</span>
										{{/if}}
									</p>


								{{/if}}
								{{if racktype!="UNKNOWN"}}
								<div class="applace clearfix" g-pipe>
									<span class="fl">电视挂件安装：</span>
									<div 
									class="fl g-select mr10"
									g-click="toggle [g-body],toggleClass arrowdown [g-icon],toggleClass arrowup2 [g-icon]">
										<a href="javascript:void(0)" class="block" style="width:50px" click-document-pre>
											<i class="c-i select_arrowup"></i><!--兼容IE7浏览器 i必须写前面-->
											{{if racktype=="Y"}}
											安装
											{{else}}
											不安装
											{{/if}}
										</a>
										<ul  g-body class="hide" click-document-hide>
											<li g-click="gjaz ${itemId}_Y allItems.${idx}.items.${$index}.racktype Y">安装</li>
											<li g-click="gjaz ${itemId}_N allItems.${idx}.items.${$index}.racktype N">不安装</li>
										</ul>
									</div>
									<span class="fontGray">安装方式及价格以客服确认为准</span>
									<a  class="fontBlue" target="_blank" href="${$config.URL.homeinstallation}">安装说明></a>
								</div>
								{{/if}}
							</div>
						</div>
						<div>
							{{if warrantyItem}}
							<p class="ns clearfix">
								{{if warrantyItem.warrantyType=="SPECIAL"}}
									<span class="c-i cu mr5 mt4 fl mr10"></span>
								{{else warrantyItem.warrantyType=="NOT_SPECIAL"}}
									<span class="c-i extend_icon mr10 mt4 fl"></span>
								{{/if}}
								<span class="font33 fl mr15">延长保修${warrantyItem.year}年</span>
								<span class="fontRed fl mr20">¥${$config.formatAmount(warrantyItem.price)}</span>
								<span class="fontGray fl">x${warrantyItem.quantity}</span>
								<!-- <span class="c-i calender ml15 fl"></span> -->
							</p>
							{{/if}}
							{{each zps}}
							<p class="p-zp fontGray clearfix">[赠品] ${name} X ${quantity} </p>
							{{/each}}
					</div>
					</li>
				{{/each}}
			</ul>
		</div>

	{{/each}}
</div>