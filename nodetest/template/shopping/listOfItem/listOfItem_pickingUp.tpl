<div class="store_payment bgwhite pr">
	<em class="c-i closebtn-new pabs" g-close style="top:15px;right:20px;cursor: pointer;"></em>
	<div class="select_area clearfix mb10">
		<div class="w150 fl text-right" style="line-height:32px;"><b class="fontRed pdl_r5">*</b> 选择区域：</div>
		<ul class="fl clearfix" style="line-height:32px;">
			<li class="fl mr10">${pickingUP.stateName}</li>
			<li class="fl mr10">
				${pickingUP.cityName}
			</li>
			<!-- <li class="fl ck mr10"> -->
			<li class="fl">
				<div class="g-select" g-select>
					<a href="javascript:void(0)" class="block" style="width:115px;">
						<i class="c-i select_arrowup"></i>
						{{if selectedArea(list)}}
						<san g-title>${selectedArea(list).countyName}</san>
						{{else}}
						<san g-title>请选择</san>
						{{/if}}
					</a>
					<ul class="hide" g-select-body>
						{{each list}}
						<li s-idx="${$index}">${countyName}</li>
						{{/each}}
					</ul>
				</div>

			</li>
		</ul>	
	</div>
	<div style="clear:both;"></div>	
	<div class="select_store clearfix" >
		<div class="w150 fl text-right"><b class="fontRed pdl_r5">*</b>选择自提门店：</div>
		<div class="fl h185" style="width:610px;overflow:auto;background:#f8f8f8;padding:18px 0px 0px 22px;" id="store-content">
			
		</div>	
	</div>
	<div style="clear:both;"></div>
	<div class=" mt20 clearfix">
		<a href="javascript:void(0)" id="pks-saveStore" class="btn btn-primary btn-w83 mr50" style="margin-left:149px;">保存自提门店</a>
		<a href="javascript:void(0)" g-close class="btn btn-default btn-w83">取消</a>
	</div>		
</div>
