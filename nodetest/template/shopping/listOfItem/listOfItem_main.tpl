<div class="clearfix">
	{{if $page.site=="gomeVirtualCard"}}
	<h3 class="title-color font14 fl">商品清单</h3>
	{{else}}
	<h3 class="title-color font14 fl">送货清单</h3>
	{{/if}}
	{{if $config.listOfitemTipVisible()}}
	<p class="fl fontGray tip">大家电配送时部分偏远地区需额外收取远程费 
		<a href="${$config.URL.dispatchQuery}" target="query" >
			查看详情
			<span class="jt">&gt;</span>
		</a>
	</p>
	{{/if}}
	{{if $config.backCardLink()}}
	<a href="javascript:void 0" id="back-cart" class="fr tip cl" style="margin-right:29px;">
		返回购物车修改
	</a>
	{{/if}}
	<span href="javascript:void 0" class="fr tip pr30 cl pr" hoverup>
		<i class="c-i tips fl" style="margin:5px 5px 0px 0px;"></i>
		<span class="fl">价格说明</span>
		<div class="pabs desc hide box-sd1" hover>
			<i class="pabs c-i arrowup"></i>
			因可能存在商品更新等不确定因素，具体售价以本结算页为准。如有疑问，请咨询客服。
		</div>
	</span>
</div>
{{if $page.site=="gomeVirtualCard"}}
<div class="list-box">
	<div class="header clearfix">
		<div class="col-1">商品</div>
		<div class="col-2">单价</div>
		<div class="col-3">数量</div>
		<div class="col-4">金额</div>
	</div>
	{{each $data}}
	<div class="body clearfix">
		<div class="col-1">
			<a href="{{html itemURL}}" target="_blank" class="img">
				<img src="{{html itemImageURL}}">
			</a>
			<div class="info">
				<div class="title">
          <a href="{{html itemURL}}" target="_blank">${itemName}</a>
        </div>
				<div>
					<span class="fontRed" style="margin-right: 20px;">¥${$config.formatAmount(salePrice)}</span>
					<span class="fontGray" style="margin-right: 20px;">x&nbsp;${quantity}</span>
					<!-- <span class="fontGray">有货</span> -->
				</div>
			</div>
		</div>
		<div class="col-2">¥${$config.formatAmount(salePrice)}</div>
		<div class="col-3">x&nbsp;${quantity}</div>
		<div class="col-4">¥${$config.formatAmount(totalPrice)}</div>
	</div>
	{{/each}}
</div>
{{else}}
<div class="infoH">
	{{each(idx,shop) $data}}
	<!--国美自营购物车list-->
	<div class="info-bd clearfix">
		<!--购物车list-top-->
		<div class="backStyle">
			<span class="fr fontRed" style="top:0px;padding-right:29px;">
			{{html $config.shopTip(shop)}}
			</span>
			<span class="hcon">配送信息</span>
			{{if shop.shopId=="GOME"}}
			<span class="gomesend">国美自营</span>
			{{else}}
			<span class="gomesend">${shop.shopName}</span>
			{{/if}}
		</div>
		<!--配送信息,商品清单-->
		<div class="dilivery clearfix">
			<!--配送信息left-->
			<div class="sideleft fl" info-path="${idx}">
			</div>
			<!--配送信息right-->
			<div class="sideright fl" list-path="${idx}">
			</div>
		</div>
	</div>
	{{/each}}
</div>
{{/if}}
