{{each(iidx,info) shoppinginfo}}
	{{if iidx!==0}}
	<div style="height:30px;"></div>
	{{/if}}
	<div class="all_listpay">
		<div class="clearfix tittle mb15">
			<h4 class="fl mr15 send_way">配送方式</h4>
			<div class="goods_dy" map-item>
				{{if visibleCorrespondItems(shopId)}}
				<a href="javascript:void(0)" class="fontBlue clearfix" hoverup-1>
					<i class="fl c-i tips" style="margin-top:5px; margin-right:4px;"></i>
					<span class="fl" >对应商品</span>
				</a>
				{{/if}}
				<!--对应商品弹层开始-->
				<div class="pr zi1000">
					<div class="goods_fenlei pabs hide box-sd1" hover-1>
						<i class="c-i arrowup pabs tips_arrow"></i>
						
						{{if info.items&&info.items.length>4}}
						<div style="height:15px;"></div>
						<div class="fenlei_in" style="height:370px;overflow-y:auto;">
						{{else}}
						<div class="fenlei_in">
						{{/if}}
						{{each info.items}}
						<div class="clearfix">
							<div class="mr10 col-1 fl" >
								<img src="${itemImageURL}" width="80" height="80" alt="">
							</div>
							<div class="fl col-2" >
								<a href="${itemURL}" class="p-name" target="_blank" title="${itemName}">
									<p class="p-name">
									{{if itemPropCode !="UNKNOWN"}}
									<em class="fontRed mr5">${$config.labels[itemPropCode]}</em>
									{{/if}}
									${$config.limitStr(itemName,48)}
									</p>
								</a>
							</div>
						</div>
							{{if $index!==info.items.length-1}}
							<div style="height:15px;"></div>
							{{/if}}
						{{/each}}
						</div>
					</div>
				</div>
				<!--对应商品弹层结束-->
			</div>
		</div>
	</div>
	<!--快递-->
	<div class="Express clearfix">
		{{each info.express}}
			<div class="dispatfont btn_Square_out mb15">
				<div class="h28">
					{{if $value.selected}}
					<a href="javascript:void(0)" class="btn btn-check btn-checked mw150 mr10">
						<span class="name">${$config.labels[$value.code]}</span>
						<i class="c-i chose_icon"></i>
					</a>
						{{if $value.shippingFee}}
						<span class="fontRed">运费:¥${$config.formatAmount($value.shippingFee)}</span>
						{{else $value.shippingFee===0}}
						<span class="fontRed">免运费</span>
						{{/if}}
					{{else}}
					<a href="javascript:void(0)" class="btn btn-check mw150 mr10"
					g-e-path="shoppinginfo,${iidx},express,${$index}">
					<span class="name">${$config.labels[$value.code]}</span>
					</a>
						{{if $value.shippingFee}}
						<span class="fontGray">运费:¥${$config.formatAmount($value.shippingFee)}</span>
						{{else $value.shippingFee===0}}
						<span class="fontGray">免运费</span>
						{{/if}}
					{{/if}}
				</div>
			</div>
		{{/each}}
	</div>
	{{if selectedfn(info.express)&&selectedfn(info.express).code=="Gome Picking Up"}}
	<div class="mb10">
		<span class="strong">自提门店：</span>
		<span class="cash mr20 strong">${selectedfn(info.express).storeName}</span>
		<a href="javascript:void(0)" g-picking-up="shoppinginfo,${iidx},express">修改</a>
		<div style="line-height: 15px">
			温馨提示：商品到店后，国美在线会发短信提醒您，请您<br/>在收到短信后3个自然日内至门店提货。
		</div>
	</div>
	{{/if}}
	<!--支付方式-->
	{{each payments}}
	{{if selected}}
	<div class="pay_way_c mb10">
		<span>支付方式：</span>
		<span class="cash mr20">${$config.labels[$value.code]}</span>
		{{if payments.length>1}}
		<a href="javascript:void(0)" g-p-path="shoppinginfo,${iidx},payments">修改</a>
		{{/if}}
	</div>
	{{/if}}
	{{/each}}
{{if selectedfn(info.express)&&selectedfn(info.express).code!="Gome Picking Up"}}
	<!--配送时间-->
	<div class="otherTime pr clearfix zi10">
		<div class="fl" >配送时间：</div>
	{{each(tidx,ptime) info.times}}
		<div class="chooseTime pr" style="margin-left:60px">
			{{if ptime.code=="DAY"}}
			<div class="chooseDay">
				{{each ptime.items}}
					{{if selected}}
					<label class="lb_check show" >
					<span class="c-i radio_chose mr5 mt4 fl"></span>
					<input type="radio" class="btn_radio">
					${$config.labels[code]}
					</label>
					{{else}}
					<label class="lb_check show" 
						g-t-path="shoppinginfo,${iidx},times,${tidx},items,${$index}">
						<span class="c-i radio mr5 mt4 fl"></span>
						${$config.labels[code]}
					</label>
					{{/if}}
				{{/each}}
			</div>
			{{/if}}
			{{if ptime.code=="JSD"}}
			<div class="chooseDay">
					{{if ptime.selected}}
				<label for="" class="lb_check checktime mb5">
					<span class="c-i radio_chose fl mr5 mt5"></span>
					{{else}}
				<label  g-t-path="shoppinginfo,${iidx},times,${tidx}" class="lb_check checktime mb5">
					<span class="c-i radio fl mr5 mt5"></span>
					{{/if}}
					{{if ptime.days==0}}
					预计将在今天${ptime.hours}前送达
					{{else ptime.days==1}}
					预计将在明天${ptime.hours}前送达
					{{/if}}
				</label>
			</div>
			{{/if}}
			{{if ptime.code=="XSD"}}
			<div class="chooseDay" >
					{{if xsdSelectedTime(ptime)}}
					<label for="" class="lb_check checktime fl wd105 ">
						<span class="c-i radio_chose fl mr5 mt5"></span>
						<input type="radio" class="btn_radio">
					{{else}}
					<label g-t-path="shoppinginfo,${iidx},times,${tidx}" class="lb_check checktime fl wd105 ">
						<span class="c-i radio fl mr5 mt5"></span>
					{{/if}}
					指定送货时间
				</label>
				<div class="fl" hoverup >
					<div class="calendar_out pr fl calendar_z" >
						<input type="text" class="calendar_text c_wd40 c_bd "  
						value="${xsdSelectedTime(ptime)}" 
						readonly="readonly">
						<i class="c-i calender_icon c_icon pabs"></i>
					</div>
					<div class="pabs deli_time box-sd1 hide" hover style="padding:15px;">
						<div class="header clearfix">
							{{each(idx,timeItem) ptime.head}}
								{{if idx==0}}
									<div class="col-1" style="width:124px;">
										时间段
									</div>
								{{else}}
									<div class="col-1" style="width:86px;padding-bottom:5px;">
										<div class="col-1-1">
											${timeItem.md}
										</div>
										<div class="col-1-2">
											${timeItem.label}
										</div>
									</div>
								{{/if}}
							{{/each}}
						</div>
						{{each(bidx,bodyitem) ptime.body}}
							<div class="body clearfix">
							{{each(biidx,timeitem) bodyitem}}
								{{if biidx==0}}
									<div class="col-1 first" style="width:124px;">
										${$config.formathhmm(timeitem.startTime)}-${$config.formathhmm(timeitem.endTime)}
									</div>
								{{else timeitem.available}}
									{{if timeitem.selected}}
									<div class="col-1 hover" style="width:86px;">
										<div class="item">已选</div>
									</div>
									{{else}}
									<div class="col-1"
									style="width:86px;"
									g-t-path="shoppinginfo,${iidx},times,${tidx},body,${bidx},${biidx}">
										<div class="item">可选</div>
									</div>
									{{/if}}
								{{else}}
									<div class="col-1" style="width:86px;">&nbsp;</div>
								{{/if}}
							{{/each}}
							</div>
						{{/each}}
					</div>
				</div>
			</div>
			{{/if}}
		</div>
	{{/each}}
	</div>
{{/if}}
{{/each}}
{{if $config.VBLE.beizhu()}}
<p class="valignp mt40" g-pipe>
	<span class="remarks fl">备注:</span>
	<label class="rmarkLabel fl">
		<input type="text" class="form-control" style="width:250px;" value="${comments}" g-keyup="changeComment [g-keyup] [g-tip]" placeholder="最多输入30个字">
	</label>
	<span class="nWarings fl hide fontRed" g-tip>&nbsp;&nbsp;最多输入30个字 </span>
</p>
{{/if}}
{{if shopPhone}}
<p class="dispatfont">
	<span class="fontGray mr5">如有疑问请联系</span>
	${shopPhone} 
</p>
{{/if}}