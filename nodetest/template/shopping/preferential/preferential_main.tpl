<div class="p-panel" g-pipe>
{{if $config.VBLE.youhuiquan()}}
	{{if $config.shoppingAtom.yhj=="N"}}
	<div class="chooes-hd clearfix mb10 pl38" g-click="setStage yhj Y,render">
		<span class="triger fl mr5" >
			<em class="ii c-i open fl mr5"></em>
	{{else $config.shoppingAtom.yhj=="Y"}}
	<div class="chooes-hd clearfix mb10 pl38" g-click="setStage yhj N,render">
		<span class="triger fl mr5" >
			<em class="ii c-i close fl mr5"></em>
	{{/if}}
			使用优惠券
		</span>
		<span class="fontGray fl">{{html $config.renderNotice("YHQ",$data)}}</i></em></span>
		{{if $config.shoppingAtom.yhj=="Y"}}
		<a class="shipping-coupon-rule fr" target="_blank" href="${$config.URL.discountUseRule}">了解优惠券&nbsp;<span class="jt">&gt;</span></a>
		{{/if}}
	</div>
	{{if $config.shoppingAtom.yhj=="Y"}}
	<div class="chooes-bd">
		<div class="tab-trigger clearfix">
			<span class="tab-trigger-item tab-trigger-item-active text-center fl">我的优惠券
				<em> （</em>${allCouponNum}<em>）</em>
			</span>
		</div>
		<!--使用优惠券-->
		<div class="tab-content">
			<!--使用优惠券 已经激活的优惠券列表-->
			<div class="coupon-own">
				{{if vrbsos.bcs&&vrbsos.bcs.length>0}}
					<!--蓝券-->
					<dl class="tab-content-item">
						<dt class="lineH">蓝券：</dt>
						<dd class="Yhqcon">
							<div class="divtabel">
								<table class="redCard-lists" style="width:100%;">
									<tbody>
										{{each getBCS().list}}
											{{if available==false}}
											<tr class="gray">
											{{else checked}}
											<tr class="checkedbg">
											{{else}}
											<tr>
											{{/if}}
												<td class="w30"></td>
												<td class="w30 text-center">
													{{if available==false}}
													<label class="checkbox_out" >
														<span class="c-i no_check">
													{{else checked}}
													<label class="checkbox_out" g-click="assocPathSelected vrbsos.bcs.${$index} false B,render">
														<span class="c-i checkbox_chose">
													{{else}}
													<label class="checkbox_out" g-click="assocPathSelected vrbsos.bcs.${$index} true B,render">
														<span class="c-i checkboxs">
													{{/if}}
														</span>
													</label>
												</td>
												{{if available==false}}
												<td class="w80">
												{{else}}
												<td class="fontRed w80 strong">
												{{/if}}
													¥${faceValue}
												</td>
												<td title="${description}">
													<span class="i-block text-overflow w512">${description}</span>
												</td>
												<td class="fontGray">有效期至：${$config.formatLong(expirationDate)}</td>
											</tr>
										{{/each}}
									</tbody>
								</table>
							</div>
							<div class="clearfix pdl_r20" more-btn>
								{{if getBCS().bottom}}
									<span class="fl line_up"></span>
									{{if $config.shoppingAtom.lq_more=="Y"}}
									<a href="javascript:void(0)"class="moreyhq fl"
										g-click="setStage lq_more N,render">
										更多<i class="c-i arrowdown ml8"></i>
									</a>
									{{else $config.shoppingAtom.lq_more=="N"}}
									<a href="javascript:void(0)"class="moreyhq fl" 
									g-click="setStage lq_more Y,render">
										收起<i class="c-i arrowup2 ml8"></i>
									</a>
									{{/if}}
									<span class="fl line_up"></span>
								{{/if}}
							</div>
						</dd>
					</dl>
				{{/if}}
				{{if vrbsos.rcs&&vrbsos.rcs.length>0}}
					<!--红券-->
					<dl class="tab-content-item">
						<dt class="lineH">红券：<span class="fontGray"></span></dt>
						{{if verifyStatus=="VERIFY_NOT_ACTIVATED"}}
						<dd class="open_mima">{{html $config.errorText.f1}}</dd>
						{{/if}}
						<dd class="Yhqcon">
							<div class="divtabel">
								<table class="redCard-lists" style="width:100%;">
									<tbody>
										{{each getRCS().list}}
											{{if available==false}}
											<tr class="gray">
											{{else checked}}
											<tr class="checkedbg">
											{{else}}
											<tr>
											{{/if}}
												<td class="w30"></td>
												<td class="w30 text-center">
													{{if available==false}}
													<label class="checkbox_out" >
														<span class="c-i no_check">
													{{else checked}}
													<label class="checkbox_out" g-click="assocPathSelected vrbsos.rcs.${$index} false R,render">
														<span class="c-i checkbox_chose" >
													{{else}}
													<label class="checkbox_out" g-click="assocPathSelected vrbsos.rcs.${$index} true R,render">
														<span class="c-i checkboxs">
													{{/if}}
														</span>
													</label>
												</td>
												{{if available==false}}
												<td class="w80">
												{{else}}
												<td class="fontRed w80 strong">
												{{/if}}
													¥${faceValue}
												</td>
												<td title="${description}">
													<span class="i-block text-overflow w512">${description}</span>
												</td>
												<td class="w200 fontGray mr30">有效期至：${$config.formatLong(expirationDate)}</td>
											</tr>
										{{/each}}
									</tbody>
								</table>
							</div>
							<div class="clearfix pdl_r20" more-btn>
								{{if getRCS().bottom}}
									<span class="fl line_up"></span>
									{{if $config.shoppingAtom.hq_more=="Y"}}
									<a href="javascript:void(0)"class="moreyhq fl"
										g-click="setStage hq_more N,render">
										更多<i class="c-i arrowdown ml8"></i>
									</a>
									{{else $config.shoppingAtom.hq_more=="N"}}
									<a href="javascript:void(0)"class="moreyhq fl" 
									g-click="setStage hq_more Y,render">
										收起<i class="c-i arrowup2 ml8"></i>
									</a>
									{{/if}}
									<span class="fl line_up"></span>
								{{/if}}
							</div>
						</dd>
					</dl>
				{{/if}}
				{{if vrbsos.scs&&vrbsos.scs.length>0}}
					<!--红券-->
					<dl class="tab-content-item">
						<dt class="lineH">店铺券：</dt>
						<dd class="Yhqcon">
							<div class="divtabel">
								<table class="redCard-lists" style="width:100%;">
									<tbody>
										{{each getDCS().list}}
											{{if available==false}}
											<tr class="gray">
											{{else checked}}
											<tr class="checkedbg">
											{{else}}
											<tr>
											{{/if}}
												<td class="w30"></td>
												<td class="w30 text-center">
													{{if available==false}}
													<label class="checkbox_out" >
														<span class="c-i no_check">
													{{else checked}}
													<label class="checkbox_out" g-click="assocPathSelected vrbsos.scs.${$index} false D,render">
														<span class="c-i checkbox_chose">
													{{else}}
													<label class="checkbox_out" g-click="assocPathSelected vrbsos.scs.${$index} true D,render">
														<span class="c-i checkboxs">
													{{/if}}
														</span>
													</label>
												</td>
												{{if available==false}}
												<td class="w80">
												{{else}}
												<td class="fontRed w80 strong">
												{{/if}}
													¥${$config.formatAmount(faceValue)}
												</td>
												<td title="${description}">
													<span class="i-block text-overflow w512">${description}</span>
												</td>
												<td class="w200 fontGray mr30">有效期至：${$config.formatLong(expirationDate)}</td>
											</tr>
										{{/each}}
									</tbody>
								</table>
							</div>
							<div class="clearfix pdl_r20" more-btn>
								{{if getDCS().bottom}}
									<span class="fl line_up"></span>
									{{if $config.shoppingAtom.dpq_more=="Y"}}
									<a href="javascript:void(0)"class="moreyhq fl"
										g-click="setStage dpq_more N,render">
										更多<i class="c-i arrowdown ml8"></i>
									</a>
									{{else $config.shoppingAtom.dpq_more=="N"}}
									<a href="javascript:void(0)"class="moreyhq fl" 
									g-click="setStage dpq_more Y,render">
										收起<i class="c-i arrowup2 ml8"></i>
									</a>
									{{/if}}
									<span class="fl line_up"></span>
								{{/if}}
							</div>
						</dd>
					</dl>
				{{/if}}
			</div>
			<!--激活的优惠券-->
			<div class="coupon-own activate mt10 text-left">
				<dl class="redCard-act" id="actRedcard">
					<dt class="activateYhq ">
						{{if $config.shoppingAtom.yhj_jh=="N"}}
						<span class="trigers fontRed gomeactivate mr5" g-click="setStage yhj_jh Y,render">
							<em class="ii c-i open fl"></em>激活优惠券
						</span>
						{{else $config.shoppingAtom.yhj_jh=="Y"}}
						<span class="trigers fontRed gomeactivate mr5" g-click="setStage yhj_jh N,render">
							<em class="ii c-i close fl"></em>激活优惠券
						</span>
						{{/if}}
						<span class="fontGray ml5">国美在线或者国美在线合作方发放的优惠券</span>
					</dt>
					{{if $config.shoppingAtom.yhj_jh=="Y"}}
					<dd class="mt10 redCard-box" >
						<form class="active-nForm">
							<table class="redReady" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td>请输入兑换码：</td>
										<td>
											<input 
											g-keyup="setValueStage yhj_bm this"
											value="${$config.shoppingAtom.yhj_bm}" 
											class="
											form-control mr5
											{{if $config.shoppingAtom.yhj_tip}}
											error
											{{/if}}
											" type="text" placeholder="编码"  style="width:135px;">-</td>
										<td>
											<input 
											g-keyup="setValueStage yhj_jhm this"
											value="${$config.shoppingAtom.yhj_jhm}"
											class="
											form-control mr5 ml5
											{{if $config.shoppingAtom.yhj_tip}}
											error
											{{/if}}
											" type="text" placeholder="激活码" style="width:135px;">-</td>
										<td>
											<input 
											g-keyup="setValueStage yhj_yzm this"
											value="${$config.shoppingAtom.yhj_yzm}"
											class="
											form-control mr5 ml5
											{{if $config.shoppingAtom.yhj_tip}}
											error
											{{/if}}
											" 
											type="text" placeholder="验证码" style=" width:75px;vertical-align: middle;">
											<span class="nValidate mr5 ml5"><img src="${$config.URL.imgcode}&capCd=${$config.shoppingAtom.yhj_img}"></span>
											<a class=" fontBlue mr20 ml5" href="javascript:void(0)" g-click="setTimeLongStage yhj_img,render">换一张</a>
										</td>
										<td>
											{{if $config.isDisabledJH()}}
											<a href="javascript:void(0)" class="btn btn-disabled" >激活</a>
											{{else}}
											<a href="javascript:void(0)" class="btn btn-primary" g-click="activeYCode,setTimeLongStage yhj_img">激活</a>
											{{/if}}
										</td>							
									</tr>
									<tr>
										<td ></td>
										<td colspan="3">
											{{if $config.shoppingAtom.yhj_tip}}
											<span class="fontRed">&nbsp;&nbsp;${$config.shoppingAtom.yhj_tip}</span>
											{{/if}}
										</td>					
									</tr>
								</tbody>
							</table>
						</form>
					</dd>
					{{/if}}
				</dl>
				<div style="height:5px;border-bottom:1px solid #e6e6e6"></div>
				{{if vrbsos.sp.availablePoint!==0}}
				<p style="font-weight: normal;">可用国美门店积分
					<span>${vrbsos.sp.currentPoint}</span>
					积分，本次最多可使用&nbsp;
					<span class="fontRed">${vrbsos.sp.availablePoint}</span>
					&nbsp;积分<span class="fontGray">&nbsp;(1积分等于1元红券)</span>
				</p>
				<dl class="clearfix">
					<dt class="fl">门店会员积分：</dt>
					<dd class="fl mr20">
						<div class="input-group">
							<input 
							type="text" .
							class="form-control sm r-no-radius" 
							style="width:150px;" 
							value="${$config.shoppingAtom.mdhyjf}" 
							g-keyup="setValueStage mdhyjf this" />
							{{if vrbsos.sp.availablePoint==0}}
							<a class="btn btn-disabled sm l-no-radius" href="javascript:void(0)" > 兑换 </a>
							{{else}}
							<a class="btn btn-primary sm l-no-radius" href="javascript:void(0)" g-click="changeInto"> 兑换 </a>
							{{/if}}
						</div>
						{{if $config.shoppingAtom.mdhyjf_tip}}
						<span class="fontRed">${$config.shoppingAtom.mdhyjf_tip}</span>
						{{/if}}
					</dd>
					<dd class="fl">
						
					</dd>
				</dl>
				<div style="height:5px;border-bottom:1px solid #e6e6e6"></div>
				{{/if}}
				<p class="syyhq">共使用<em class=" fontRed">${selectedCouponNum}</em>张优惠券，可优惠<b class=" fontRed">${$config.formatAmount(canBeSubtracted)}</b>元</p>
			</div>
		</div>
	</div>
	{{/if}}
{{/if}}
{{if $config.VBLE.shiyongjifen()}}	
	<div class="chooes-hd clearfix mb10 pl38">
		{{if $config.shoppingAtom.syjf=="N"}}
		<span class="triger fl mr5" g-click="setStage syjf Y,render"><em class="ii c-i open fl mr5"></em>使用积分</span>
		{{else $config.shoppingAtom.syjf=="Y"}}
		<span class="triger fl mr5" g-click="setStage syjf N,render"><em class="ii c-i close fl mr5"></em>使用积分</span>
		{{/if}}
		<span class="fontGray fl">(国美在线积分)</span>
		{{if $config.shoppingAtom.syjf=="Y"}}
		<a class="know fr pr30 " target="_blank" href="${$config.URL.integralRule}">了解积分&nbsp;<span class="jt">&gt;</span></a>
		{{/if}}
	</div>
	{{if $config.shoppingAtom.syjf=="Y"}}
	<div class="chooes-bd bd1">
		<div class="integral">
			<p class="use_jifen">国美在线积分&nbsp;&nbsp;<span class="tips fontGray pl20">积分一经使用，暂不支持返还，若订单取消、拒收、退货等均以红券退回（红券有效期30天）。</span></p>
			{{if verifyStatus=="VERIFY_NOT_ACTIVATED"}}
				{{html $config.errorText.f1}}
			{{/if}}
				{{if $config.isDisableSYGMZXJF(vrbsos)}}
			<label class="checkbox_out">
				<span class="c-i no_check mr5 fl use" > </span>
				{{else vrbsos.op.selected}}
			<label class="checkbox_out" g-click="assocPathSelected vrbsos.op false ZXJF,render">
				<span class="c-i checkbox_chose mr5 fl use" > </span>
				{{else}}
			<label class="checkbox_out" g-click="assocPathSelected vrbsos.op true ZXJF,render">
				<span class="c-i checkboxs mr5 fl use" > </span>
				{{/if}}
				本次使用 <em class="fontRed">${vrbsos.op.currentPoint}</em>积分<span class="fontGray ml20">账户可用积分为${vrbsos.op.availablePoint}积分</span> 
				{{if vrbsos.op.vase=="EXCEPTION"}}
				<span class="fontRed ml20">您的账户异常，暂时无法使用，请联系客服：4008-708-708</span>
				{{/if}}
			</label>
			
		</div>
	</div>
	{{/if}}
{{/if}}

<!--美通卡-->
{{if $config.VBLE.shiyongguomeiE()}}
	<div class="chooes-hd clearfix mb10 pl38">
		{{if $config.shoppingAtom.sygmek=="N"}}
	        <span class="triger fl mr5" g-click="setStage sygmek Y,render"><em class="ii c-i open fl mr5"></em>使用美通卡</span>
	    {{else $config.shoppingAtom.sygmek=="Y"}}
	    	<span class="triger fl mr5" g-click="setStage sygmek N,render"><em class="ii c-i close fl mr5"></em>使用美通卡</span>
	    {{/if}}
	</div>
	{{if $config.shoppingAtom.sygmek=="Y"}}
	<div class="bd1 chooes-bd">
		<div class="e-card-wrap">
			<div class="e-card-title">
			{{if verifyStatus=="VERIFY_NOT_ACTIVATED"}}
				{{html $config.errorText.f1}}
			{{/if}}
				<!-- <span>为了保障您的账户资金安全，国美E卡暂不可用，请先</span>开启国美在线支付密码 > -->
				<p>特别提示：美通卡只用于购买自营商品，不可购买金银投资、汽车及虚拟类商品。</p>
			</div>	
			<div class="bind-e">
				<div class="bind-e-content">	
					<p>绑定美通卡</p>
					<div class="bind-e-card-wrap" >
						<span class="bind-e-please">请您输入手中美通卡的密码：</span>
						<input  class="form-control  e-num"  type="text" 
							g-keyup="setValueStage ecard_1 this"
							value="${$config.shoppingAtom.ecard_1}">
						<span>-</span>
						<input  class="form-control  e-num"  type="text"
							g-keyup="setValueStage ecard_2 this"
							value="${$config.shoppingAtom.ecard_2}"
						>
						<span>-</span>
						<input  class="form-control  e-num" type="text"
							g-keyup="setValueStage ecard_3 this"
							value="${$config.shoppingAtom.ecard_3}"
						>
						<span>-</span>
						<input  class="form-control  e-num" type="text"
							g-keyup="setValueStage ecard_4 this"
							value="${$config.shoppingAtom.ecard_4}"
						>
		
						<input class="form-control ver-code" 
							g-keyup="setValueStage ecard_yzm this"
							value="${$config.shoppingAtom.ecard_yzm}">
						<span class="fontRed">&nbsp;&nbsp;{{html $config.shoppingAtom.ecard_yzm_tip}}</span>	
						<a class="ver-code-pic"><img src="${$config.URL.imgCodeMTK}&capCd=${$config.shoppingAtom.yhj_img}"></a>
						<a class="ver-code-refresh c-i ecard-refresh" href="javascript:void(0);" g-click="setTimeLongStage yhj_img,render"></a>
						<a class="binding btn btn-primary btn-sm " g-click="bindECard,setTimeLongStage yhj_img">绑定</a>
						<span class="binding-explain">卡密区分大小写</span>  
					</div>
				</div>
			</div>
			<div class="use-e">
				<b>使用美通卡支付</b>
				<div  class="e-card-title bb-line">
					<i class="chose c-i checkbox_chose" style="visibility: hidden;"></i>
					<div class="card-num">卡号</div>
					<div class="card-price">面值 </div>   
					<div class="card-balance">余额</div>
					<div class="card-thisuse">本次使用</div>
					<div class="card-endtime">有效期 </div>
					<div class="card-type">类型</div>
				</div>
				{{if vrbsos.pcs.pcs &&  vrbsos.pcs.pcs.length>0}}
					<div class="use-e-wrap">
					{{each(i,pcs) vrbsos.pcs.pcs}}
							<div class="use-e-info {{html pcs.use?'':'fontGray'}}" style="display:block;">
							{{if pcs.use }}
								{{if pcs.checked == true}}
								<i 
								g-click="assocPathSelected vrbsos.pcs.pcs.${i} false EC,render" 
								class="chose c-i checkbox_chose"></i>
								{{else}}
								<i 
								g-click="assocPathSelected vrbsos.pcs.pcs.${i} true EC,render" 
								class="chose c-i checkboxs"></i>
								{{/if}}
							{{else}}
								<i 
								class="chose c-i no_check" ></i>
							{{/if}}
								<div class="card-num">${pcs.cardCode}</div>
								<div class="card-price">${pcs.totalAmount}</div>   <!-- ¥ -->
								<div class="card-balance">${pcs.remainAmount}</div>
								<div class="card-thisuse">${pcs.usedAmount}</div>
								<div class="card-endtime">${$config.formatLong(pcs.endDate)}</div>
								<div class="card-type">
									{{if pcs.type == "2"}}电子卡{{/if}}
									{{if pcs.type == "1"}}实体卡{{/if}}		
								</div>
							</div>
						
					{{/each}}
					</div>
				{{/if}}		

			</div>
			<div class="use-e-summary">
				共使用了<span class="red" >${vrbsos.pcs.prepaidCardsNum}</span> 张美通卡，金额 <span class="red">${vrbsos.pcs.prepaidCardsAmount}</span> 元
			</div>
		</div>
	</div>
	{{/if}}
{{/if}}
{{if $config.VBLE.shiyongzhanghuyue()}}
	<div class="chooes-hd clearfix pl38 mb10">
		{{if $config.shoppingAtom.syzhye=="N"}}
		<span class="triger fl mr5" g-click="setStage syzhye Y,render"><em class="ii c-i open fl mr5"></em>使用账户余额</span>
		{{else $config.shoppingAtom.syzhye=="Y"}}
		<span class="triger fl mr5" g-click="setStage syzhye N,render"><em class="ii c-i close fl mr5"></em>使用账户余额</span>
		{{/if}}
		{{if $config.shoppingAtom.syzhye=="Y"}}
		<a class="know fr pr30" target="_blank" href="${$config.URL.accountbalanceRule}">了解账户余额&nbsp;<span class="jt">&gt;</span></a>
		{{/if}}
	</div>
	{{if $config.shoppingAtom.syzhye=="Y"}}
	<div class="bd1 chooes-bd">
		<div class="balance">
			{{if verifyStatus=="VERIFY_NOT_ACTIVATED"}}
				{{html $config.errorText.f1}}
			{{/if}}
				{{if vrbsos.va.vase=="FREEZED"||vrbsos.va.payAmount==0}}
			<label class="lb_check fl mr20">
				<span class="c-i no_check mr5 fl use inpt2" > </span>
				{{else vrbsos.va.selected}}
			<label class="lb_check fl mr20" g-click="assocPathSelected vrbsos.va false ZHYE,render">
				<span class="c-i checkbox_chose mr5 fl use inpt2" > </span>
				{{else}}
			<label class="lb_check fl mr20" g-click="assocPathSelected vrbsos.va true ZHYE,render">
				<span class="c-i checkboxs mr5 fl use inpt2" > </span>
				{{/if}}
				<span >使用余额：</span><em class="fontRed">¥${$config.formatAmount(vrbsos.va.payAmount)}</em>
			</label>
			<span class=" fontGray">账户当前余额为：¥${$config.formatAmount(vrbsos.va.virtualAccountBalance)}</span>
		</div>
	</div>
	{{/if}}
{{/if}}	
{{if $config.VBLE.shiyongtuijianhao()}}
	<div class="chooes-hd clearfix pl38 mb10">
		{{if $config.shoppingAtom.sytjh=="N"}}
		<span class="triger fl mr5" g-click="setStage sytjh Y,render"><em class="ii c-i open fl mr5"></em>使用推荐号</span>
		{{else $config.shoppingAtom.sytjh=="Y"}}
		<span class="triger fl mr5" g-click="setStage sytjh N,render"><em class="ii c-i close fl mr5"></em>使用推荐号</span>
		{{/if}}
	</div>
	{{if $config.shoppingAtom.sytjh=="Y"}}
	<div class="bd1 chooes-bd" style="margin-bottom: 15px;">
		<div class="yz_mima">
			{{if $config.shoppingAtom.referrerInfo==null}}
			<div class="clearfix mdtjh pr">
				<div class="fl mr20">门店推荐人员工号:</div>
				<div class="input-group fl" style="width:230px;">
					<input 
					type="text" 
					style="width:135px;" 
					placeholder="编码" 
					class="
					form-control sm r-no-radius
					{{if $config.shoppingAtom.rygh_tip}}
					error
					{{/if}}
					" 
					g-keyup="setValueStage rygh this" 
					value="${$config.shoppingAtom.rygh}">
					{{if $config.isDisabledMD()}}
						<a 
					class="btn btn-disabled sm l-no-radius" 
					href="javascript:void(0)">
						确定
					</a>
					{{else}}
						<a 
						class="btn btn-primary sm l-no-radius" 
						href="javascript:void(0)" 
						g-click="ryghAction">
							确定
						</a>
					{{/if}}					
				</div>
				<span class="fontRed pabs" style="top:41px;left:146px;">{{html $config.shoppingAtom.rygh_tip}}</span>
			</div>
			{{else}}
			<div class="clearfix mdtjh pr">
				<div class="fl mr20">门店推荐人员工号:</div>
				<div class="fl mr20">${$config.shoppingAtom.referrerInfo.employeeId}</div>
				<div class="fl mr20">${$config.shoppingAtom.referrerInfo.employeeName}</div>
				<div class="fl mr20">${$config.shoppingAtom.referrerInfo.structureName}</div>
				<div class="fl mr20"><a href="javascript:void(0)" class="fontBlue ml10" g-click="ryghCanel">取消使用</a></div>
			</div>
			{{/if}}
		</div>
	</div>
	{{/if}}
{{/if}}	
</div>
{{if verifyStatus=="NEED"||verifyStatus=="PASSED"||verifyStatus=="ERRLOCKED"}}
<div g-pipe>
	<div class="yz_mima">
		<form class="active-nForm pt10">
			{{if verifyStatus=="NEED"}}
			<p class="fontGray">为了您的账户安全，需要验证您的支付密码</p>
			<div class="clearfix">
				<span class="fl">支付密码：</span>
				<input 
				type="password" 
				style="width:135px;" 
				class="form-control sm mr5 fl" 
				g-keyup="setValueStage yzm_pw this"
				value="${$config.shoppingAtom.yzm_pw}">
				<a class="btn btn-primary btn-sm fl" href="javascript:void(0)" g-click="payRegAction,setTimeLongStage yzm_img">确定</a>
				<a class="ml10 fl"  target="_blank" href="${$config.URL.forgetPassword}">忘记密码<span class="jt">&gt;</span></a>
			</div>
			<div style="color:#f00;padding-left:60px;">
				{{html $config.shoppingAtom.yzm_tip}}
			</div>
			{{/if}}
			{{if verifyStatus=="PASSED"}}
			<p class="clearfix"><span class="fl">支付密码已验证</span><em class="c-i correct mt5 ml5 fl"></em></p>
			{{else verifyStatus=="ERRLOCKED"}}
			<p class="clearfix"><span class="fl">密码错误5次，已被锁定，将于锁定后24小时解锁</span><em class="c-i warning mt5 ml5 fl"></em></p>
			{{/if}}
		</form>
	</div>
</div>
{{/if}}
