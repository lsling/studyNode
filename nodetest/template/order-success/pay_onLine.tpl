<div class="oc_cashOnDelivery">
    {{each sc}}
    <dl class="delivery">
        <dt>${sme}</dt>
        {{each scs}}
        <dd>
            <span class="i-block col_105">${itemTypeNum}种商品共${itemNum}件</span>
            {{if shippingDateAsString}}<span class="i-block col_300">送货时间：${shippingDateAsString}</span>{{/if}}
            {{if deadl}}<span>距商品到达时间还剩余 <b data-time="${deadl}"  class="oc_cuttime"></b></span>{{/if}}
        </dd>
        {{/each}}
    </dl>
    {{/each}}
</div>