var express=require("express");
var gomeConfig=require("./gomeConfig.js");
var connectLog=require("../util/connectLog.js");
var router=express();
//基础配置信息
router.use(function(req,res,next){
	if(req.query.debug){
		req.cartconfig=gomeConfig.debug;
	}else{
		req.cartconfig=gomeConfig.normal
	}
	next();
});
//日志管理
router.use(connectLog);
//购物车页
function cart(req,res){

	var subDomain        = {};
    subDomain.home       = 'www';
    subDomain.groupOn    = 'tuan';
    subDomain.rushBuy    = 'q';
    subDomain.secondHand = 'tao';
    subDomain.warranty   = 'www';
    subDomain.presell    = 'www';


    var cookieDomain = req.cartconfig.cookieDomain;

	var site=req.params.site || "home";
	res.render("cart.tpl",{
		config:req.cartconfig,
		site:site,
		gomeUrl: ('http://' + subDomain[site] + cookieDomain)
	});
}
//提交订单页
function shopping(req,res){
	var site=req.params.site||"home";
	res.render("shopping.tpl",{
		config:req.cartconfig,
		site:site
	});
}
//提交订单成功页
function orderSuccess(req,res){
	res.render("order-success.tpl",{
		config:req.cartconfig
	});
}
//海外购身份认证
function authorization(req,res){
	res.render("readAuthorization.tpl",{
		config:req.cartconfig
	});
}
//延保身份认证页面
function allowance(req,res){
	res.render("save-energy-allowance.tpl",{
		config:req.cartconfig,
		productId:req.query.productId,
		skuId:req.query.skuId
	});
}
//加入购物车成功页
function addSuccess(req,res){
	res.render("addsuccess.tpl",{
		config:req.cartconfig
	});
}
//最新加入购物车成功页
function addSuccess2(req,res){
	res.render("addsuccess2.tpl",{
		config:req.cartconfig
	});	
}
//支付成功页
function paymentSuccess(req,res){
	res.render("payment-success.tpl",{
		config:req.cartconfig
	});
}
//公共头 本地测试用
function commonHead_user(req,res){
	res.send("HELLO HEAD USER")
}
router.get("/",cart);
router.get("/:site/cart",cart);
router.get("/shopping",shopping);
router.get("/:site/shopping",shopping);
router.get("/order-success",orderSuccess);
router.get("/haiwaigou/read/authorization",authorization);
router.get("/save/energy/allowance",allowance);
router.get("/addsuccess.html",addSuccess);
router.get("/addsuccess",addSuccess2);
router.get("/payment-success",paymentSuccess);
router.get("/n/common/*",commonHead_user);
module.exports=router;
