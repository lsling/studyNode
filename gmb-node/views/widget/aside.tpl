{% if config.debug %}
<div id="gome-bar">
    <div class="gome-aside">
        <ul>
            <li id="gome-aside-coupon">
                <b></b><a href="javascript:void(0)"><i></i></a>
                <div><a>我的优惠券</a></div>
            </li>
            <li id="gome-aside-survey">
                <b></b><a data-code="1000064961" href="http://wj.qq.com/survey.html?id=165822&hash=1c70" target="_blank"><i></i></a>
                <div><p><a data-code="1000064961" href="http://wj.qq.com/survey.html?id=165822&hash=1c70" target="_blank">调研问卷</a></p></div>
            </li>
            <li id="gome-aside-service">
                <b></b><a class="live800-service"><i></i></a>
                <div><p><a class="live800-service">在线客服</a></p></div>
            </li>
            <li id="gome-aside-backtop">
                <b></b><a href="javascript:void(0)"><i></i></a>
                <div><p><a>返回顶部</a></p></div>
            </li>
        </ul>
    </div>
    <!-- gome-aside -->
    <div id="gome-bar-border">
        <ul id="gome-bar-border-tab">
            <li class="gome-bar-btn-coupon" data-open="coupon"><b></b><i></i></li>
        </ul>
    </div>
    <!-- gome-bar-border -->
    <div id="gome-bar-box">
        <div id="gome-bar-coupon" class="gome-bar-box-wrap">
            <h3><span class="gome-bar-close"></span></h3>
            <div id="coupon-body" class="gome-bar-box-body">
                <div class="gome-bar-box-loading">
                    <img src="http://img.gomein.net.cn/images/grey.gif" gome-src="http://img.gomein.net.cn/gmlib/ui/loading/1.0.0/loading.gif">
                </div>
            </div>
        </div>
        <!-- gome-bar-coupon -->
    </div>
    <!-- gome-bar-box -->
</div>
<!--aside-->
{% else %}
<!--# include virtual="/n/common/a11/aside.html"-->
{% endif %}

