module.exports.gomeCartNative ={
   // "apiData":"E:/workspaces/dev/Storefront/storedocroot.war/gmpro/1.0.0/cart/front-cart",
    "apiData":"D:/node-express-project/studyNode",
    "statics": [
        "D:/node-express-project/studyNode/nodetest"
        // "E:/workspaces/pre/Storefront/storedocroot.war"
    ],
    "project": {
        "routes": [
            [
                "/icon",
                "{config.statics.0}/css/i/index.html"
            ], [
                "/:a/api/:b",
                "{config.apiData}/{params.a}/{params.b}.json"
            ]
        ],
        "tplTofn":{
            "src":"{config.statics.0}/template",
            "dist":"{config.statics.0}/templatefn",
            "/comp.js":[
                "/comp"
            ],
            "/cart.js":[
                "/cart",
                "/comp/alert"
            ],
            "/shopping.js":[
                "/shopping",
                "/comp/gstore",
                "/comp/alert"
            ],
            "/order-success.js":[
                "/order-success"
            ],
            "/addsuccess2.js":[
                "/addsuccess2",
                "/comp/alert"
            ]
        },
        /*"sprite":{
            "src":"{config.statics.0}/gmpro/1.0.0/cart/1.0.0/css/i/sprite",
            "dist":"{config.statics.0}/gmpro/1.0.0/cart/1.0.0/css/i/sprite.png",
            "selector":".c-i",
             "html":"{config.statics.0}/gmpro/1.0.0/cart/1.0.0/html/icons/index.html",
            "css":"{config.statics.0}/gmpro/1.0.0/cart/1.0.0/css/i/sprite.css"
        }*/
        "sprite":{
            "src":"{config.statics.0}/css/i/sprite",
            "dist":"{config.statics.0}/css/i/sprite.png",
            "selector":".c-i",
             "html":"{config.statics.0}/css/i/index.html",
            "css":"{config.statics.0}/css/i/sprite.css"
        }
    }
}
